#!/usr/bin/env bash
CONTAINERNAME="mrestore"
LOCALPORT=33306
RESTOREDATE="$1"
if [ "$1" = "-h" ] ; then
	echo "$0 [ <restore date> ]"
	exit 1
fi
if [ -z "$1" ] ; then
	RESTOREDATE="$(basename "$(find mysql.backup -mindepth 1 -maxdepth 1 -type d | sort | tail -1)")"
	echo "Restore date set to $RESTOREDATE"
	echo "Ctrl+c NOW if you don't agree"
	sleep 5
fi
set -x
docker build -f mariarestore.dockerfile -t $CONTAINERNAME .
docker run -d --mount src="$(pwd)/mysql.backup",target=/mysql.backup,type=bind -p $LOCALPORT:3306 "$CONTAINERNAME"
docker exec -t "$(docker ps --filter="ancestor=$CONTAINERNAME" --format="{{.ID}}")" bash /mariarestore "$RESTOREDATE"
docker exec -d -t "$(docker ps --filter="ancestor=$CONTAINERNAME" --format="{{.ID}}")" su mysql -c mariadbd
sleep 5 ; docker exec -t "$(docker ps --filter="ancestor=$CONTAINERNAME" --format="{{.ID}}")" mariadb -e "source /init.sql"
set +x
echo "You can now play: mysql -u root -P $LOCALPORT"
