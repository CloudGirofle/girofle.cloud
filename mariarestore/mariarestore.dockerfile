FROM mariadb:10.11
RUN apt update && apt install -y mariadb-plugin-provider-bzip2 mariadb-plugin-provider-lz4 mariadb-plugin-provider-lzma mariadb-plugin-provider-lzo mariadb-plugin-provider-snappy vim
COPY mariarestore .
COPY init.sql .
RUN chmod 755 mariarestore
ENTRYPOINT ["sleep","1221221"]
