Copier le répertoire de backup mysql.backup ici
en ne laissant :
- si le backup date d'avant le 23/01/2025 : l'initial et TOUS les intermédiaires
- si le backup est postérieur au 23/01/2025 : l'initial et JUSTE celui que vous voulez restaurer

Puis:

    END="2025-01-11-00h00"
    docker build -f mariarestore.dockerfile -t mrestore .
    docker run -d --mount src="$(pwd)/mysql.backup",target=/mysql.backup,type=bind -p 33306:3306 mrestore
    docker exec -t "$(docker ps --filter="ancestor=mrestore" --format="{{.ID}}")" bash /mariarestore "$END"
    docker exec -d -t "$(docker ps --filter="ancestor=mrestore" --format="{{.ID}}")" su mysql -c mariadbd
    sleep 5 ; docker exec -t "$(docker ps --filter="ancestor=mrestore" --format="{{.ID}}")" mariadb -e "source /init.sql"
    mysql -u root -P 33306

Ou alors:

    ./restore.sh [ <END> ]
