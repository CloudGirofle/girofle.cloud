Scripts hegerbement
===================

La doc, elle, se trouve dans le wiki.

# Architecture
- `scripts` des scripts à exécuter sur les machines
- `admin` des outils qui se connectent en remote sur les machines pour effectuer des tâches

# Environement Python et dependences

On utilise l'outil [uv](https://docs.astral.sh/uv/) pour gérer les dépendences Python et les environements virtuels.

# Pre-commit hooks

Les pre-commit hooks permettent de lancer des vérifications automatique lors des commits. https://pre-commit.com/

Pour installer l'outil `pre-commit`, vous pouvez utilser `pip`.

```
pip install pre-commit
```

Pour installer les hooks ou les mettre à jour, lancez la commande suivante.

```pre-commit install```
