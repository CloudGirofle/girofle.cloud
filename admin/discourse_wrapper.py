#!/usr/bin/python3
# A script to manage a multi-forum Discourse install
# Based on the default documentation.
# By MW, Cloud Girofle, GPLv3+, May 2021

# usage:
# ./discourse_wrapper -h # show help
# ./discourse_wrapper list # lists all configured instances, whether they are active or not
# ./discourse_wrapper activate <id>
# ./discourse_wrapper deactivate <id>

# statuses: active, inactive, main

#import yaml
import ruamel.yaml
import os, argparse, ast, sys, datetime

app_p = "./app.yml"
#inactive_p = "/var/discourse/containers/app_inactive.json"

## ==== Functions
def list_instances(y, c, check=True, ssl=True, verbose=True):
    """List existing instances and check if they are complete
    y: yaml (active)
    c: comment (starting with #inactive)"""

    ff = [i['file']['contents'] for i in  y['hooks']['before_bundle_exec'] if 'file' in i and i['file']['path']=='$home/config/multisite.yml'][0]

    #db_l = yaml.safe_load(ff)
    db_l = ruamel.yaml.safe_load(ff)
    db_li = [ast.literal_eval(i.replace('##inactive: ','')) for i in c]

    if verbose:
        print("-- Active instances:")
        for (i,l) in enumerate(db_l):
            print(f"{i+1}/{len(db_l)}: {l}, database:{db_l[l]['database']}, host_names:{db_l[l]['host_names']}")

        print("-- Inactive instances:")
        for (i,l) in enumerate(db_li):
            print(f"{i+1}/{len(db_li)}: {l['name']}, database:{l['database']}, host_names:{l['host_names']}")

    if check:
        check_after_postgres(y, db_l)
        if ssl:
            check_after_ssl(y, db_l)
    return db_l
   
def check_after_postgres(y, db_l):
    """Check if the database is properly created"""
    
    pat = [{'exec': 'sudo -u postgres createdb {} || exit 0'},
           {'exec': {'stdin': 'grant all privileges on database {} to discourse;', 'cmd': 'sudo -u postgres psql {}', 'raise_on_fail': False}},
           {'exec': '/bin/bash -c \'sudo -u postgres psql {} <<< "alter schema public owner to discourse;"\''},
           {'exec': '/bin/bash -c \'sudo -u postgres psql {} <<< "create extension if not exists hstore;"\''},
           {'exec': '/bin/bash -c \'sudo -u postgres psql {} <<< "create extension if not exists pg_trgm;"\''}]

    db_ll = [i['database'] for i in db_l.values()]
    yy=[i['exec'] for i in y['hooks']['after_postgres']]
    def _remback(i):
        """Remove newlines in stdin"""
        if 'stdin' in i:
            i['stdin'] = i["stdin"].replace('\n','')
        return i
    
    yy = [i if type(i)==str else _remback(i) for i in yy] # we remove newlines
    matched_lines = []
    for dbn in db_ll:
        idx=-1
        for ll in pat:
            try:
                if type(ll['exec']) is dict:
                    cmd = ll['exec'].copy()
                    cmd['stdin']=cmd['stdin'].format(dbn)
                    cmd['stdin']=cmd['stdin'].replace('\n', '') # DBG
                    cmd['cmd']=cmd['cmd'].format(dbn)
                elif type(ll['exec']) is str:
                    cmd=ll['exec'].format(dbn)
                else:
                    raise TypeError(f"'exec' content type should be str or dict, got: {ll['exec']}")
                i=yy.index(cmd)
            except ValueError as e:
                raise ValueError(f"Database not properly created, line '{ll}' not found")
            if i<idx:               
                raise ValueError("database creation happens in the wrong order")
            matched_lines.append(i)
            idx=i
    
    # Check that there is no extra DB
    if len(matched_lines)!=len(yy):
        print("Lines not matched:")
        for l in [yy[i] for i in range(len(yy)) if i not in matched_lines]:
            print(l)

def check_after_ssl(y, db_l):
    """Check that the domains are properly targeted for SSL"""
    yy=[i['replace']['to'] for i in y['hooks']['after_ssl'] if 'replace' in i and i['replace']['from']=='/--keylength/'][0]
    db_domains = []
    for d in db_l.values():
        db_domains+=d['host_names']
    db_domains=set(db_domains)
    yy_d= yy.replace(" --keylength", "").split(" ")
    assert all([j=='-d' for (i,j) in enumerate(yy_d) if i%2==0]), "Missing leading '-d' in syntax of the SSL domains, got: {}".format(yy)
    ssl_d = set([j for (i,j) in enumerate(yy_d) if i%2==1])
    assert db_domains==ssl_d, "Domains enumerated in 'before_bundle' and 'after_ssl' do not match, got {} and {}, respectively".format(db_domains, ssl_d)

def activate_instance(idx, c, y):
    """Activate an instance"""
    db_li = [ast.literal_eval(i.replace('##inactive: ','')) for i in c]
    l=db_li[idx-1]

    ## Make sure we are doing right
    print("The following instance will be activated:")
    print(f"{idx}/{len(db_li)}: {l['name']}, database:{l['database']}, host_names:{l['host_names']}")
    r=input("Is that ok? [yes|no] ") # commented for dbg
    if r!='yes':
        print("Aborting")
        return

    new_y = y.copy()

    ## after_postgres   #.yaml_add_eol_comment('some comment', key='new_key', column=40) # column is optional
    after_postgres = [{'exec': 'sudo -u postgres createdb {} || exit 0'},
                      {'exec': {'stdin': 'grant all privileges on database {} to discourse;', 'cmd': 'sudo -u postgres psql {}', 'raise_on_fail': False}},
                      {'exec': '/bin/bash -c \'sudo -u postgres psql {} <<< "alter schema public owner to discourse;"\''},
                      {'exec': '/bin/bash -c \'sudo -u postgres psql {} <<< "create extension if not exists hstore;"\''},
                      {'exec': '/bin/bash -c \'sudo -u postgres psql {} <<< "create extension if not exists pg_trgm;"\''}]
    ins = after_postgres.copy()
    ins[0]['exec']=ins[0]['exec'].format(l['database'])
    ins[1]['exec']['stdin']=ins[1]['exec']['stdin'].format(l['database'])
    ins[1]['exec']['cmd']=ins[1]['exec']['cmd'].format(l['database'])
    for i in [2,3,4]:
        ins[i]['exec']=ins[i]['exec'].format(l['database'])
    new_y['hooks']['after_postgres'].extend(ins)
    #print(new_y['hooks']['after_postgres'])

    ## before_bundle_exec
    ins='{name}:\n  adapter: postgresql\n  database: {database}\n  pool: 25\n  timeout: 5000\n  db_id: 2\n  host_names:\n'.format(**l)
    ins+=''.join('    - {}\n'.format(i) for i in l['host_names'])
    new_y['hooks']['before_bundle_exec'][0]['file']['contents']+=ins
    #print(new_y['hooks']['before_bundle_exec'][0]['file']['contents'])    

    ## after_ssl
    
    ## Add it to the yml
    yi=[j for (j,i) in enumerate(y['hooks']['after_ssl']) if 'replace' in i and i['replace']['from']=='/--keylength/'][0]
    s=new_y['hooks']['after_ssl'][yi]['replace']['to']
    new_y['hooks']['after_ssl'][yi]['replace']['to']=''.join([f'-d {i} ' for i in l['host_names']])+s
    #print(new_y['hooks']['after_ssl'][yi]['replace']['to'])

    ## Remove from inactive
    db_ln = db_li.copy()
    db_ln.pop(idx-1)

    return (new_y, db_ln, l)

def save_file(new_y, db_ln, app_p, verbose=True):
    """Save a YAML file, with a list of inactive files"""

    assert app_p.endswith('.yml')
    d=datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d-%Hh%M")
    app_n = app_p[:-4]+f'.{d}.yml'

    assert not os.path.isfile(app_n)

    with open(app_n, 'w') as f:
        f.write('## /!\ THIS SCRIPT IS MACHINE-GENERATED BY discourse_wrapper.py, DO NOT EDIT MANUALLY\n')
        f.write(ruamel.yaml.dump(new_y, Dumper=ruamel.yaml.RoundTripDumper))
        f.write('\n')
        for l in db_ln:
            f.write('##inactive: '+str(l)+'\n')

    return app_n

if __name__ == "__main__":
    ## ==== Argument parser
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-list', help='lists all configured instances, whether they are active or not', action='store_true')
    group.add_argument('-activate', help='activate a given instance, specified by its <idx>', type=int)
    group.add_argument('-deactivate', help='deactivate a given instance, specified by its <idx>', type=int)
    parser.add_argument('-ssl', help='check if SSL is properly configured. Only used if Discourse handles Letsencrypt itself', action='store_true')
    args = parser.parse_args()

    ## ==== Initial checks
    assert os.path.isfile(app_p)

    ## ==== Load data
    with open(app_p, 'r') as f:
        #y = yaml.safe_load(f)
        y = ruamel.yaml.load(f, Loader=ruamel.yaml.RoundTripLoader)
    with open(app_p, 'r') as f:
        c = [i for i in f.readlines() if i.startswith('##inactive')]

    print(args)
    if args.list:
        list_instances(y, c, check=True, ssl=args.ssl, verbose=True)
    elif args.activate is not None:
        assert 0<args.activate<=len(c)
        a,b,l=activate_instance(args.activate, c, y)
        app_n = save_file(a,b,app_p)
        print(f"File saved to {app_n}")
        print()
        print("Do not forget to create a new admin (see https://meta.discourse.org/t/create-admin-account-from-console/17274)")
        print("cd /var/discourse/")
        print("./launcher enter app # in root")
        print(f"RAILS_DB={l['name']} rake admin:create # admin, with a blank pw; pay attention to RAILS_DB")
        print("BEFORE THAT:")
        for domain in l['host_names']:
            print("  run `./create_discourse.sh {}`".format(domain))
        
    elif args.deactivate is not None:
        print("Deactivating: NOT IMPLEMENTED")

## == Admins

#Important doc: https://meta.discourse.org/t/create-admin-account-from-console/17274
## ATTENTION À RAILS_DB
#cd /var/discourse/
#sudo ./launcher enter app
#RAILS_DB=secondsite rake admin:create # First create our admin, with a non-blank pw
#RAILS_DB=secondsite rake admin:create # Do it again with the external email, and press enter when the pw is asked
#RAILS_DB=barasso rake admin:create # First create our admin, with a non-blank pw
#RAILS_DB=bl_vexincentre rake admin:create
