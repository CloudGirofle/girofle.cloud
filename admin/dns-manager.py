#!/usr/bin/python3
# By CG, GPLv3+, Aug 2023
# Script pour gérer la configuration DNS de plusieurs domaines

import argparse
import datetime
import io
import json
import os
import re
import subprocess
import sys
import textwrap
from argparse import RawTextHelpFormatter
from typing import Tuple

import dns.resolver  # dnspython
import requests
import tldextract
from dotenv import dotenv_values  # python-dotenv

# DKIM it now functions
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DKIMITNOW_DIR = "../scripts/email/"
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, DKIMITNOW_DIR)))
import dkimitnow  # noqa E402

# fabric functions
try:
    import fabric

    FABRIC_AVAILABLE = True
except ImportError:
    FABRIC_AVAILABLE = False

dns_resolver = dns.resolver.Resolver()

SECRET_ENV = "./girofle.env"  # path vers les variables d'environnement
DATE_FORMAT = "%Y-%m-%d-%Hh%M"


class GandiAPI:
    def __init__(self, key):
        self.APIKEY = key
        self.APIROOT = "https://api.gandi.net/v5/livedns/domains/{}/{}?page={}"
        self.APIINSERT = " https://api.gandi.net/v5/livedns/domains/{}/records/{}"
        self.GANDI_RECORD_TYPES = (
            "A",
            "AAAA",
            "ALIAS",
            "CAA",
            "CDS",
            "CNAME",
            "DNAME",
            "DS",
            "KEY",
            "LOC",
            "MX",
            "NAPTR",
            "NS",
            "OPENPGPKEY",
            "PTR",
            "RP",
            "SPF",
            "SRV",
            "SSHFP",
            "TLSA",
            "TXT",
            "WKS",
        )

    def get_dns_zone(self, domain, print_mode=[]) -> dict:
        """print_mode is a list of representation, can be 'json', 'txt', 'table'"""
        headers = {"authorization": f"Apikey {self.APIKEY}"}
        r = []
        page = 1
        while True:
            rr = requests.get(self.APIROOT.format(domain, "records", page), headers=headers)
            assert rr.status_code == 200
            if rr.content != b"[]":
                r.append(rr.content)
                page += 1
            else:
                break

        # JSON formatting
        zone = [json.loads(i) for i in r]
        zone = [item for sublist in zone for item in sublist]

        # TXT formatting
        pat = "{rrset_name} {rrset_ttl} IN {rrset_type} {value}"
        txt = []
        for rec in zone:
            for val in rec["rrset_values"]:
                txt.append(pat.format(value=val, **rec))
        rep = {"json": zone, "txt": txt}

        # Print if needed
        if "json" in print_mode:
            print(json.dumps(rep["json"], indent=2))
        if "txt" in print_mode:
            for line in rep["txt"]:
                print(line)

        return rep

    def _parse_dns_line(self, dnsline: str) -> Tuple[str, dict]:
        """Returns (tuple of a string and a dict)
        ("coucou"
        {
            "rrset_type": "A",
            "rrset_values": ["192.0.2.1"],
            "rrset_ttl": 10800 # optional
        })
        """
        ls = dnsline.split(" ")
        rrset_name = ls[0]
        rrset_ttl = int(ls[1])
        assert 300 <= rrset_ttl <= 2592000
        assert ls[2] == "IN"
        rrset_type = ls[3]
        assert rrset_type in self.GANDI_RECORD_TYPES
        rrset_values = " ".join(ls[4:]).strip()

        return (
            rrset_name,
            {
                "rrset_type": rrset_type,
                "rrset_values": [rrset_values],
                "rrset_ttl": rrset_ttl,
            },
        )

    def insert_record(self, domain: str, dnsline: str) -> None:
        dat = self._parse_dns_line(dnsline)
        headers = {
            "authorization": f"Apikey {self.APIKEY}",
            "content-type": "application/json",
        }
        rr = requests.post(
            self.APIINSERT.format(domain, dat[0]),
            headers=headers,
            data=json.dumps(dat[1]),
        )
        try:
            print(json.loads(rr.content)["message"])
        except Exception:
            print(rr.content)

        def _err():
            return "error: {}\ndomain: {}\n payload: {}\n".format(rr.content, dat[0], dat[1])

        assert rr.status_code in (200, 201), _err()


class DNSGitStore:
    def __init__(self, p: str):
        self.DNS_GIT_REPOSITORY = p
        self.DNS_REMOTE_REPOSITORY = None
        self.pass_env = {"PASSWORD_STORE_DIR": self.DNS_GIT_REPOSITORY}

    def _exec_pass_cmd(self, cmd, content_input=None):
        """Take a shell str as input and execute it"""
        if content_input is None:
            r = subprocess.run(cmd.split(), capture_output=True, env=self.pass_env)
        else:
            assert isinstance(content_input, str)
            r = subprocess.run(
                cmd.split(),
                capture_output=True,
                env=self.pass_env,
                input=content_input,
                text=True,
            )
        return r

    def get_local_repository(self) -> str:
        return self.DNS_GIT_REPOSITORY

    def get_distant_repository(self) -> str:
        if self.DNS_REMOTE_REPOSITORY is None:
            cmd = "pass git remote -v"
            r = self._exec_pass_cmd(cmd)
            rdec = r.stdout.decode().split("(fetch)\n")[0].split("\t")[-1]
            self.DNS_REMOTE_REPOSITORY = rdec
        return self.DNS_REMOTE_REPOSITORY

    def get_snapshots_by_date(self, domain) -> list:
        """Return a list of tuples of files, sorted by date, most recent first"""
        # List snapshot dates
        list_exts = (".json", ".txt")
        files = os.listdir(os.path.join(self.DNS_GIT_REPOSITORY, domain))
        files = [re.sub(".gpg$", "", i) for i in files]
        files = list(set([os.path.splitext(i)[0] for i in files if os.path.splitext(i)[1] in list_exts]))

        # Sort by date
        files.sort(key=lambda x: datetime.datetime.strptime(x, DATE_FORMAT), reverse=True)

        # Parse dates
        dates = [datetime.datetime.strptime(d, DATE_FORMAT) for d in files]

        return list(zip(files, dates))

    def get_dns_snapshot(self, domain, date, pull=False) -> str:
        """Gracefully return a given snapshot"""
        if pull:
            self._pull()
        pp = os.path.join(domain, date + ".txt")
        p = os.path.join(self.DNS_GIT_REPOSITORY, pp + ".gpg")
        if os.path.isfile(p):
            return self.show(pp)
        else:
            print(f"No snapshot has been found for the domain {domain} at date {date}")
            print("But the following snapshots exist for this domain:")
            for s in self.get_snapshots_by_date(domain):
                print("\t{}".format(s[0]))

    def show(self, path) -> str:
        cmd = f"pass show {path}"
        r = self._exec_pass_cmd(cmd)
        return r.stdout.decode()

    def list(self, domain="") -> None:
        """List the content of the pass"""
        if not os.path.isdir(os.path.join(self.DNS_GIT_REPOSITORY, domain)):
            print(f"The pass/git repo does not have snapshots for domain '{domain}'")
        else:
            cmd = f"pass ls {domain}"
            r = self._exec_pass_cmd(cmd)
            print(r.stdout.decode())

    def _insert(self, path, content):
        """wrapper around `pass insert` command"""
        cmd = f"pass insert --multiline {path}"
        return self._exec_pass_cmd(cmd, content)

    def _push(self):
        cmd = "pass git push"
        return self._exec_pass_cmd(cmd)

    def _pull(self):
        """Update repository from distant repo"""
        cmd = "pass git pull"
        return self._exec_pass_cmd(cmd)


def load_env(path) -> dict:
    """Charger les variables d'environnement contenues dans `path`"""
    assert os.path.isfile(path), f".env file {SECRET_ENV} does not exist"
    config = dotenv_values(path)
    if "DOMAINS" in config:
        config["DOMAINS"] = [i.strip() for i in config["DOMAINS"].split(",") if i.strip() != ""]
    if "SERVER_IPS" in config:
        config["SERVER_IPS"] = eval(config["SERVER_IPS"])
        assert isinstance(config["SERVER_IPS"], dict)
    return config


def snapshot(domain, dnsgit, gandiapi) -> None:
    """Download a DNS zone and save it in the git repo"""
    fn = datetime.datetime.now().strftime(DATE_FORMAT)
    path = f"{domain}/{fn}"
    z = gandiapi.get_dns_zone(domain)
    keys = []

    # Snapshot json
    c = json.dumps(z["json"], indent=2)
    dnsgit._insert(path + ".json", c)
    keys.append(path + ".json")

    # Snapshot txt
    c = "\n".join(z["txt"])
    dnsgit._insert(path + ".txt", c)
    keys.append(path + ".txt")

    print("Keys inserted:")
    print("".join([f"-- {i} \n" for i in keys]))


def snapshot_all(domains, dnsgit, gandiapi) -> None:
    """Simply loop over all defined domains"""
    for d in domains:
        print(f"Taking a snapshot for domain {d}")
        snapshot(d, dnsgit, gandiapi)
    print("Pushing to server...")
    dnsgit._push()


def compare_dns_snapshot(domain, dnsgit, gandiapi, pull=True) -> None:
    """Check if the latest snapshotted version matches with the current Gandi zone"""

    # git pull if needed
    if pull:
        dnsgit._pull()

    # Retrieve latest zones
    git_snapname = dnsgit.get_snapshots_by_date(domain)[0]  # most recent snapshot
    git_zone = dnsgit.show(f"{domain}/{git_snapname[0]}.txt")
    gdi_zone = gandiapi.get_dns_zone(domain)["txt"]
    print("Last snapshot: {}".format(git_snapname[0]))

    # Compare
    ok = True
    for lgit in git_zone.split("\n"):
        try:
            i = gdi_zone.index(lgit)
            gdi_zone.pop(i)
        except Exception:
            ok = False
            print(f"LINE not in GANDI: {lgit}")
    if len(gdi_zone) > 0:
        ok = False
    for lgdi in gdi_zone:
        print(f"LINE not in GIT snapshot: {lgdi}")
    if ok:
        print("Current DNS zone is conform to snapshot")


# ==== Handle input
def fparse_config(ns, config, dnsgit, gandiapi) -> None:
    """Handle the `config` keyword
    config file: show URL of .env file
    config url: show URL of remote pass
    config show: display .env file"""

    if ns.action == "file":
        print(ns.config)
    elif ns.action == "store":
        print(config["DNS_GIT_REPOSITORY"])
    elif ns.action == "url":
        print(dnsgit.get_distant_repository())
    elif ns.action == "show":
        with open(ns.config, "r") as f:
            print(f.read())


def fparse_dnszone(ns, config, dnsgit, gandiapi) -> None:
    """
    dnszone --list <domain(s)|all>
    dnszone --snapshot <domain(s)|all>
    dnszone --check <domain|all>
    dnszone --show <(sub)domain> <current|date>
    """
    if ns.pull:
        print("Pulling from repository")
        print(dnsgit._pull().stdout.decode())

    if ns.list is not None:
        if "all" in ns.list:
            dnsgit.list()
        elif len(ns.list) > 0:
            for i in ns.list:
                dnsgit.list(i)
        else:
            parser_dnszone.print_help()
    elif ns.snapshot is not None:
        if "all" in ns.snapshot:
            snapshot_all(config["DOMAINS"], dnsgit, gandiapi)
        elif len(ns.snapshot) > 0:
            for d in ns.snapshot:
                snapshot(d, dnsgit, gandiapi)
        else:
            parser_dnszone.print_help()
    elif ns.check is not None:
        if "all" in ns.check:
            for d in config["DOMAINS"]:
                print(f"\nChecking DNS zone: {d}")
                compare_dns_snapshot(d, dnsgit, gandiapi, pull=False)
        elif len(ns.check) > 0:
            for d in ns.check:
                print(f"\nChecking DNS zone: {d}")
                compare_dns_snapshot(d, dnsgit, gandiapi, pull=False)
        else:
            parser_dnszone.print_help()
    elif ns.show is not None:
        fulldomain = ns.show[0]
        ext = tldextract.extract(ns.show[0])
        domain = ".".join(ext[1:])
        date = ns.show[1]
        if date == "current":
            z = gandiapi.get_dns_zone(domain)["txt"]
        else:
            z = dnsgit.get_dns_snapshot(domain, date, pull=False).split("\n")
        if ext.subdomain == "":
            print("\n".join(z))
        else:  # we are dealing with a subdomain
            print("; Basic filtering for subdomain {}".format(fulldomain))
            print("\n".join([i for i in z if i.split(" ")[0].endswith(ext.subdomain)]))
    else:
        if (not ns.pull) and (not ns.push):
            parser_dnszone.print_help()

    if ns.push:
        print("Pushing to repository")
        print(dnsgit._push().stdout.decode())


def check_remote_dkimitinow(config) -> None:
    assert FABRIC_AVAILABLE, "Fabric package not installed"
    assert config["EMAILRELAY_SSH"], ".env variable EMAILRELAY_SSH not defined"
    assert config["EMAILRELAY_DKIMITNOW"], ".env variable EMAILRELAY_DKIMITNOW not defined"


def check_email_config(domain, selector="mail", dnsonly=True, verbose=True) -> None:
    if dnsonly:
        dkimitnow.check_spf(domain, cname=None, print_dns=False, verbose=verbose)
        dkimitnow.check_dmarc(domain, cname=None, print_dns=False, verbose=verbose)
        dkimitnow.check_dkim_dns(domain, selector, verbose=verbose)
    else:
        # Check that we have the info to connect
        check_remote_dkimitinow(config)

        # Run the test
        server = config["EMAILRELAY_SSH"]
        print(f"Connecting to {server}, be ready to enter your remote password")
        c = fabric.Connection(server)
        dkimitpath = config["EMAILRELAY_DKIMITNOW"]
        cmd = f"sudo {dkimitpath} --check {domain}"
        c.run(cmd)


def setup_email_config(subdomain, cname=None, ips=None) -> None:
    """Call the dkimitnow script on the remote host, parse the output, and insert records in gandi if necessary."""
    ext = tldextract.extract(cname)
    domain = ".".join(ext[1:])
    if ips is not None:
        if "ip4" in ips:
            ip4 = "--ip4 {}".format(ips["ip4"])
        if "ip6" in ips:
            ip6 = "--ip6 {}".format(ips["ip6"])
    else:
        ip4 = ip6 = ""

    # run dkimitnow
    server = config["EMAILRELAY_SSH"]
    print(f"Connecting to {server}, be ready to enter your remote password")
    c = fabric.Connection(server)
    dkimitpath = config["EMAILRELAY_DKIMITNOW"]
    cmd = f"sudo {dkimitpath} --dns {subdomain} --initial-cname {cname} {ip4} {ip6}"
    r = c.run(cmd, hide=False)

    dnsentries = []
    dnsentries_ext = []
    lstart = False
    lend = False
    for line in r.stdout.strip().split("\n"):
        # skip empty lines
        if not line:
            continue
        if line == ";; Internal DNS zone configuration":
            lstart = True
            continue
        if line == ";; External DNS zone configuration":
            lend = True
            continue
        if lstart and not lend:
            dnsentries.append(line)
        if lend:
            dnsentries_ext.append(line)

    for e in dnsentries:
        print(f"-- {e}")
        gandiapi.insert_record(domain, e)

    if len(dnsentries_ext) > 0:
        print("\n;; External DNS zone configuration")
    for line in dnsentries_ext:
        print(line)


def fparse_email(ns, config, dnsgit, gandiapi) -> None:
    """
    emailconfig --setup <subdomain>
    emailconfig --check <subdomain>
    """
    # print(ns)
    if ns.checkdns is not None:
        if ns.checkdns[0] == "all":
            print("-- Looping over all domains recorded in {}".format(config["EMAILRELAY_LISTOFDOMAINS"]))

            # Retrieve list of domains
            fd = io.BytesIO()
            c = fabric.Connection(config["EMAILRELAY_SSH"])
            c.get(config["EMAILRELAY_LISTOFDOMAINS"], fd)
            content = fd.getvalue().decode("utf-8")

            # parse it
            for d in content.split("\n"):
                domain = d.replace("\t", " ").replace("@", "").split(" ")[0]
                if len(domain) > 0:
                    print(f"\n-- {domain}")
                    check_email_config(domain, selector=ns.dkimselector, dnsonly=True)
        else:
            check_email_config(ns.checkdns[0], selector=ns.dkimselector, dnsonly=True)
    elif ns.checkfull is not None:
        check_email_config(ns.checkfull[0], selector=ns.dkimselector, dnsonly=False)
    elif ns.setup is not None:
        serveur_ips = config.get("SERVER_IPS", None)
        if serveur_ips:
            serveur_ips = serveur_ips[ns.server]
        setup_email_config(ns.setup[0], ns.setup[1], serveur_ips)


if __name__ == "__main__":
    # ==== Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="Path of the .env file")
    subparsers = parser.add_subparsers(help="Main categories")

    # ==== subparser config
    CFG_HELP = """file: display the path of .env file
url: display the URL of the remote password store/git repository
store: display the local path of the password store/git repository
show: display the content of the .env file
"""

    parser_config = subparsers.add_parser(
        "config",
        help="Display configuration info",
        formatter_class=RawTextHelpFormatter,
    )
    parser_config.add_argument(
        "action",
        choices=["file", "url", "store", "show"],
        help=textwrap.dedent(CFG_HELP),
    )
    parser_config.set_defaults(func=fparse_config)

    # ==== subparser dnszone

    parser_dnszone = subparsers.add_parser("dnszone", help="Check/snapshot/edit a DNS zone")
    pd_g1 = parser_dnszone.add_mutually_exclusive_group(required=False)

    pd_g1.add_argument(
        "--list",
        help="List the dates of snapshots of the zones. Can be used with 'all' as an argument to list all zones",
        nargs="*",
        metavar="domain",
    )
    pd_g1.add_argument(
        "--snapshot",
        help="Snapshot one or more DNS zones. Can be used with 'all' as an argument to snapshot all zones",
        nargs="*",
        metavar="domain",
    )
    pd_g1.add_argument(
        "--check",
        help="Compare the current DNS zone with the latest snapshot",
        nargs="*",
        metavar="domain",
    )
    pd_g1.add_argument(
        "--show",
        help="Show a DNS zone for a (sub) domain, at a given date",
        nargs=2,
        metavar=("(sub)domain,", "<date>|'current'"),
    )
    parser_dnszone.add_argument("--push", help="Push snapshot to remote", action="store_true")
    parser_dnszone.add_argument("--pull", help="Pull snapshot from remote", action="store_true")

    parser_dnszone.set_defaults(func=fparse_dnszone)

    # ==== subparser emailconfig

    parser_email = subparsers.add_parser(
        "emailconfig",
        help="Check/setup/edit a DNS email configuration",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    pe_g1 = parser_email.add_mutually_exclusive_group(required=True)

    pe_g1.add_argument(
        "--checkdns",
        help="Compare only the current DNS configuration (SPF, DKIM, DMARC)\n"
        "This is not a full test\n"
        "If domain is set to all, we iterate over all domains recorded in the (remote) file EMAILRELAY_LISTOFDOMAINS",
        nargs=1,
        metavar="domain",
    )
    pe_g1.add_argument(
        "--checkfull",
        help="Compare only the current DNS configuration - THIS is not a full test",
        nargs=1,
        metavar="domain",
    )
    pe_g1.add_argument(
        "--setup",
        help="Compare the current DNS configuration",
        nargs=2,
        metavar=("domain", "cname"),
    )
    parser_email.add_argument(
        "--dkimselector",
        default="mail",
        metavar="SELECTOR",
        help="DKIM selector to query by DNS",
    )
    parser_email.add_argument(
        "--server",
        default="mutu2",
        metavar="server",
        help="server name to be installed",
    )
    parser_email.set_defaults(func=fparse_email)

    # ====

    # Parse config
    args = parser.parse_args()

    # Handle --config
    if args.config is None:
        args.config = SECRET_ENV

    # ==== Initialize script
    config = load_env(args.config)
    dnsgit = DNSGitStore(config["DNS_GIT_REPOSITORY"])
    gandiapi = GandiAPI(config["GANDI_APIKEY"])

    # Debug Zone
    # setup_email_config("nuage.aarse.fr", "nc.aarse.girofle.cloud")
    # gandiapi.insert_record('girofle.cloud', 'coucou4 300 IN TXT "lalala1"')
    # sys.exit(1)

    # Main function
    try:
        func = args.func
    except Exception:
        parser.print_help()
        sys.exit(0)

    args.func(args, config, dnsgit, gandiapi)  # Main function
