from scripts.email import dmarcreportanalyzer
import gzip


def test_parse_report_yahoo_hubspot():
    with gzip.open("dmarc-report-yahoo-hubspot.xml.gz", 'rb') as f:
        report = f.read()

    parsed_reports = dmarcreportanalyzer.parse_report(report)
    assert len(parsed_reports) == 1

    first_report = parsed_reports[0]
    assert first_report["count"] == '1'
    assert "ip_source" in first_report
    assert "ip_source_correct" == "pass"


def test_parse_report_andra():
    with gzip.open("dmarc-report-andra.fr.xml.gz", 'rb') as f:
        report = f.read()

    parsed_reports = dmarcreportanalyzer.parse_report(report)
    assert len(parsed_reports) == 1

    first_report = parsed_reports[0]
    assert first_report["count"] == '1'
    assert "ip_source" in first_report
    assert "ip_source_correct" == "pass"
