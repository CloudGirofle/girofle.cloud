#!/bin/bash
# Script pour extraire des logs d'un postfix dans docker
# Et pour l'envoyer à Omailgw
# À mettre dans une crontab toutes les 5 min

# abort on error
set -e
LOCK_FILE="/var/run/$(basename "$0").lock"
OMAILGW_API_LOG_FILE="/home/girofle/omailgw_debug.log"
LOCK_FILE_MAX_AGE="$((5*60))"		# En secondes
SLICE_SIZE=900				# En secondes. Combien de secondes de logs je vais filtrer puis pusher
tailfile="/home/debian/mailcow_postfix_tail.txt"
DA="$(tail -n 1 $tailfile)"
now="$EPOCHSECONDS"
delta_s="$(( now-DA ))"
IID="$(openssl rand -hex 4)"
# Timezone locale, en supposant que mailcow ai la même
LTZ="$(date "+%Z")"
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function error_on_exit() {
    echo "Something went wrong on line $1"
    echo "Tail of $OMAILGW_API_LOG_FILE:"
    tail $OMAILGW_API_LOG_FILE
    exit 1
}
trap 'error_on_exit ${LINENO}' ERR
function disp() {
	printf "[%s] [%s] %s\n" "$IID" "$(date)" "$@" >&2
}
function edit_date() {
	while read -r data; do
		#{ read olddate ; read endofline; } < <(sed -re 's/^(.{15}) (.*)/\1\n\2/'<<<"$data")
		local olddate="$(cut -c -15 <<< "$data")"
		local endofline="$(cut -c 16- <<< "$data")"
		# Oct  9 17:22:03, en UTC please
		local newdate="$(date -u --date="${olddate}${LTZ}" "+%b %d %H:%M:%S")"
		echo "$newdate" "$endofline"
		#local newdate="$(date --date="$olddate 1 hour ago" "+%b %d %H:%M:%S")"
	done
}
function i_am_the_one() {
	if [ -e "$LOCK_FILE" ] ; then
		local age="$(bc <<< "$now - $(stat -c '%Y' "$LOCK_FILE" )" )"
		if [ -n "$( ps -h $(<$LOCK_FILE))" ] ; then	# le lock existe, le pid aussi
			disp "I'm existing since ${age}s under pid $(<$LOCK_FILE)"
			exit 1
		else
			if [ "$age" -gt "$LOCK_FILE_MAX_AGE" ] ; then		# trop vieux, remplacement
				disp "Stall lock file (pid $(<$LOCK_FILE) does not exists and lock file older than ${LOCK_FILE_MAX_AGE}s), removing"
			else
				disp "Stall lock file but not old enough (${age}s < ${LOCK_FILE_MAX_AGE}s), exiting"	# pas assez vieux, let's wait
				exit 1
			fi
		fi
	fi
	echo "$$" > "$LOCK_FILE" 
	#trap "rm -f $LOCK_FILE" 0
}
# read last date
#DA_new=$(date --rfc-3339=seconds | sed 's/ /T/') # Current timestamp
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

i_am_the_one

cd /opt/mailcow-dockerized

# db
#DA="2023-10-26 10:35:18+02:00"
#docker compose logs postfix-mailcow --since $DA | cut -c 40- | edit_date
disp "Starting form $(date -d @$DA)"
# taille d'une tranche de temps
maxslice="$((1+delta_s/SLICE_SIZE))"
disp "Parsing ${delta_s}s in $maxslice slices of ${SLICE_SIZE}s"
t="$(mktemp /tmp/omailgw.XXXXXX)" ; trap "rm -f $t" 0
for ((from=delta_s;from>0;from=from-SLICE_SIZE)) ;do	# Je vais remonter du passé vers maintenant par tranches de SLICE_SIZE secondes
	((nslice+=1))
	until="$((from-SLICE_SIZE))"
	disp "#$nslice/$maxslice ($((100*nslice/maxslice))%): from $(date -d @$((now-from)) '+%d/%m: %H:%M:%S' ) until $(date -d @$((now-until)) '+%H:%M:%S')"
	start="$SECONDS"
	disp "   filtering"
	docker compose logs postfix-mailcow --since ${from}s --until ${until}s | cut -c 22- | grep -Ev "^Uptime:|^postmap|Checking if ASN for your IP is listed for Spamhaus Bad ASN List...|The AS of your IP is listed as a banned AS from Spamhaus\!|No SPAMHAUS_DQS_KEY found... Skipping Spamhaus blocklists entirely\!|^chown|^Waiting" | edit_date > "$t"
	disp "   pushing $(wc -l "$t" | cut -d " " -f 1) lines"
	cat "$t" | php /home/debian/omailgw-cli/omailgw-cli.php -vv --log=stdin >> "$OMAILGW_API_LOG_FILE" 2>&1
	duration="$(( SECONDS - start ))"
	speed=$(( SECONDS/nslice  ))		# temps moyen pour traiter une tranche
	disp "   done in ${duration}s. avg: ${speed}s/slice. ETA: $(date -d "now + $(( speed*(maxslice-nslice) )) seconds")"
	# save last timestamp + le temps mis par le script depuis son démarrage
	# ca permet, en cas de reprise, de recommencer au bon endroit
	ts=$(date -d "$((until+SECONDS)) seconds ago" +%s)
	if [ $ts -gt $now ] ; then
		echo $now >> "$tailfile"
	else
		echo $ts >> "$tailfile"
	fi
done
rm -f "$LOCK_FILE"
# Pour debug
# et au cas où plusieurs jours auraient été loupés (le script perl est bien plus rapide)
#docker compose logs postfix-mailcow --since ${delta_s}s | /root/edit_date | tee /tmp/omailgw-cli.$(date +%s).log | sponge | php /home/debian/omailgw-cli/omailgw-cli.php -vv --log=stdin >> /home/girofle/omailgw_debug.log 2>&1
# One line
# Mais ca scotch "read -r data"
#docker compose logs postfix-mailcow --since ${delta_s}s | cut -c 26- | grep -Ev "^Uptime:|^postmap|Checking if ASN for your IP is listed for Spamhaus Bad ASN List...|The AS of your IP is listed as a banned AS from Spamhaus\!|No SPAMHAUS_DQS_KEY found... Skipping Spamhaus blocklists entirely\!|^chown|^Waiting" | edit_date | sponge | php /home/debian/omailgw-cli/omailgw-cli.php -vv --log=stdin >> /home/girofle/omailgw_debug.log 2>&1
# Donc, un fichier temporaire au milieu
