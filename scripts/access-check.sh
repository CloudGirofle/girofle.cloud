#!/bin/bash
# List SSH keys that have access to the server

for i in "/root" `ls -d /home/*`; do
    KEY="${i}/.ssh/authorized_keys"
    if [[ -f ${KEY} ]]; then
	echo "-- $i"
	cat ${KEY} | cut --delimiter=" " -f3
    fi
    
done
