from os import listdir
from os.path import isfile, join
import itertools

INPUT_FOLDER = "/home/keoma/php/"


if __name__ == '__main__':
    files = [f for f in listdir(INPUT_FOLDER) if isfile(join(INPUT_FOLDER, f))]

    processes = {}
    sections = []
    in_section = False

    for f in files:
        current_section = []
        #print(f"############ {f}")
        with open(INPUT_FOLDER + f, "r") as input_file:
            for line in input_file:
                if line == "************************\n":
                    in_section = True
                    if current_section:
                        sections.append(current_section)
                        current_section = []  # reset
                else:
                    if not in_section:
                        continue
                    else:
                        if line != "\n":
                            current_section.append(line)

    for section in sections:
        d = {}
        for line in section:
            try:
                k, v = line.split(":", 1)
                d[k] = v.strip()
            except Exception as e:
                print(f">>>> {line} {e}")

        if "pid" in d:
            if d["pid"] not in processes:
                processes[d["pid"]] = []
            processes[d["pid"]].append(d)

    scripts = {}
    for pid, process in processes.items():
        max_cpu = 0
        max_memory = 0

        for item in process:
            if float(item["last request cpu"]) > max_cpu:
                max_cpu = float(item["last request cpu"])
            if float(item["last request memory"]) > max_memory:
                max_memory = float(item["last request memory"])
            if item["script"] not in scripts:
                scripts[item["script"]] = 1
            else:
                scripts[item["script"]] += 1

        max_cpu = max_cpu / 1000000
        max_memory = max_memory / 1000000
        print(f"pid: {pid} - {item['script']} - mcpu: {round(max_cpu)} - mmem: {round(max_memory)}")

    for s, v in sorted(scripts.items(), key=lambda item: item[1], reverse=True)[:10]:
        print("%-60s%-60s" % (s, v))



