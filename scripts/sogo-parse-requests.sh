#!/usr/bin/env bash
# Parse un fichier de log de sogo
# Affiche la taille des requêtes, en ko
size=0
while [ -n "$1" ] ; do
	case $1 in
		-s)		size="$2"; shift;;
		-h)		echo "$0 [ -s <taille min> ] fichier | <stdin>" ;;
		*)		file="$1";;
	esac
shift
done
awk -F'"' -v minsize=$size '/"(GET|POST|PUT|REPORT|PROPFIND|OPTIONS|HEAD) /{split($3,a," ");split($1,d," ");s=a[6];if (match(a[6],/K/)){ s=substr(a[6],0,RSTART-1)};if (match(a[6],/M/)){ s=1024*substr(a[6],0,RSTART-1)};da=sprintf("date -d \"%s %s %s\" +%%H:%%M:%%S ",d[3],d[4],d[5]);da | getline ts;close(da);split($2,r," ");if (s > minsize) {printf "%i\t%s\t%s\t%s\n",s,ts,r[1],r[2]}}' "$file"
