## Import a Mattermost instance
## MW, GPLv3+, mar 2022

USAGE="usage: sudo ./import_mattermost.sh <backup-file-archive>"

## Run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# abort on error
set -e
function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR

if [[ $1 == "" ]]
then
    echo "Please enter the backup file you want to restore. ${USAGE}"
    exit 1
fi

########## CONSTANTS ##########
BACKUP_FILE="${1}"
BACKUP_DIR="${BACKUP_FILE#*.tar.gz}"

# untar
tar xzf "${BACKUP_FILE}"

# stop mattermost
service mattermost stop

# backup database & config.json
mysqldump -u root mattermost > "mm_vanilla_$(date +%Y%m%d).sql"
cp /opt/mattermost/config/config.json config.vanilla.json

# drop & recreate existing database
mysql -u root -e "drop database mattermost;"
mysql -u root -e "create database mattermost;"

# import database
mysql mattermost < "${BACKUP_DIR}/mattermost.sql"

# Copy data
cp -r "${BACKUP_DIR}/data/" /opt/mattermost/data
chown -R mattermost:mattermost /opt/mattermost/data/

# Copy config
cp "${BACKUP_DIR}/config.json" /opt/mattermost/config/config.json
chown -R mattermost:mattermost /opt/mattermost/config/config.json

# Start service
sudo service mattermost start
