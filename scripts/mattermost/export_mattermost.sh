## A small script to back up a Mattermost server
## Source : https://docs.mattermost.com/deploy/backup-disaster-recovery.html
## By MW GPLv3+
# usage: sudo ./export_mattermost.sh

BACKUP_DIR="mattermost_backup_`date +"%Y%m%d"`"
MATTERMOST_ROOT_DIR="/opt/mattermost"

## Run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# abort on error
set -e
function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR

# Create backup dir
mkdir "${BACKUP_DIR}"

# Backup database
mysqldump -u root mattermost > "${BACKUP_DIR}/mattermost.sql"

# Backup config
cp "${MATTERMOST_ROOT_DIR}/config/config.json" "${BACKUP_DIR}"

# Backup data
cp -r "${MATTERMOST_ROOT_DIR}/data/" "${BACKUP_DIR}"

# zip all
tar -czf "${BACKUP_DIR}.tar.gz" "${BACKUP_DIR}"
