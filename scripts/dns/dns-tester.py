#!/usr/bin/python3
# By CG, GPLv3+
# Script to test whether a domain is blacklisted under various DNS resolvers
import pandas as pd
import dns.resolver
import os
my_resolver = dns.resolver.Resolver()
reference_resolver = 'FDN'

THE_DB = {}  # a key-value dict to store all the lookups

resolvers_file = './dns-resolvers.csv'
domains_file = '/root/.the-watcher-prometheus.rc'
domains_file2 = '/etc/postfix/sender_login_maps'

extra_domains = ['forum.girofle.cloud', 'mail.girofle.org']


def get_list_of_resolvers():
    r = pd.read_csv(os.path.join(__location__, resolvers_file))
    c = ['IPv4', 'IPv4bis', 'IPv6', 'IPv6bis', 'Organization', 'Remark']
    if list(r.columns) != c:
        raise IOError("Columns names of the CSV file do not match the expected {}".format(str(c)))
    return r


def get_list_of_domains():
    domains = extra_domains.copy()

    # Add NC instances
    with open(domains_file, 'r') as f:
        domains += [l.replace("/", ";").split(";")[0] for l in f.readlines() if not l.startswith('#')]

    # Add instances registered in email
    with open(domains_file2, 'r') as f:
        domains += [l.replace('\t', ' ').split(" ")[0][1:] for l in f.readlines() if not l.startswith("#")]

    domains = list(set(domains))
    return domains


def get_true_resolution(d, resolvers):
    if d not in THE_DB:
        r4 = resolvers.IPv4[resolvers.Organization == reference_resolver][0]
        #ipv6 = resolvers.IPv6[resolvers.Organization == reference_resolver][0]
        my_resolver.nameservers = [r4]
        try:
            ipv4 = my_resolver.resolve(d, 'a')
        except:
            ipv4 = ["not resolved"]
        try:
            ipv6 = my_resolver.resolve(d, 'aaaa')
        except:
            ipv6 = ["not resolved"]
        THE_DB[d] = {reference_resolver: [str(ipv4[0]), str(ipv6[0])]}
        return THE_DB[d]


def test_domain(d, resolvers, ipv4=True, ipv6=False, ipv4bis=False, ipv6bis=False):
    # Test if we have reference A/AAAA fields for the domain
    ref = get_true_resolution(d, resolvers)
    ref_a = ref[reference_resolver][0]
    ref_aaaa = ref[reference_resolver][1]

    # Generate list of resolvers to test
    df_ip = resolvers.iloc[:, :5].melt(id_vars=['Organization']).dropna()
    list_of_ip = []
    if not ipv4:
        df_ip = df_ip[df_ip.variable != 'IPv4']
    if not ipv4bis:
        df_ip = df_ip[df_ip.variable != 'IPv4bis']
    if not ipv6:
        df_ip = df_ip[df_ip.variable != 'IPv6']
    if not ipv6bis:
        df_ip = df_ip[df_ip.variable != 'IPv6bis']

    # Iterate over resolvers
    a_lookup = []
    aaaa_lookup = []
    allok = True
    for l in df_ip.iterrows():
        r = l[1].value
        rname = l[1].Organization
        if rname == reference_resolver:
            a_lookup.append(ref_a)
            aaaa_lookup.append(ref_aaaa)
            continue

        my_resolver.nameservers = [r]

        try: # check A entry
            a = str(my_resolver.resolve(d, 'a')[0])
            if a=='n':
                print(my_resolver.resolve(d, 'a'))
        except:
            allok = False
            print("{}:\t[A] resolution failed for {}".format(d, rname))
            a = 'failed'

        try: # check AAAA entry
            aaaa = str(my_resolver.resolve(d, 'aaaa')[0])
        except:
            allok = False
            print("{}:\t[AAAA] resolution failed for {}".format(d, rname))
            aaaa = 'failed'

        a_lookup.append(a)
        aaaa_lookup.append(aaaa)
        if (a!='failed' and a != ref_a) or (aaaa != 'failed' and aaaa != ref_aaaa):
            print("{}:\tDifference in resolution found for resolver {} ({}). Resolves to {} / {}".format(d, rname, r, a, aaaa))
    if allok:
        print("OK:\t{}".format(d))

    df_ip['a'] = a_lookup
    df_ip['aaaa'] = aaaa_lookup
    THE_DB[d]['resolutions'] = df_ip

if __name__ == "__main__":
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    resolvers = get_list_of_resolvers()
    domains = get_list_of_domains()
    for d in domains:
        test_domain(d, resolvers)

    #print(get_list_of_resolvers())
