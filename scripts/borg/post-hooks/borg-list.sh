#!/bin/bash
# list the existing backups, so that a restore can be prepared
set -e
function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR


[[ -z "${BORG_EXEC}" ]] && BORG_EXEC=borg
[[ -z "${BORGLIST_REPORT}" ]] && BORGLIST_REPORT="/var/log/borg_list.log"

echo "-- List of archives for $(date)" >> ${BORGLIST_REPORT}
BORG_RSH="ssh ${SSH_OPTIONS}"
${BORG_EXEC} list $BORG_REPO --rsh "${BORG_RSH}" $BORG_OPTIONS >> $BORGLIST_REPORT #::$(date +%Y%m%d-%Hh)-${BACKUP_NAME}-mysqldump $DB_TMP_FILE  # create borg
