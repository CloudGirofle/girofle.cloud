#!/bin/bash
# Script to check if we should initiate a backup
# To be put in a crontab
#
# Files located in pre-hook/ or post-hook directories will
# be executed unless if the filename is ending with .off

set -e # abort on error
trap 'logerror $LINENO' ERR
export PS4="> [\$FUNCNAME]@\$LINENO "

function logerror() {
    echo "backupfailed $(date +%d-%m-%Y\ %H:%M:%S) Line: $1" >> $pathLog
    exit 1
}

DIR_EXE_ABS=$(dirname $(realpath $0))
pathLock="/var/run/lock/borg_backup_api.lock"
pathRun="/var/run/lock/borg_backup_running.lock"
pathLog="/var/log/borg_api.info"
BACKUP="${DIR_EXE_ABS}/borg_backup.sh"
PRE_HOOKS_DIR="${DIR_EXE_ABS}/pre-hooks"
POST_HOOKS_DIR="${DIR_EXE_ABS}/post-hooks"

if [ -f $pathLock ] ; then
    if [ -f $pathRun ] ; then
        #echo "Borg backup running"
        exit 0
    else
        echo "backupstarted $(date +%d-%m-%Y\ %H:%M:%S)" >> $pathLog

        # create run lock
        touch $pathRun

        ## === We run the pre-hooks
	# moved to borg_backup
        #for h in ${PRE_HOOKS_DIR}/* ; do
        #    if [[ "$h" == *.off ]]; then continue; fi
        #    ${h}
        #done

        ## === We run the backup
        echo "BACKUP!"
        $BACKUP

        ## === We run the post-hooks
	# moved to borg_backup.sh
        #for h in ${POST_HOOKS_DIR}/*; do
        #    if [[ "$h" == *.off ]]; then continue; fi
        #    ${h}
        #done

        ## === We release the lock
        echo "backupterminated $(date +%d-%m-%Y\ %H:%M:%S)" >> $pathLog
        rm $pathRun
        rm $pathLock
    fi
fi
