<?php
// Simple API to create a lock file.
// By MW & KBL, Cloud Girofle, GPLv3+, Jul. 2021

//header("Content-Type:application/json");

// open syslog, include the process ID and also send
// the log to standard error, and use a user defined
// logging mechanism
openlog("borg_api.info", LOG_PID | LOG_PERROR, LOG_LOCAL0);

//Define constants
//$saltGET = "MySALTforAVerySecureCipher"; // this might leak in the logs
//$saltPOST = "MySALTforAVerySecureCipher"; // this will not
$retzoNasIp = "109.190.200.24";
$allowedIp = array("172.18.0.1", "81.66.156.234", $retzoNasIp);
$allowedClients = array("/backup/zici", "/backup/zici/");
$pathLog = "/var/log/borg_api.info";
$pathLock = "/var/run/lock/borg_backup_api.lock";

syslog(LOG_INFO, "Borg Api Called");

// Validate
// 0. make sure HTTPS is used (need to activate only when it actually is)
if( !isset($_SERVER['HTTPS'] ) ) {
    print("Access denied\n");
    syslog(LOG_WARNING, "Access denied - Not using HTTPS");
    return http_response_code(401);
}

// 1. make sure it has the right IP and
if (!in_array($_SERVER['REMOTE_ADDR'], $allowedIp)) {
    print("Access denied\n");
    syslog(LOG_WARNING, "Access denied - IP not allowed: " . $_SERVER['REMOTE_ADDR']);
    return http_response_code(401);
}

// 2. make sure it has the right hash
// if ($_SERVER['REQUEST_METHOD'] === 'POST') {
//     if (!(isset($_POST['securetoken']) && validatehash($saltPOST, $_POST['securetoken']))) {
//         print("Access denied\n");
//         return http_response_code(401);
//     }
// } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
//     if (!(isset($_GET['securetoken']) && validatehash($saltGET, $_GET['securetoken']))) {
//         print("Access denied\n");
//         return http_response_code(401);
//     }
// }

// 3. make sure the client asks a valid URL
if (!(in_array(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), $allowedClients))) {
    print("Access denied\n");
    syslog(LOG_WARNING, "Access denied - Invalied URL: " . $_SERVER['REQUEST_URI']);
    return http_response_code(401);
}

// NOW WE CAN RESPOND :)
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    syslog(LOG_INFO, "Signal received");
    if (file_exists($pathLock)) {
    $message = "File is present, aborting";
        print($message . "\n");
        syslog(LOG_WARNING, $message);
        return http_response_code(500);
    } else {
        syslog(LOG_INFO, "Creating Lock file");
    $retval = null;
    $output = null;
        exec("touch " . $pathLock, $output, $retval);
    if ($retval == 0){
            syslog(LOG_INFO, "Lock file created");
        print("All good. The backup should start soon.\n");
        return http_response_code(200);
    } else {
            syslog(LOG_WARNING, "Could not create Lock file:".print_r(output));
        return http_response_code(500);
    }
    }
    print "<br/>Youpi: this is a POST";
    // create the "pleasebackup" file
    // if the file does not exist: return 200
    // if the file exists: return 500
    // error code
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    exec("cat " . $pathLog . ' 2>&1', $a);
    print end($a);
    syslog(LOG_INFO, "Method GET called");
}

function validatehash($salt, $skt) { // Function that returns TRUE if the HASH is valid
    // skt: secure token
    if( $skt== "lol" ) {
        return true;
    } else {
        return false;
    }
};

closelog();

?>
