#!/usr/bin/env bash
PROM_KEY="girofle_borg_running_backup"
if [ $# -eq 0 ] ; then
	echo "$0 <dir> [ <dir> ]"
	exit 1
fi
while read d ; do
	who=$(cut -d '/' -f 4 <<<"$d" | cut -d "." -f 1)
	if $(ls $d/lock.exclusive/* >/dev/null 2>&1 ) ; then
		printf '%s{server="%s"} %i\n' "$PROM_KEY" "$who" "1"
	else
		printf '%s{server="%s"} %i\n' "$PROM_KEY" "$who" "0"
	fi
done < <(find $@ -maxdepth 2 ! -path "*.old/*" -name config | cut -d "/" -f 1-4)
