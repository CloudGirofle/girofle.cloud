#!/usr/bin/env bash
dirs=( /mnt/backup /mnt/backup2 )
for dir in ${dirs[@]} ; do
	for d in $(find $dir -maxdepth 1 -type d -name "*.cloud" ) ; do
		echo ">>> $d"
		if [ -t 1 ] ; then
			borg compact --progress "$d"
		else
			borg compact "$d"
		fi
	done
done

