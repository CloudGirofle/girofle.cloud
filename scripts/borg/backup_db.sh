#!/bin/bash
# By CG, Jun 2023, GPLv3+
# In general, nothing bad could happen if we run --prepare twice or even more times. Bad things could happen if we will try to apply incremental on top of the fully prepared backup.

# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong on line $1, exiting" >> ${log_file}
    exit 1
}
trap 'error_on_exit $LINENO' ERR

#### === ARGS ===
nb_rolling_backups=30
nb_open_files=16000
backup_dir=/var/lib/mysql.backup/
incr_dir=/home/girofle/incrbackup/
today="$(date '+%Y-%m-%d-%Hh%M')"
log_file=/var/log/mariabackup.log


function logme() {
    echo $1
    logger --no-act -s "$1" 2>&1 | sed 's/^<[0-9]\+>//' >> ${log_file}
}

function initial_backup() {
    local main=$1

    # Check ulimits
    if [[ $(ulimit -n) -lt $nb_open_files ]]; then
    logme "WARNING Limit of number of files open is $(ulimit -n) < ${nb_open_files}."
    logme "ERROR This usually causes problems. ABORTING."
    exit 1
    fi

    # Run initial backup
    logme "Starting initial backup at $(date)"
    SECONDS=0
    mariabackup --backup -uroot --target-dir=${main} 2> ${log_file}
    logme "Finished initial backup at $(date) (duration ${SECONDS}s)"

}


function incremental_backup() {
    local main=$1
    local incr=$2

    # Perform incremental backup
    logme "Starting incremental backup at $(date)"
    SECONDS=0
    mariabackup --backup -uroot --target-dir=${incr} --incremental-basedir=${main} \
        --parallel=4 2> /dev/null
    logme "Finished incremental backup at $(date) (duration ${SECONDS}s)"
}

function backup() {
    logme "${0} started"
    # Check if we should roll the backups
    local backupfolder=$1
    local nfolders=$(find ${backupfolder} -maxdepth 1 -type d | wc -l)

    if [[ ${nfolders} -gt $nb_rolling_backups ]]; then # If we need to roll backups
        logme "Deleting all mysql backups and creating a fresh one"
        rm -rf ${backupfolder}/*
        initial_backup ${backupfolder}/${today}
        ln -s ${backupfolder}/${today} ${backupfolder}/last
    elif [[ ${nfolders} -eq 1 ]]; then # If we initiate the backup
        initial_backup ${backupfolder}/${today}
        ln -s ${backupfolder}/${today} ${backupfolder}/last
    else
        if [[ ! -L ${backupfolder}/last ]] ||  [[ ! -e  ${backupfolder}/last ]]; then
            logme "pointer ${backupfolder}/last does not exist, aborting"
            exit 1;
        fi
        incremental_backup ${backupfolder}/last ${backupfolder}/${today}
        rm ${backupfolder}/last
        ln -s ${backupfolder}/${today} ${backupfolder}/last
    fi
}

case $1 in
    "initial") initial_backup ${backup_dir} ;;
    "incremental") incremental_backup ${backup_dir} ${incr_dir} ;;
    "backup") backup ${backup_dir} ;; # decide whether we want incremental or not
    *) echo "??"; exit 1;;
esac
