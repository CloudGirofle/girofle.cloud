#!/bin/bash
# By CloudGirofle, GPLv3+

###### CATCH ERRORS ######

set -o errexit  # set -e; Exit if a command fail
set -o errtrace # set -E; Catch command fail inside functions too
set -o pipefail # pour que ça fail même derrière un | log
export PS4="> [\$FUNCNAME]@\$LINENO "

###### ENVS ######
DIR_EXE_ABS="$(dirname "$(realpath "$0")")"

## Check that required environment variables are set
[[ -z "${BORG_PASSPHRASE}" ]] && exit 1
[[ -z "${BACKUP_TARGETS}" ]]  && exit 1
[[ -z "${BACKUP_NAME}" ]]     && exit 1

## Check that other environment variables are set or use default values
[[ -z "${BORG_EXEC}" ]] && BORG_EXEC=borg
[[ -z "${BACKUP_SERVER}" ]] && BACKUP_SERVER="root@backup.girofle.cloud"
[[ -z "${BACKUP_SERVER_PORT}" ]] && BACKUP_SERVER_PORT=22
[[ -z "${BORG_REPO}" ]]     && BORG_REPO="$BACKUP_SERVER:backup/"
[[ -z "${BORG_REPORT}" ]]   && BORG_REPORT="/tmp/.borg_report"
[[ -z "${MAIL_FROM}" ]]     && MAIL_FROM="borg-mutu@girofle.cloud"
[[ -z "${USE_SENDMAIL}" ]]  && USE_SENDMAIL=false
[[ -z "${NC_BASE_DIR}" ]]   && NC_BASE_DIR="/var/www/"
[[ -z "${DB_TMP_FILE}" ]]   && DB_TMP_FILE=/tmp/backup_"$(date +%Y%m%d-%Hh)".bak.gz
[[ -z "${DB_POSTGRES_TMP_FILE}" ]]   && DB_POSTGRES_TMP_FILE=/tmp/backup_postgres_"$(date +%Y%m%d-%Hh)".bak.gz
[[ -z "${MARIABACKUP_FOLDER}" ]]   && MARIABACKUP_FOLDER=/var/lib/mysql.backup
[[ -z "${DB_DUMP}" ]] || [[ -z "${DB_MYSQL_DUMP}" ]]  && DB_MYSQL_DUMP=false
[[ -z "${DB_POSTGRES_DUMP}" ]] && DB_POSTGRES_DUMP=false
[[ -z "${DB_MARIABACKUP}" ]] && DB_MARIABACKUP=false
[[ -z "${BORG_LOG}" ]]      && BORG_LOG=/var/log/borg.info
[[ -z "${PRE_HOOKS_DIR}" ]]      && PRE_HOOKS_DIR="${DIR_EXE_ABS}/pre-hooks"
[[ -z "${POST_HOOKS_DIR}" ]]      && POST_HOOKS_DIR="${DIR_EXE_ABS}/post-hooks"
[[ -z "${MAILTO}" ]]        && MAILTO="girofle@tech.girofle.cloud"
# "ssh://borg@$DESTINATION.lan/mnt/backup/mutu3.girofle.cloud/" => /mnt/backup/mutu3.girofle.cloud/
BACKUP_DIR="$(cut -d '/' -f 4- <<<"$BORG_REPO")"

function send_error_mail () {
	exitcode=$?

	if [[ $exitcode -ne 0 ]]; then
		clean_up
		echo "==============> BORG BACKUP FAILED <=================" >> "$BORG_REPORT"
		echo "Bort Backup failed at $(date)" >> "$BORG_REPORT"
		if [[ "$USE_SENDMAIL" = false ]]; then
			mail -s "backup report" "$MAILTO" < "$BORG_REPORT"
		else
			sendmail -v $MAILTO < "$BORG_REPORT"
		fi
		#else
		#    cat "$BORG_REPORT" | mail -s "mutu backup report" "$MAILTO"
	fi
}
function log() {
	local m
	if ! [ -t 0 ] ; then	# Permet: 'echo plop | log' ET 'log "plop"'
		m="$(cat)"
		printf '%s\n' "$m" | tee -a "$BORG_REPORT"
	else
		m="$1"
		printf '[%s] %s\n' "$(date)" "$m" | tee -a "$BORG_REPORT"
	fi
}
function clean_up() {
	if [[ "$DB_DUMP" = true ]]; then
		log "Removing $DB_TMP_FILE"
		rm -f "$DB_TMP_FILE"
	fi
	if [[ "$DB_POSTGRES_DUMP" = true ]] ; then
		log "Removing $DB_POSTGRES_TMP_FILE"
		rm -f "$DB_POSTGRES_TMP_FILE"
	fi
}
trap send_error_mail EXIT # Capturing exit signals on shell script
#rm -f "$BORG_REPORT"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BORG_RSH="ssh ${SSH_OPTIONS}"
DB_BACKUP_EXE="${DIR_EXE_ABS}/backup_db.sh"

###### PREPARE FOR BACKUP ######

## Prepare backup report
{
    echo "Subject: Borg Backup report"
    echo "From: $MAIL_FROM"
    echo ""
} | log
log "Starting backup"

# Then, let's make some checks
MSG_HDR="$(ssh $SSH_OPTIONS $BACKUP_SERVER -p $BACKUP_SERVER_PORT df -h / | head -1)"  || log "Can't get disk usage"
MSG_FRE="$(ssh $SSH_OPTIONS $BACKUP_SERVER -p $BACKUP_SERVER_PORT df -h . | tail -1 )" || log "Can't get free space for /home" # How much free space on the target?
MSG_BKP="$(ssh $SSH_OPTIONS $BACKUP_SERVER -p $BACKUP_SERVER_PORT df -h "$BACKUP_DIR" | tail -1 )" || log "Can't get free space for $BACKUP_DIR" # How much free space on the target?

{
    echo "-- Free space"
    echo "$MSG_HDR"
    echo "$MSG_FRE"
    echo "$MSG_BKP"
} | log

## Run pre-hooks
for h in "${PRE_HOOKS_DIR}"/* ; do
    if [[ "$h" == *.off ]]; then continue; fi
    log "-- Running script $h"
    ${h} | log
    log "-- End of script $h"
done


## Dump the Database
if [[ "$DB_MYSQL_DUMP" = true ]]; then
    echo "Create a Mysql dump file of all databases" >> $BORG_REPORT
    sudo mysqldump --single-transaction --all-databases > "$DB_TMP_FILE"
else
    echo "NOT Creating a Mysql dump file of all databases" >> $BORG_REPORT
fi

## Dump the Postgres Database
if [[ "$DB_POSTGRES_DUMP" = true ]]; then
    log "Create a postgres dump file of all databases"
    # Evite le "could not change directory to "/root": Permission denied"
    ( cd /tmp ; sudo -u postgres pg_dumpall | gzip > "$DB_POSTGRES_TMP_FILE" )
else
    log "NOT Creating a postgres dump file of all databases"
fi

## Mariabackup the Database
if [[ "$DB_MARIABACKUP" = true ]]; then
    log "Create a mariabackup of all databases"
    ulimit -n 16000 && ${DB_BACKUP_EXE} backup | log
else
    log "NOT Creating a mariabackup file of all databases"
fi


###### RUN BACKUP ######

log "-- Performing Borg backup"
log "The following paths will be backup'd: $BACKUP_TARGETS"
log "The Borg RSH is the following ${BORG_RSH}"
log "The Borg repo is the following ${BORG_REPO}"

for nm_tgt in $BACKUP_TARGETS; do
    IFS=":" read -r nm tgt <<< "${nm_tgt}"
    if [[ "$nm" != "db" ]] || [[ "$DB_MYSQL_DUMP" = true ]]; then
	log "Backing up ${nm} at $(date)"
	${BORG_EXEC} create $BORG_REPO::$(date +%Y%m%d-%Hh)-$nm $tgt --lock-wait 360 --rsh "${BORG_RSH}" $BORG_OPTIONS --stats 2>&1 | log # create borg backup archive
    fi
done

# Mysql Dump
if [[ "$DB_MYSQL_DUMP" = true ]]; then
    if [[ -f "$DB_TMP_FILE" ]]; then
        log "$DB_TMP_FILE exists."
    else
        log "$DB_TMP_FILE does not exist. Exiting."
        exit 1
    fi
    ${BORG_EXEC} create $BORG_REPO::$(date +%Y%m%d-%Hh)-${BACKUP_NAME}-mysqldump $DB_TMP_FILE --rsh "${BORG_RSH}" $BORG_OPTIONS --stats 2>&1 | log # create borg backup archive
fi

# PostgreSQL Dump
if [[ "$DB_POSTGRES_DUMP" = true ]]; then
    if [[ -f "$DB_POSTGRES_TMP_FILE" ]]; then
        log "$DB_POSTGRES_TMP_FILE exists."
    else
        log "$DB_POSTGRES_TMP_FILE does not exist. Exiting."
        exit 1
    fi
    ${BORG_EXEC} create $BORG_REPO::$(date +%Y%m%d-%Hh)-${BACKUP_NAME}-postgresdump $DB_POSTGRES_TMP_FILE --rsh "${BORG_RSH}" $BORG_OPTIONS --stats 2>&1 | log # create borg backup archive
fi

# MariaDB Backup
if [[ "$DB_MARIABACKUP" = true ]]; then
    ${BORG_EXEC} create $BORG_REPO::$(date +%Y%m%d-%Hh)-${BACKUP_NAME}-mariabackup $MARIABACKUP_FOLDER --rsh "${BORG_RSH}" $BORG_OPTIONS --stats 2>&1 | log # create borg backup archive
fi


#echo "NOT Pruning backups at $(date)"  >> $BORG_REPORT
log "Pruning backups"
if [ "$(hostname)" != "mutu2.girofle.cloud" ] ; then
	for nm_tgt in $BACKUP_TARGETS "mysqldump:lala" "mariabackup:lala" ; do
		nm=$(cut -d ':' -f1 <<< "${nm_tgt}")
		${BORG_EXEC} prune --glob-archives='*-'$nm --keep-within=31d --keep-weekly=13 $BORG_REPO --rsh="${BORG_RSH}"
	done
else
	log "... but not on mutu2, for now"
fi
case "$(${BORG_EXEC} --version | cut -d ' ' -f 2)" in
	1.2*)
		log "Compacting free space"
		${BORG_EXEC} compact --rsh="${BORG_RSH}" $BORG_REPO
		;;
	1.1*)
		log "Borg 1.1 doesn't compact"
		;;
esac

## Post-hook
for h in ${POST_HOOKS_DIR}/*; do
    if [[ "$h" == *.off ]]; then continue; fi
    log "Running post-hook $h."
    ${h}
done


## all done!
log "Backup complete"

###### CLEANING ######

if [[ "$DB_MYSQL_DUMP" = true ]]; then
    ## Remove local Database backup
    log "Removing temporary db dump file."
    rm -f "$DB_TMP_FILE"
fi

if [[ "$DB_POSTGRES_DUMP" = true ]]; then
    ## Remove local Postgres Database backup
    log "Removing temporary postgres db dump file."
    rm "$DB_POSTGRES_TMP_FILE"
fi

###### SEND REPORT ######

## Finally, let's see if it worked
log "-- New free space"
MSG_HDR2="$(ssh $SSH_OPTIONS "$BACKUP_SERVER" -p "$BACKUP_SERVER_PORT" df -h | head -n 1)"  || echo "Can't get disk usage"
MSG_FRE2="$(ssh $SSH_OPTIONS "$BACKUP_SERVER" -p "$BACKUP_SERVER_PORT" df -h . | tail -1 )" || echo "Can't get free space for /home" # How much free space on the target?
MSG_BKP2="$(ssh $SSH_OPTIONS "$BACKUP_SERVER" -p "$BACKUP_SERVER_PORT" df -h "$BACKUP_DIR" | tail -1 )" || echo "Can't get free space for $BACKUP_DIR" # How much free space on the target?
log "$MSG_HDR2"
log "$MSG_FRE2"
log "$MSG_BKP2"

# Send email
#if [[ "$USE_SENDMAIL" = false ]]; then
#    cat "$BORG_REPORT" | mail -s "backup report" "maxime@la-maison-bleue.org"
#else
#    cat "$BORG_REPORT" | sendmail -v maxime@la-maison-bleue.org
#fi

log "Backup terminated"
log "Borg Backup terminated successfully"
# Pour the-watcher
echo "$(date -Is -u) Borg Backup terminated successfully in $SECONDS seconds" >> ${BORG_LOG}
exit 0

###### LOVE ######
