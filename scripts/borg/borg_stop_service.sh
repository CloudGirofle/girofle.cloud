#!/bin/bash
# Script to disable a service before backup
# To be put in a crontab
#
# Service will be stoped before and restarted after


trap 'logerror $LINENO' ERR
function logerror() {
    echo "backupfailed $(date +%d-%m-%Y\ %H:%M:%S) Line: $1" >> $pathLog
    exit 1
}

SERVICE_NAME=garage
DIR_EXE_ABS=$(dirname $(realpath $0))
pathLog="/var/log/borg_stop_service.info"
BACKUP="${DIR_EXE_ABS}/borg_backup.sh"


echo "backupstarted $(date +%d-%m-%Y\ %H:%M:%S)" >> $pathLog

## === We stop garage
echo "stopping-$SERVICE_NAME $(date +%d-%m-%Y\ %H:%M:%S)" >> $pathLog
systemctl stop $SERVICE_NAME 
# Wait for garage to be fully stopped
while [ "$(sudo systemctl is-active $SERVICE_NAME)" != "inactive" ]; do
    echo "Waiting for $SERVICE_NAME to stop..."
    sleep 1
done

## === We run the backup
echo "BACKUP!"
$BACKUP

echo "backupdone $(date +%d-%m-%Y\ %H:%M:%S)" >> $pathLog

## === We start
echo "starting-$SERVICE_NAME $(date +%d-%m-%Y\ %H:%M:%S)" >> $pathLog

systemctl start $SERVICE_NAME --wait

echo "backupterminated $(date +%d-%m-%Y\ %H:%M:%S)" >> $pathLog

