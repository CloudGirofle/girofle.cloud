#!/bin/bash
# A simple script to test borg backup options

export BORG_PASSPHRASE="<thekey>"
export BORG_OPTIONS="-i /home/<local_user>/.ssh/id_rsa"
export BACKUP_NAME="<backup_name>"
export DB_DUMP=true
export BACKUP_TARGETS="mutu:/var/www db:/var/lib/mysql etc:/etc"
export BORG_EXEC=borg
export BACKUP_SERVER="<remote_user>@backup.girofle.cloud"

./borg_backup.sh

