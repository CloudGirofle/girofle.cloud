#!/usr/bin/bash

# Args
DIR_EXE=$(dirname $0)
THE_EXE=$(basename $0 .sh)

# Constants
PHP_TEMPLATE_PATH=${DIR_EXE}/conf/extra.www.conf
PHP_CONF_FILE_PATH=/etc/php/7.4/fpm/pool.d/www.conf
NGINX_COMMON_TEMPLATE_PATH=${DIR_EXE}/conf/nginx_common.conf
NGINX_COMMON_FILE_PATH=/etc/nginx/conf.d/nginx_common.conf

# Install dependencies
apt-get install -y \
	php7.4-fpm \
	php7.4-cli \
	php7.4-common \
	php7.4-mysql \
	php7.4-gd \
	php7.4-json \
	php7.4-xml \
	php7.4-zip \
	php7.4-curl \
	php7.4-bz2 \
	php7.4-intl \
	php7.4-mbstring \
	php7.4-ldap \
	php-apcu \
	php-bcmath \
	php-gmp \
	redis-server \
	php-redis \
	php-imagick \
	imagemagick \
	nginx \
	mariadb-server \
	certbot \
	python-certbot-nginx

# Change max_execution_time = 180 in /etc/php/7.4/fpm/php.ini

# setup PHP
echo "Editing php configuration file at ${PHP_CONF_FILE_PATH}."
  cat ${PHP_TEMPLATE_PATH} >> ${PHP_CONF_FILE_PATH}
echo "PHP-FPM is reloading"
systemctl restart php7.4-fpm

# setup NGINX
if [[ ! -f ${NGINX_COMMON_FILE_PATH} ]]
then
  echo "Editing nginx configuration file at ${NGINX_COMMON_FILE_PATH}."
    cat ${NGINX_COMMON_TEMPLATE_PATH} >> ${NGINX_COMMON_FILE_PATH}
  echo "Nginx is reloading"
  systemctl restart nginx
else
  echo "Nginx common configuration file already exists. Skipping to avoir overwritting it."
fi
echo "Removing Nginx default site"
unlink /etc/nginx/sites-enabled/default
rm /etc/nginx/sites-available/default

## Specifics to Nextcloud
# Edit /etc/php/7.4/fpm/conf.d/20-apcu.ini and add `apc.enable_cli=1`
# reload php-fpm & nginx
# copy the crontabs to the right locations

# setup certbot // NEED TO EDIT THE DOMAIN :)
sudo certbot certonly --noninteractive --nginx --email contact@girofle.cloud --agree-tos -d mutu2.girofle.cloud

