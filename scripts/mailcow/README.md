Ce dossier contient les fichiers/scripts relatifs à Mailcow.

Nous avons fait notre propre version du script de backup/restore afin de pouvoir réaliser des backups incrémentaux.
Afin de faire fonctionner les scripts de backup/restore, le fichier `backup_and_restore_rsync.sh` doit être linké (ou copié) dans le dossier `helpers-scripts/` de Mailcow (par défaut dans `/opt/mailcow-dockerized/helper-scripts/`.

# Backup

Pour réaliser un backup, il faut lancer la commande `./backup_and_restore_rsync.sh backup all`

# Restore

Il faut d'abord builder l'image docker se trouvant dans le dossier `backup`.
Puis lancer la commande `./backup_and_restore_rsync.sh restore`

