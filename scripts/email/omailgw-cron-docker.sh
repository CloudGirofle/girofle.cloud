#!/bin/bash
#  By CG, GPLv3+
# Script pour récupérer/exporter les données d'un postfix dans un docker
# Attention à bien vérifier la ligne 20 (couper 40 caractères, adresse de omailgw-cli.php)

# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR


tailfile=/home/debian/mailcow_postfix_tail.txt
DA=$(tail -n 1 $tailfile)
# test if it is a real date
# read last date
DA_new=$(date --rfc-3339=seconds | sed 's/ /T/') # Current timestamp
cd /opt/mailcow-dockerized
docker compose logs postfix-mailcow --since $DA | cut -c 40- | sponge | php /home/debian/omailgw-cli/omailgw-cli.php -v --log=stdin
# là va falloir couper les préfix des logs sinon ça va pas passer à la machine je pense.
# save last date si ça s'est bien passé
echo $DA_new >> $tailfile
