#!/usr/bin/python3
# A script to handle DKIM & relay configuration
# Cloud Girofle, GPLv3+, Jun 2022
import argparse
import os
import pwd
import subprocess
import sys

import dns.resolver

# ==== CONSTANTS
DNS_REFERENCE_DOMAIN = "girofle.cloud"
NC_INSTANCES_FILE = "/root/.the-watcher-prometheus.rc"
RELAYS_FILE = "/etc/postfix/sender_login_maps"
RELAYS_AUTHENTICATED_USER = "postier"  # the yunohost user that can be used to authenticate the user
DKIM_SELECTOR = "mail"  # configured in rspamd
DKIM_FOLDER = "/etc/dkim/"
DMARC_EXPECTED = '"v=DMARC1;p=reject;pct=100;ruf=mailto:dmarc@chif.fr;rua=mailto:dmarc@chif.fr;aspf=r;"'  # careful, double quotes are needed
SPF_EXPECTED = '"v=spf1 include:_spf.girofle.org ~all"'
SERVER_IPS = {"mutu2": {"ip4": "94.23.29.115", "ip6": "2001:41d0:2:1e73::1"}}

# === GLOBAL VARIABLES
my_resolver = dns.resolver.Resolver()
my_resolver.nameservers = ["1.1.1.1"]
dnstext = {"internal": [], "external": []}


def list_domains(senderonly: bool = False) -> list:
    """List all the domains that should be configured
    If senderonly is set to True, exclude the domains that we know are not sending emails.
    """
    extra_nodkim = ["mutu.girofle.cloud", "mutu2.girofle.cloud", "mutu3.girofle.cloud"]
    extra_instances = ["chif.fr"]

    allrelays = []
    with open(NC_INSTANCES_FILE, "r") as f:
        nc_instances = [line.split(";")[0] for line in f.readlines()]

    if not senderonly:  # Only
        allrelays += extra_nodkim
    allrelays += extra_instances
    allrelays += nc_instances
    return allrelays


def list_relays() -> list:
    """List the domains that are allowed (based on the relay_f file)"""
    with open(RELAYS_FILE, "r") as f:
        relays = [line.replace("\t", " ").split(" ")[0][1:] for line in f.readlines()]
    return relays


def _add_to_relay_hash(domainslist: list, verbose=True) -> None:
    if verbose:
        print("Adding lines to {}".format(RELAYS_FILE))
    with open(RELAYS_FILE, "a") as f:
        for r in domainslist:
            if verbose:
                print("- {}".format(r))
            f.write("@{} {}\n".format(r, RELAYS_AUTHENTICATED_USER))
    if verbose:
        print("Regenerating .db file")
    subprocess.call(["postmap", RELAYS_FILE])


def check_authenticated_relays(edit: bool = False) -> None:
    exclude_instances = ["chif.fr/nextcloud", "chif.fr"]
    allrelays = list_domains()
    relays = list_relays()

    addrelay = []
    for r in allrelays:
        if r not in relays and r not in exclude_instances:
            print("{} is not in {}".format(r, RELAYS_FILE))
            addrelay.append(r)

    for r in relays:
        if r not in allrelays and r not in exclude_instances:
            print("{} is not in {}".format(r, NC_INSTANCES_FILE))

    if edit:
        _add_to_relay_hash(addrelay)


def check_all_dkim(edit: bool = False) -> None:
    sumnotok = 0
    ld = list_domains(senderonly=True)
    for d in ld:
        ok = check_dkim(d, DKIM_FOLDER, edit)
        if ok == "break":
            break
        if not ok:
            sumnotok += 1

    print("====")
    print("{}/{} verifications failed O_O".format(sumnotok, len(ld)))


def check_all_dmarc() -> None:
    sumnotok = 0
    ld = list_domains(senderonly=True)
    for d in ld:
        ok = check_dmarc(d, verbose=False)
        if ok == "break":
            break
        if not ok:
            sumnotok += 1

    print("====")
    print("{}/{} verifications failed O_O".format(sumnotok, len(ld)))


def check_all_spf() -> None:
    sumnotok = 0
    ld = list_domains(senderonly=True)
    for d in ld:
        print(f"-- {d}")
        ok = check_spf(d, verbose=False)
        if ok == "break":
            break
        if not ok:
            sumnotok += 1

    print("====")
    print("{}/{} verifications failed O_O".format(sumnotok, len(ld)))


def check_relay(domain: str, edit: bool = False) -> None:
    """Check if the relay server allows relaying emails from this domain"""
    relays = list_relays()
    if domain not in relays:
        print("  [ERROR] domain not allowed in {}".format(RELAYS_FILE))
        if edit:
            _add_to_relay_hash([domain])
    else:
        print("  -- OK: SMTP relay: allowed")


def check_dmarc(domain: str, cname: str = None, print_dns: bool = True, verbose: bool = True) -> bool:
    """Check that the DMARC record is present"""

    if print_dns:
        if verbose:
            cmt = " ; -- DMARC"
        else:
            cmt = ""
        if domain.endswith(DNS_REFERENCE_DOMAIN):  # internal
            sub_domain = domain[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
            line_internal = f"_dmarc.{sub_domain} 10800 IN TXT {DMARC_EXPECTED}"
            dnstext["internal"].append(cmt)
            dnstext["internal"].append(line_internal)
        else:
            sub_domain = ".".join(domain.split(".")[:-2])
            sub_cname = cname[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
            line_internal = f"_dmarc.{sub_domain} 10800 IN CNAME _dmarc.{cname}"
            line_external = f"_dmarc.{sub_cname} 10800 IN TXT {DMARC_EXPECTED}"
            dnstext["internal"].append(cmt)
            dnstext["external"].append(cmt)
            dnstext["external"].append(line_internal)
            dnstext["internal"].append(line_external)

    dmd = "_dmarc." + domain  # dmarc domain
    ok = False
    try:
        for r in my_resolver.resolve(dmd, "TXT"):
            if DMARC_EXPECTED == str(r):
                ok = True
                if verbose:
                    print("  -- OK: DMARC DNS entry")
            else:
                print("  -- BAD: DMARC DNS entry:\n\t Expected:\t{}\n\t Found:\t{}".format(DMARC_EXPECTED, r))
    except dns.resolver.NXDOMAIN:
        print(" -- BAD: DMARC DNS entry {} not found".format(dmd))
        if verbose:
            print("You should add a TXT DNS entry for domain {} with value:".format(dmd))
            print("\t{}".format(DMARC_EXPECTED))
    except Exception as e:
        raise e

    return ok


def check_spf(domain: str, cname: str = None, print_dns: bool = False, verbose: bool = True) -> bool:
    """Check that the SPF record is present"""
    if print_dns:
        c = cname[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
        dnstext["internal"].append('{} 10800 IN TXT "v=spf1 include:_spf.girofle.org ~all"'.format(c))

        ok = True
    else:
        ok = False
        try:
            rr = my_resolver.resolve(domain, "TXT")
            if any([str(r) == SPF_EXPECTED for r in rr]):
                ok = True
                if verbose:
                    print("  -- OK: SPF DNS entry")
            else:
                print("  -- BAD: SPF DNS entry:\n\t Expected:\t{}".format(SPF_EXPECTED))
                for r in rr:
                    print("          Found: {}".format(r))
        except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
            print(" -- BAD: SPF DNS entry {} not found".format(domain))
            if verbose:
                print("You should add a TXT DNS entry for domain {} with value:".format(domain))
                print("\t{}".format(SPF_EXPECTED))
        except Exception as e:
            raise e

    return ok


def check_dkim(
    domain: str, dkim_folder: str, edit: bool, print_dns: bool = True, cname: str = None, verbose: bool = True
) -> bool:
    """Check if the DKIM is properly configured"""

    def _make_dns_dkim(domain: str, cname: str, save: bool = False) -> str:
        with open(os.path.join(dkim_folder, basef + ".txt"), "r") as f:
            dk = f.read()

        selector = dk.split("._domainkey")[0].split(".")[-1]
        dd = "".join(dk.replace("(", "").replace(")", "").replace("\n", "").split("\t")[3:])
        dd = dd.split("; ----- ")[0]  # remove comments
        if domain.endswith(DNS_REFERENCE_DOMAIN):
            sub_domain = domain[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
            line_internal = "{}._domainkey.{} 10800 IN TXT {}".format(selector, sub_domain, dd)
            if save:
                dnstext["internal"].append(line_internal)
            return line_internal

        else:
            if verbose:
                cmt = " ; -- DKIM"
            else:
                cmt = ""
            cname_target = f"{selector}._domainkey.{cname}"
            sub_domain = ".".join(domain.split(".")[:-2])
            sub_cname = cname[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
            line_internal = f"{selector}._domainkey.{sub_cname} 10800 IN TXT {dd}"
            line_external = f"{selector}._domainkey.{sub_domain} 10800 IN CNAME {cname_target}"

            if save:
                dnstext["external"].append(cmt)
                dnstext["internal"].append(line_internal)
                dnstext["external"].append(line_external)

            return line_internal + "\n;; -- On the external domain" + line_external

    basef = os.path.join(dkim_folder, domain + "." + DKIM_SELECTOR)
    ok = True

    if verbose and not print_dns:
        print()
        print("[DOMAIN] {}".format(domain))

    # Check that the key pair is present
    if os.path.isfile(basef + ".key") and os.path.isfile(basef + ".txt"):
        print("  -- OK: DKIM keys {} are present".format(domain))
        if (
            pwd.getpwnam("_rspamd").pw_uid != os.stat(basef + ".key").st_uid
        ):  # check that _rspamd owns the private key file
            print("  [BAD] {} is not owned by _rspamd user".format(basef + ".key"))
        else:
            print("  -- OK: DKIM private key {} have the right owner".format(basef + ".key"))
    else:
        print("  [BAD] files {} or {} are missing".format(basef + ".key", basef + ".txt"))
        ok = False

    # Generate the key pair if needed
    if not ok:
        if not edit:
            return ok
        else:
            print("Generating a DKIM key pair for the domain {}".format(domain))
            keyfile_f = domain + "." + DKIM_SELECTOR
            subprocess.call(["opendkim-genkey", "-d", domain, "-a", "-D", dkim_folder, "-s", keyfile_f])
            os.rename(
                os.path.join(dkim_folder, keyfile_f + ".private"),
                os.path.join(dkim_folder, keyfile_f + ".key"),
            )
            os.chown(
                os.path.join(dkim_folder, keyfile_f + ".key"),
                pwd.getpwnam("_rspamd").pw_uid,
                0,
            )
            ok = True

    # Check that the DNS entry is present and that the key is valid
    if ok:
        r = subprocess.call(
            [
                "opendkim-testkey",
                "-d",
                domain,
                "-s",
                DKIM_SELECTOR,
                "-k",
                basef + ".key",
            ]
        )
        if r != 0:
            ok = False
            print("Please add the following DNS TXT field:")
            print(_make_dns_dkim(domain, cname))

        else:
            ok = True
            print("  -- OK: DKIM is OK, congrats!")
    if print_dns:
        _make_dns_dkim(domain, cname, save=True)
    return ok


def check_dkim_dns(domain: str, selector: str, verbose: bool = True) -> bool:
    """Check that a DKIM key is present in the DNS records"""
    ok = False
    subdomain = f"{selector}._domainkey.{domain}"
    dkim_expected = '"v=DKIM1;k=rsa;t=s;s=email;p=M'  # prefix
    try:
        rr = my_resolver.resolve(subdomain, "TXT")
        rr = [str(i) for i in rr]  # dbg
        if len(rr) == 1 and rr[0].startswith(dkim_expected):
            ok = True
            if verbose:
                print("  -- OK: DKIM DNS entry seems valid")
        else:
            print("  -- BAD: DKIM DNS entry:\n\t Expected something that begins with:\t{}".format(dkim_expected))
            for r in rr:
                print("          Found: {}".format(r))
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
        print(" -- BAD: DKIM DNS entry {} not found".format(subdomain))
        if verbose:
            print("You should add a TXT DNS entry for domain {} with value provided by Cloud Girofle".format(subdomain))
    except Exception as e:
        raise e

    return ok


def generate_ip(server: str, ips: bool = None, verbose: bool = True) -> None:
    if (server not in SERVER_IPS) and ips is None:
        raise KeyError("{} is not a valid server for which the ip is known".format(server))

    if cname.endswith(DNS_REFERENCE_DOMAIN):
        if ips is None:
            ii = SERVER_IPS[server]
        else:
            ii = ips
        c = cname[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
        if verbose:
            cmt = f"; -- assuming the instance is on {server}"
        else:
            cmt = ""
        dnstext["internal"].append(cmt)
        dnstext["internal"].append("{} 10800 IN A {}".format(c, ii["ip4"]))
        dnstext["internal"].append("{} 10800 IN AAAA {}".format(c, ii["ip6"]))


def check_cname(domain: str, cname: str, print_dns: bool = True) -> bool:
    if print_dns:
        if not cname.endswith(DNS_REFERENCE_DOMAIN):
            raise IOError(
                "--cname option should be a subdomain of the `dns_reference_domain`, namely {}".format(
                    DNS_REFERENCE_DOMAIN
                )
            )

        if not domain.endswith(DNS_REFERENCE_DOMAIN):  ## Rule for external CNAME
            print("## CNAME SHOULD BE SET TO EXTERNAL DNS")
            print("## and should point from {} to {}".format(domain, cname))
        else:  ## Rule for internal CNAME
            d = domain[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
            c = cname[: (-len(DNS_REFERENCE_DOMAIN) - 1)]
            dnstext["internal"].append("{} 10800 IN CNAME {}".format(d, c))
        return True

    rr = my_resolver.resolve(domain, "CNAME")
    r = [str(r) for r in rr][0]
    print("resolved: {}, expected: {}".format(r, cname + "."))
    if r == cname + ".":
        return True
    else:
        raise Exception("{} has no CNAME pointing to {}".format(domain, cname))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Check and edit DKIM configuration.")
    parser.add_argument(
        "--check-all",
        const="check",
        action="store_const",
        help="Check all the configuration",
    )
    parser.add_argument(
        "--check-all-spf",
        const="check",
        action="store_const",
        help="Check all the configuration",
    )
    parser.add_argument(
        "--generate-all",
        const="check",
        action="store_const",
        help="Regenerate all the configuration",
    )
    parser.add_argument(
        "--check",
        nargs=1,
        dest="domain",
        metavar="domain",
        help="Check the configuration of a specific domain",
    )
    parser.add_argument(
        "--generate",
        nargs=1,
        dest="domain1",
        metavar="domain",
        help="Configure DKIM for a specific domain",
    )

    # Added to be able to generate a DNS configuration
    parser.add_argument(
        "--dns",
        nargs=1,
        dest="domain2",
        metavar="domain",
        help="Check the configuration of a specific domain",
    )
    parser.add_argument(
        "--initial-cname",
        nargs=1,
        dest="cname",
        metavar="domain",
        help="If the first domain points to a second domain",
    )
    parser.add_argument(
        "--ip4",
        nargs=1,
        dest="ip4",
        metavar="domain",
        help="IPv4 address of the target server",
    )
    parser.add_argument(
        "--ip6",
        nargs=1,
        dest="ip6",
        metavar="domain",
        help="IPv4 address of the target server",
    )

    args = parser.parse_args()

    # Print help if no input
    if (
        args.check_all is None
        and args.generate_all is None
        and args.domain is None
        and args.domain1 is None
        and args.check_all_spf is None
        and args.domain2 is None
        and args.cname is None
        and args.ip4 is None
        and args.ip6 is None
    ):
        parser.print_help()
        sys.exit(0)

    if args.ip4 is None and args.ip6 is None:
        ips = None
    else:
        ips = {}
    if args.ip4 is not None:
        ips["ip4"] = args.ip4[0]
    if args.ip6 is not None:
        ips["ip6"] = args.ip6[0]

    if args.check_all or args.generate_all:  # Check all domains
        if args.check_all:
            edit = False
        else:
            edit = True

        # Relays
        print("1. Check that all the relays are present")
        check_authenticated_relays(edit)

        # DKIM keys
        print("2. Check that all the DKIM key pairs are present")
        check_all_dkim(edit)

        # DMARC records
        print("2. Check that all the DMARC DNS records are present")
        check_all_dmarc()

        # SPF records
        print("3. Check that all the SPF DNS records are present")
        check_all_spf()
    elif args.check_all_spf:
        check_all_spf()

    elif (args.domain is not None) or (args.domain1 is not None) or (args.domain2 is not None):  # Check a single domain
        print_dns = False
        if args.domain is not None:  # --check option
            domain = args.domain[0]
            edit = False
        elif args.domain2 is not None:  # --dns option,
            domain = args.domain2[0]
            edit = True
            print_dns = True
        else:  # --generate option
            domain = args.domain1[0]
            edit = True

        if args.cname is None:
            cname = None
        else:
            cname = args.cname[0]
            check_cname(domain, cname, print_dns)
            if print_dns:
                generate_ip(server="mutu2", ips=ips, verbose=False)

        check_dkim(domain, DKIM_FOLDER, edit, print_dns, cname, verbose=True)
        check_relay(domain, edit)  # check that the relay is properly configured
        check_dmarc(domain, cname, print_dns, verbose=False)
        check_spf(domain, cname, print_dns)

        print()
        print("## ==== DNS ZONE CONFIGURATION")
        if print_dns:
            if dnstext["internal"]:
                print()
                print(";; Internal DNS zone configuration")
                for i in dnstext["internal"]:
                    print(i)
            if dnstext["external"]:
                print()
                print(";; External DNS zone configuration")
                for i in dnstext["external"]:
                    print(i)
