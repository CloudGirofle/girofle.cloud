#!/bin/bash

NEW_MAILS="/var/mail/maxime/new/"

for i in $(find ${NEW_MAILS} -type f -mtime -45); do
	cat $i | /var/mail/sievescript/maxime/scripts/extractattachment.sh ; 
	#echo $i
done

python3 /home/girofle/girofle.cloud/scripts/email/dmarcreportanalyzer.py
