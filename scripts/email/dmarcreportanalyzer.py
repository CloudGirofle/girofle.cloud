#!/usr/bin/python3
# By Cloud Girofle, Jun 2022, GPLv3+
# This script parses DMARC reports

import argparse
import datetime
import gzip
import logging
import os
import xml.etree.ElementTree as ET
import zipfile

import pandas as pd
from dateutil.relativedelta import relativedelta

logging.basicConfig(level=os.environ.get("LOG_LEVEL", "INFO"))

# Constants
DEFAULT_REPORT_FOLDER = "/var/mail/maxime/dmarc"
source_ip = ["91.121.67.157", "2001:41d0:1:769d::1"]  # chif.fr IPs


def extract_zip(input_zip) -> dict:
    input_zip = zipfile.ZipFile(input_zip)
    return {name: input_zip.read(name) for name in input_zip.namelist()}


def load_reports(folder: str) -> list:
    """Read report files from the given folder and only keep the supported formats (.zip and .gz)"""
    logging.debug(f"Reading reports from folder {folder}")
    report_list = os.listdir(folder)
    all_reports = []
    for report in report_list:
        p = os.path.join(folder, report)
        if report.endswith('.zip'):
            res = extract_zip(p)
            all_reports.append((report, res[list(res.keys())[0]]))
        elif report.endswith('.gz'):
            try:
                with gzip.open(p, 'rb') as f:
                    all_reports.append((report, f.read()))
            except Exception as e:
                logging.debug(f"Cannot open file {report}. Error: {e}")
        else:
            logging.debug(f"Unknown extension for file {report}")
    return all_reports


def print_report(report: dict) -> str:
    ips = ['fail', 'pass'][report['ip_source_correct']]
    return "{datebegin}-{dateend}: {header_from}: DKIM {policyevaluated_dkim}, SPF {policyevaluated_spf}, IP {ips}".format(
        **r, ips=ips)


def parse_report(report: str) -> list:
    tree = ET.ElementTree(ET.fromstring(report))
    root = tree.getroot()

    meta = root.find('report_metadata')
    date = [meta.find('date_range/begin').text, meta.find('date_range/end').text]
    fromr = meta.find('org_name').text

    allr = []
    for record in root.findall('record'):
        ip = record.find('row/source_ip').text
        da = {
            'from': fromr,
            'datebegin': datetime.datetime.fromtimestamp(int(date[0])),
            'dateend': datetime.datetime.fromtimestamp(int(date[1])),
            'count': record.find('row/count').text,
            'policyevaluated_disposition': record.find('row/policy_evaluated/disposition').text,
            'policyevaluated_dkim': record.find('row/policy_evaluated/dkim').text,
            'policyevaluated_spf': record.find('row/policy_evaluated/spf').text,
            'header_from': record.find('identifiers/header_from').text,
            'ip_source': ip,
            'ip_source_correct': ['fail', 'pass'][ip in source_ip],
        }

        allr.append(da)
    return allr


if __name__ == "__main__":
    # argument parses
    parser = argparse.ArgumentParser()
    parser.add_argument("--folder", help="folder", default=DEFAULT_REPORT_FOLDER, nargs="?", const=1)
    args = parser.parse_args()

    # load and extract reports
    rep = load_reports(args.folder)
    rr = []
    err = []
    for (f, r) in rep:
        try:
            rr.append(pd.DataFrame(parse_report(r)))
        except:
            err.append(f)
    ar = pd.concat(rr)
    ar.sort_values('datebegin', ascending=False, inplace=True)

    # filter out old reports
    ar = ar.loc[ar.datebegin > datetime.datetime.now() + relativedelta(months=-1)]  # DMARC report analyzer

    # format results
    columns_replace = {
        'from': 'from',
       'datebegin': 'begin',
       'dateend': 'end',
       'count': 'count',
       'policyevaluated_disposition': 'decision',
       'policyevaluated_dkim': 'DKIM',
       'policyevaluated_spf': 'SPF',
       'header_from': 'header_from',
       'ip_source_correct': 'IP'
    }
    tmp = ar.loc[:, columns_replace.keys()]
    tmp.columns = columns_replace.values()
    df = tmp.copy()
    df['count'] = df['count'].astype(int)
    aggregation_functions = {'count': 'sum'}
    df_new = df.groupby(['from', 'begin', 'end', 'decision', 'DKIM', 'SPF', 'header_from']).aggregate(
        aggregation_functions
    ).reset_index()
    df_new['begin'] = pd.to_datetime(df_new['begin'])
    df_new['end'] = pd.to_datetime(df_new['end'])
    df_new.sort_values('begin', inplace=True, ascending=False)

    # print results
    print(df_new.to_markdown())
    for f in err:
        print("-- Cannot parse file: {}".format(f))
