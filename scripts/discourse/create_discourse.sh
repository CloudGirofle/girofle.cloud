#!/bin/bash
# This script must be ran with admin rights
# Usage: sudo ./create_instance.sh yolo.girofle.cloud
# Script that contains commands to finish the setup of Discourse
# By MW, Cloud Girofle, apr 2022, GPLv3+


# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR


if [[ $1 == "" ]]
then
  echo "Please enter the host name of the new instance (ex: \`${THE_EXE}.sh mutu.girofle.cloud\`)"
  exit 1
fi

## CONSTANTS ##
DIR_EXE=$(dirname $0)
DOMAIN="${1}"
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${DOMAIN}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${DOMAIN}.conf
NGINX_TEMPLATE_PATH=${DIR_EXE}/conf/nginx-discourse.conf.tmpl

## Configure letsencrypt
echo -e "\e[32mSetting up TLS certificate ...\e[0m"
certbot certonly --noninteractive --nginx --email contact@girofle.cloud --agree-tos --expand -d $DOMAIN

########## NGINX ##########

if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
then 
  echo -e "\e[32mCreating nginx configuration file at ${NGINX_CONF_FILE_PATH} and link to it ${NGINX_CONF_LINK_PATH} ...\e[0m"
    cp ${NGINX_TEMPLATE_PATH} ${NGINX_CONF_FILE_PATH}
    sed -i 's/INSTANCE_HOST_NAME/'${DOMAIN}'/g' ${NGINX_CONF_FILE_PATH}
    ln -s ${NGINX_CONF_FILE_PATH} ${NGINX_CONF_LINK_PATH}
  echo "Nginx is reloading ..."
  systemctl reload nginx
else
  echo -e "\e[32mNginx configuration file already exists. Skipping nginx configuration.\e[0m"
fi

echo -e "\e[32mALL DONE. You can now create the admin user, etc. ...\e[0m"
