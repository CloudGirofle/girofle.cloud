#!/usr/bin/env bash
LOCKFILE='/var/run/the-watcher-prometheus-%s.pid'
HERE="$(cd "$(dirname "$0")" ; pwd)"
[ -r "/home/girofle/girofle.cloud/scripts/girofle.func.sh" ] && source "/home/girofle/girofle.cloud/scripts/girofle.func.sh"
[ -r "$HERE/../girofle.func.sh" ] && source "$HERE/../girofle.func.sh"
export LC_ALL=C
# Parce que si un lock existe depuis plus que temps
# c'est vraisemblablement qu'il vaut mieux le supprimer
LOCKTTL=$((30*60))	# 30 min
# for g_lock_me
G_LOCK_FILE_TTL="$LOCKTTL"
# Sortie standard, par défaut
SPONGED_OUT_FILE="${SPONGED_OUT_FILE:-}"
export PS4="\r[\${FUNCNAME:-main}:\$LINENO]\011"
if ! jq --version >/dev/null 2>&1 ; then
	echo 'Besoin de jq(1)'
	exit 1
fi
if ! bc --version >/dev/null 2>&1 ; then
	echo 'Besoin de bc(1)'
	exit 1
fi
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Transforme les MiB/GiB en bytes
function _bytify() {
	local s="$1"
	# Rusé ou pas ?
	s="$( sed -e 's/MiB/*1024^2/g' -e 's/GiB/*1024^3/g' -e 's/kiB/*1024^1/g' <<<"$s")"
	bc<<<"$s"
}
# Give call a timeout
function _ftimeout() {
	local t="$1" f="$2" cpid s ; shift 2
	$f $@ &
	cpid="$!"
	s=$SECONDS
	while [ $((SECONDS-s)) -lt "$t" ] ; do
		if ! ps "$cpid" > /dev/null ; then
			# fiston a fini avant le couvre-feu
			return
		else
			sleep 0.2
		fi
	done
	# fiston est en retard
	kill "$cpid"
}
# Fetch a remote certificate
function _fetch_cert() {
	local fqdn="$1"
	openssl s_client -connect "$fqdn:443" 2>/dev/null </dev/null | sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/p'
}
function watcher_nc_activity() {
	g_lock_me || return 1
	local prom_key='girofle_nextcloud_activity'
	printf '# HELP %s_last_activity Last activity timestamp\n' "$prom_key"
	printf '# TYPE %s_last_activity Gauge\n' "$prom_key"
	printf '# HELP %s_last_login Last time any user has logged in\n' "$prom_key"
	printf '# TYPE %s_last_login Gauge\n' "$prom_key"
	printf '# HELP %s_avg Activity average (per minute) since ever\n' "$prom_key"
	printf '# TYPE %s_avg Gauge\n' "$prom_key"
	printf '# HELP %s_count Activity count on a period of time\n' "$prom_key"
	printf '# TYPE %s_count Gauge\n' "$prom_key"
	while read -r d ; do
		# moui, cochon mais j'ai pas mieux
		db="$(grep dbname "$d/config/config.php" | awk '{print $3}' | cut -d "'" -f 2)"
		# pas mieux pour avoir le fqdn
		fqdn="$(cut -d '/' -f 4 <<<"$d")"
		# Bien plus rapide que select max(timestamp)
		la="$(mysql "$db" -N -e "select timestamp as lastActivity from oc_activity where user not in ('','admin') order by activity_id desc limit 1;" 2>/dev/null)"
		# oui, un occ user:lastseen ferait l'affaire, mais là au moins j'ai un timestamp
		ll="$(mysql "$db" -N -e "select max(configvalue) as lastLogin from oc_preferences where appid='login' and configkey='lastLogin' and userid not in ('','admin');" 2>/dev/null)";
		# Nb d'activity depuis x min
		# Je pourrais faire un count(activity_id) sur toute la table, et laisser grafana faire un rate
		# mais comme la table peut se faire purger, ça risque de faire des montagnes russes dans le graph
		a5="$(mysql "$db" -N -e "select count(activity_id) as activity5 from oc_activity where user != '' and timestamp>=unix_timestamp(now() - interval 5 minute);" 2>/dev/null )"
		# *ahum*
		# Nb d'activities par min depuis le début des temps
		# nb d'activities / ( ( ( dernier activity ) - (premier activity) ) / 60 )
		apm="$(mysql "$db" -N -e "select (select count(*) as activityCount from oc_activity where user != '') / ( ( (select timestamp as lastActivity from oc_activity where user != '' order by activity_id desc limit 1) - (select timestamp as firstActivity from oc_activity where user != '' order by activity_id asc limit 1) ) / 60 )" 2>/dev/null )"
		if [ -n "$la" ] ; then
			printf '%s_last_activity{server="%s"} %i\n' "$prom_key" "$fqdn" "$la"
		fi
		if [ -n "$ll" ] && [ "$ll" != "NULL" ] ; then
			printf '%s_last_login{server="%s"} %i\n' "$prom_key" "$fqdn" "$ll"
		fi
		if [ -n "$apm" ] && [ "$apm" != "NULL" ] ; then
			printf '%s_avg{server="%s"} %f\n' "$prom_key" "$fqdn" "$apm"
		fi
		if [ -n "$a5" ] ; then
			printf '%s_count{period="5min",server="%s"} %i\n' "$prom_key" "$fqdn" "$a5"
		fi
	done < <(find /var/www/ -maxdepth 2 -name occ | cut -d '/' -f 1-4)
	g_unlock_me
}
function watcher_nc() {
	local conf_file="$HOME/.the-watcher-prometheus.rc" curl_to=600 start_time=$EPOCHSECONDS logfile="/tmp/the-watcher-prometheus.log" t d n err tt
	if ! [ -s "$conf_file" ] ; then
		g_log "Pas trouvé fichier de conf $conf_file" ; exit 1
	fi
	g_lock_me || return 1
	local prom_key='girofle_nextcloud_users'
	printf '# HELP %s Currently connected users.\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	declare -A USERS APIKEY HTTP_CODE TIME_TOTAL HOSTS
	for h in mutu mutu2 mutu3 ; do
		ip="$(dig +short +noall +answer "${h}.girofle.cloud")"
		HOSTS[$ip]="$h"
	done
	while IFS=';' read -r s u k ; do
		[[ $s =~ ^# ]] && continue
		USERS[$s]="$u"
		APIKEY[$s]="$k"
	done < "$conf_file"
	echo ">>> $(date '+%F@%X') start with pid=$$" >> "$logfile"
	lastrun="$(g_get_lastrun)"
	# J'ai été lancé la dernière fois il y a ...
	iwasrunago="$(( EPOCHSECONDS - lastrun))"
	g_set_lastrun
	echo ">>> I was run $iwasrunago seconds ago" >> "$logfile"
	while read -r server ; do
		if [ -z "$(dig +short +noall +answer -t A $server)" ] && [ -z "$(dig +short +noall +answer -t AAAA $server)" ] ; then
			echo "Pas d'ip pour $server" >&2
			echo "Pas d'ip pour $server" >> "$logfile"
			unset USERS[$server]
		fi
	done < <(tr ' ' '\n' <<< "${!USERS[@]}")
	while read -r server ; do
		((n+=1))
		t="$(mktemp /tmp/.the-watcher-prometheus.nc.XXXXXX)"
		touch "${t}.err"
		echo ">>> $(date '+%F@%X') $n/${#USERS[@]} $server" >> "$logfile"
		for i in {1..3} ; do	# 3 tours, histoire de le réveiller
			# Timeout à 600s, c'est déjà long
			curl -m $curl_to -ks -w '%{stderr}http_code=%{http_code}\ntime_total=%{time_total}' --user "${USERS[$server]}":"${APIKEY[$server]}" "https://$server/ocs/v2.php/apps/serverinfo/api/v1/info?format=json" > "$t" 2>"${t}.err"
			if [ -s "${t}.err" ] ; then
				err="$(grep http_code "${t}.err" | cut -d '=' -f 2)"
				tt="$(grep time_total "$t.err" | cut -d '=' -f 2)"
			else
				err="000"
				tt=0
				echo ">>> Timeout" >> /tmp/the-watcher-prometheus.log
				echo "${USERS[$server]}:${APIKEY[$server]} https://$server/ocs/v2.php/apps/serverinfo/api/v1/info?format=json" >> "$logfile"
				cat "$t" >> "$logfile"
			fi
			HTTP_CODE[$server]="$err"
			TIME_TOTAL[$server]="$tt"
			# check that the Json activeUsers parameter is present
			if jq '.ocs.data.activeUsers.last5minutes' "$t" >/dev/null 2>&1 ; then
				# substract the current user to the user count
				d5="$(jq -r '.ocs.data.activeUsers.last5minutes-1' "$t")"
				d1="$(jq -r '.ocs.data.activeUsers.last1hour-1' "$t")"
				d24="$(jq -r '.ocs.data.activeUsers.last24hours-1' "$t")"
			else
				d5='0'
			fi
			rm -f "$t" "${t}.err"
			ip="$(dig +noall +answer +short "$server" | tail -1)"
		done
		printf '%s{server="%s",timeout="%i",last="%s",guessed_host="%s"} %i\n' "$prom_key" "$server" "$curl_to" "5min" "${HOSTS[$ip]}" "$d5"
		printf '%s{server="%s",timeout="%i",last="%s",guessed_host="%s"} %i\n' "$prom_key" "$server" "$curl_to" "1h" "${HOSTS[$ip]}" "$d1"
		printf '%s{server="%s",timeout="%i",last="%s",guessed_host="%s"} %i\n' "$prom_key" "$server" "$curl_to" "24h" "${HOSTS[$ip]}" "$d24"

	# shuf(1), so that it's not always the same that is probed first
	done < <(tr ' ' '\n' <<< "${!USERS[@]}" | shuf)
	printf '# HELP %s_http_code Returned code of %s.\n' "$prom_key" "$prom_key"
	printf '# TYPE %s_http_code Gauge\n' "$prom_key"
	for k in "${!HTTP_CODE[@]}" ; do
		printf '%s_http_code{server="%s",timeout="%i"} %s\n' "$prom_key" "${k}" "$curl_to" "${HTTP_CODE[$k]}"
	done
	printf '# HELP %s_time_total Returned code of %s.\n' "$prom_key" "$prom_key"
	printf '# TYPE %s_time_total Gauge\n' "$prom_key"
	for k in "${!TIME_TOTAL[@]}" ; do
		printf '%s_time_total{server="%s",timeout="%i"} %s\n' "$prom_key" "${k}" "$curl_to" "${TIME_TOTAL[$k]}"
	done
	printf '# HELP %s_duration Time taken by that run, in seconds\n' "$prom_key"
	printf '# TYPE %s_duration Gauge\n' "$prom_key"
	printf '%s_duration %i\n' "$prom_key" "$((EPOCHSECONDS - start_time))"
	printf '# HELP %s_time_since_last_run Time since last run, in seconds\n' "$prom_key"
	printf '# TYPE %s_time_since_last_run Gauge\n' "$prom_key"
	printf '%s_time_since_last_run %i\n' "$prom_key" "$iwasrunago"
	rm -f "$t"
	echo ">>> $(date '+%F@%X') end" >> /tmp/the-watcher-prometheus.log
	g_unlock_me
}
function watcher_backup() {
	local prom_key; prom_key="girofle_lastbackup"
	printf '# HELP %s Date of last backup\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	printf '# HELP %s_%s_seconds Duration of last backup.\n' "$prom_key" "duration"
	printf '# TYPE %s_%s_seconds Gauge\n' "$prom_key" "duration"
	for dest in bacopa retzo ; do
		g_lock_me || return 1
		if [ -r "/var/log/borg_$dest.info" ] ; then
			local lastbackup="$(date -d "$(tail -n 1 /var/log/borg_$dest.info | cut -c -25)" +%s)"
			printf '%s{dest="%s"} %s\n' "$prom_key" "$dest" "$lastbackup"
			local duration="$(awk 'END{print $7}' /var/log/borg_$dest.info)"
			if [ -n "$duration" ] ; then
				printf '%s_%s_seconds{dest="%s"} %i\n' "$prom_key" "duration" "$dest" "$duration"
			fi
		fi
		g_unlock_me
	done
}
function watcher_oo() {
	local d
	local prom_key; prom_key='girofle_onlyoffice_users'
	g_lock_me || return 1
	#declare -A STATS
	t="$(mktemp /tmp/watcher-tmp-oo-XXXXXX)"
	printf '# HELP %s Currently connected users.\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	for oo in "$@" ; do
		curl -ks "https://$oo/info/info.json" >"$t"
		if jq '.' "$t" >/dev/null 2>&1 ; then
			for period in hour day week month ; do
				#STATS[$period]="$(jq .connectionsStat.$period.edit.max $t)"
				printf '%s{oo="%s",mode="%s",period="%s"} %i\n' "$prom_key" "$oo" "edit" "$period" "$(jq .connectionsStat.$period.edit.max "$t")"
			done
		fi
	done
	rm -f "$t"
	g_unlock_me
}
function watcher_memory() {
	g_lock_me || return 1
	local prom_key; prom_key='girofle_proc_memory'
	printf '# HELP %s Memory used by each pid, in kibibyte\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	printf '# HELP %s_sum_bytes Sum of memory used by pids of proc\n' "$prom_key"
	printf '# TYPE %s_sum_bytes Gauge\n' "$prom_key"
	printf '# HELP %s_avg_bytes Average of memory used by pids of proc\n' "$prom_key"
	printf '# TYPE %s_avg_bytes Gauge\n' "$prom_key"
	printf '# HELP %s_min_bytes Min memory used by pids of proc\n' "$prom_key"
	printf '# TYPE %s_min_bytes Gauge\n' "$prom_key"
	printf '# HELP %s_max_bytes Max memory used by pids of proc\n' "$prom_key"
	printf '# TYPE %s_max_bytes Gauge\n' "$prom_key"
	printf '# HELP %s_stddev_bytes Stddev of memory used by pids of proc\n' "$prom_key"
	printf '# TYPE %s_stddev_bytes Gauge\n' "$prom_key"
	printf '# HELP %s_proc_count Count of pids of proc\n' "$prom_key"
	printf '# TYPE %s_proc_count Gauge\n' "$prom_key"
	local t types=( "uss" "rss" "vsz" "swap" ) tmp="$(mktemp /tmp/.the-watcher-prometheus.memory.XXXXXX)" proc label
	declare -A labels procs pids ; local labels procs pids
	if ps -o uss $$ >/dev/null 2>&1 ; then
		# Cas facile, mais nécessite d'être root
		_uss() { ps -o uss= "$@"; }
	else
		# Cas relou, root aussi
		_uss() { while [ -n "$1" ]; do awk '/Private/{s+=$2}END{print s}' "/proc/$1/smaps" ; shift ; done }
	fi
	while [ $# -gt 0 ] ; do
		IFS='#' read -r proc label <<<"$1"; shift
		l="${label:-$proc}"
		if [ -n "${labels[$l]}" ] ; then
			echo "Duplicate entry: $proc" >&2
			g_unlock_me
			return 1
		else
			labels[$l]=1
			procs[$l]=1
			pids[$l]="$(ps -aeo pid,cmd | awk -v p="$proc" '$2==p || $3==p {printf "%i ",$1}' )"
		fi
	done
	for label in ${!labels[@]} ; do
		#pids=( $(ps -aeo pid,cmd | awk -v p="$proc" '$2==p || $3==p {print $1}' ) )
		if [ -n "${pids[$label]}" ] ; then
			printf '%s_proc_count{proc="%s"} %i\n' "$prom_key" "$label" "$(wc -w <<<"${pids[$label]}")"
			for t in "${types[@]}" ; do
				case $t in
					uss)
						_uss ${pids[$label]} > "$tmp"
						printf '%s_sum_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "uss" "$label" "$(awk '{s+=$1}END{printf "%.0f",s*1024}' "$tmp")"
						printf '%s_avg_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "uss" "$label" "$(awk '{s+=$1}END{printf "%.0f",s*1024/NR}' "$tmp")"
						printf '%s_min_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "uss" "$label" "$(( 1024*$(sort -n "$tmp" | head -1) ))"
						printf '%s_max_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "uss" "$label" "$(( 1024*$(sort -n "$tmp" | tail -1) ))"
						printf '%s_stddev_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "uss" "$label" "$(awk '{s+=$1*$1}END{printf "%.0f",1024*sqrt(s/NR)}' "$tmp")"
						awk -v label=$label -v proc=$label -v pids="${pids[$label]}" -v pk=$prom_key 'BEGIN{split(pids,apids," ")}{printf "%s{proc=\"%s\",pid=\"%i\",type=\"uss\"} %.0f\n",pk,proc,apids[FNR],$1}' "$tmp"
						;;
					swap)
						for p in ${pids[$label]} ; do
							awk '/VmSwap/{print $2}' "/proc/$p/status" 2>/dev/null
						done > "$tmp"
						if [ -s "$tmp" ] ; then		# des fois /proc/$p/status disparait avant que j'ai pu le lire
							printf '%s_sum_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(awk '{s+=$1}END{printf "%.0f",s*1024}' "$tmp")"
							printf '%s_avg_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(awk '{s+=$1}END{printf "%.0f",s*1024/NR}' "$tmp")"
							printf '%s_min_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(( 1024*$(sort -n "$tmp" | head -1) ))"
							printf '%s_max_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(( 1024*$(sort -n "$tmp" | tail -1) ))"
							printf '%s_stddev_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(awk '{s+=$1*$1}END{printf "%.0f",1024*sqrt(s/NR)}' "$tmp")"
							awk -v label=$label -v proc=$label -v pids="${pids[$label]}" -v pk=$prom_key 'BEGIN{split(pids,apids," ")}{printf "%s{proc=\"%s\",pid=\"%i\",type=\"swap\"} %.0f\n",pk,proc,apids[FNR],$1}' "$tmp"
						fi
						;;
					*)
						ps -o "$t" ${pids[$label]} | sed 1d > "$tmp"
						printf '%s_sum_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(awk '{s+=$1}END{printf "%.0f",s*1024}' "$tmp")"
						printf '%s_avg_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(awk '{s+=$1}END{printf "%.0f",s*1024/NR}' "$tmp")"
						printf '%s_min_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(( 1024*$(sort -n "$tmp" | head -1) ))"
						printf '%s_max_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(( 1024*$(sort -n "$tmp" | tail -1) ))"
						printf '%s_stddev_bytes{type="%s",proc="%s"} %i\n' "$prom_key" "$t" "$label" "$(awk '{s+=$1*$1}END{printf "%.0f",1024*sqrt(s/NR)}' "$tmp")"
						awk -v label=$label -v proc=$label -v pids="${pids[$label]}" -v pk=$prom_key -v type="$t" 'BEGIN{split(pids,apids," ")}{printf "%s{proc=\"%s\",pid=\"%i\",type=\"%s\"} %.0f\n",pk,proc,apids[FNR],type,$1}' "$tmp"
						;;
				esac
			done
		fi
	done
	rm -f "$tmp"
	g_unlock_me
}
function watcher_mail() {
	g_lock_me || return 1
	local FILE=/var/log/mail.log
	local prom_key='girofle_mail_errors'
	printf '# HELP %s Number of given error codes in mail.log.\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	grep 'said:' "$FILE" | sed -e 's/.*said: \([0-9]*\)[- ].*/\1/' | sort -u | while read -r code ; do
		n="$(grep -E -c "said: ${code}[- ]" "$FILE")"
		printf '%s{code="%i"} %i\n' "$prom_key" "$code" "$n"
	done
	g_unlock_me
}

function watcher_mailqueue() {
	g_lock_me || return 1

	local prom_key='girofle_postfix_queue'
	# Dixit qmgr(8)
	local queues=( "incoming" "active" "deferred" "corrupt" "hold" )
	printf '# HELP %s Size of given postfix queues\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	local t="$(mktemp /tmp/.the-watcher.mailqueue.XXXXXX)"
	/usr/sbin/postqueue -j > "$t"
	for q in "${queues[@]}" ; do
		(
		l="$(jq -s "[.[]|select(.queue_name==\"$q\")]|length" "$t")"
		printf '%s{queue="%s"} %i\n' "$prom_key" "$q" "$l"
		) &
	done
	wait
	rm -f "$t"
	g_unlock_me
}

function watcher_phpfpm_status() {
	if ! [ -e /usr/bin/cgi-fcgi ] ; then
		echo "Besoin de cgi-fcgi"
		return
	fi
	g_lock_me || return 1
	local prom_key='girofle_phpfpm_status'
	printf '# HELP %s Status of php_fpm.\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	local t="$(mktemp /tmp/.the-watcher-prometheus.phpfpm-status..XXXXXX)"
	local conn_string="127.0.0.1:9000"
	SCRIPT_NAME=/status SCRIPT_FILENAME=/status QUERY_STRING="full&json" REQUEST_METHOD=GET cgi-fcgi -bind -connect "$conn_string" | tail -1 > "$t"
	# Les params à recup
	local params=("start since" "accepted conn" "listen queue" "max listen queue" "listen queue len" "idle processes" "active processes" "total processes" "max active processes" "max children reached" "slow requests")
	for k in "${params[@]}" ; do
		printf '%s_%s %i\n' "$prom_key" "${k// /_}" "$(jq ".\"$k\"" "$t")"
	done
	rm -f "$t"
	g_unlock_me
}
# wathcher_proc "<processus>[#label]
function watcher_proc() {
	g_lock_me || return 1
	local nproc procs=($@) prom_key='girofle_proc'
	printf '# HELP %s Count of processus\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	printf '# HELP %s_bytes_read Sum of bytes read from disk\n' "$prom_key"
	printf '# TYPE %s_bytes_read Gauge\n' "$prom_key"
	printf '# HELP %s_bytes_written Sum of bytes asked for written to disk\n' "$prom_key"
	printf '# TYPE %s_bytes_written Gauge\n' "$prom_key"
	printf '# HELP %s_bytes_written_cancelled Sum of bytes asked for written to disk but cancelled\n' "$prom_key"
	printf '# TYPE %s_bytes_written_cancelled Gauge\n' "$prom_key"
	for proc in "${procs[@]}" ; do
		# Chopage du label
		IFS='#' read -r proc label <<<"$proc"
		# pgrep m'en compte un de trop
		nproc="$(ps -aeo cmd | awk -v p="$proc" '$1==p || $2==p {s+=1}END{print s}' )"
		if [ -n "$nproc" ] ; then
			printf '%s{processus="%s"} %s\n' "$prom_key" "${label:-$proc}" "$nproc"
		fi
	done
	for proc in "${procs[@]}" ; do
		IFS='#' read -r proc label <<<"$proc"
		# Evite des grep. Merci https://stackoverflow.com/questions/46171193/how-do-i-use-environment-variables-in-awk
		for pid in $(ps -aeo pid="" -o command="" | awk -v p="$proc" '($2==p || $3==p) && $2!="awk" {print $1}' ) ; do
			#IFS=" " read -r a a a r w c d a < <(pidstat -d -p "$pid" | tail -1)
			read -d "\n" -r r w c < <(awk '/^(read_bytes|write_bytes|cancelled_write_bytes)/{print $2}' "/proc/$pid/io" 2>/dev/null)
			age="$( ps -o etimes= -p "$pid")"
			printf '%s_age_second{processus="%s",pid="%i"} %i\n' "$prom_key" "${label:-$proc}" "$pid" "$age"
			# Vaut -1 si pas root
			# C'est la quantité moyenne de donnée lu/écrite depuis la naissance du processus
			[ -n "$r" ] && printf '%s_bytes_read{processus="%s",pid="%i"} %s\n' "$prom_key" "${label:-$proc}" "$pid" "$r"
			[ -n "$w" ] && printf '%s_bytes_written{processus="%s",pid="%i"} %s\n' "$prom_key" "${label:-$proc}" "$pid" "$w"
			[ -n "$c" ] && printf '%s_bytes_written_cancelled{processus="%s",pid="%i"} %s\n' "$prom_key" "${label:-$proc}" "$pid" "$c"
		done
	done
	g_unlock_me
}
function watcher_proc_cpu() {
	g_lock_me || return 1
	declare -a stats
	local a stats pid procs=($@) prom_key='girofle_proc_cpu' clk_tck="$(getconf CLK_TCK|xargs)"
	for proc in "${procs[@]}" ; do
		IFS='#' read -r proc label <<<"$proc"
		while IFS=' ' read -r pid cmd ; do
			if [ -n "$pid" ] ; then
				# en tick
				if [ -r "/proc/$pid/stat" ] ; then	# En espérant qu'il ne disparaisse pas entre là et
					readarray -d ' ' stats </proc/$pid/stat	# là
				fi
				# en seconds
				read -r u a </proc/uptime
			fi
			# utime: 13, stime: 14, starttime: 21
			u="${u/./}"
			# Peut-être que l'usage de bc est overkill,
			# mais j'aime bien avoir des chiffres après la virgule
			if [ ${#stats[@]} -gt 0 ] ; then
				elapsed="$(bc<<<"$u-${stats[21]}")"
				utime="$(bc<<<"scale=6;100*${stats[13]}/$elapsed")"
				stime="$(bc<<<"scale=6;100*${stats[14]}/$elapsed")"
				printf '%s_user_percent{processus="%s",pid="%s"} %f\n' "$prom_key" "${label:-$proc}" "$pid" "$utime"
				printf '%s_system_percent{processus="%s",pid="%s"} %f\n' "$prom_key" "${label:-$proc}" "$pid" "$stime"
				printf '%s_total_percent{processus="%s",pid="%s"} %f\n' "$prom_key" "${label:-$proc}" "$pid" "$(bc<<<"$utime+$stime")"
			fi
		done < <(ps -aeo pid,cmd | awk -v p="$proc" '$2==p || $3==p {print $1,$2}' )
	done
	g_unlock_me
}
function watcher_php_logs() {
	g_lock_me || return 1
	local phpvers=$(php -r 'echo PHP_MAJOR_VERSION.".".PHP_MINOR_VERSION;')
	printf -v logfile "/var/log/php%s-fpm.log" "$phpvers"
	local prom_key='girofle_php_logs'
	local t="$(mktemp /tmp/.the-watcher-prometheus.php-logs.XXXXXX)"
	printf '# HELP %s Some log info of php.\n' "$prom_key"
	printf '# TYPE %s Gauge\n' "$prom_key"
	# Les pools php qui existent. P-e ca servira pas, mais p-e que si
	local pools=( $(grep -Eh '^\[' /etc/php/$phpvers/fpm/pool.d/* | tr -d '[]' | sort -u ) )
	#[09-May-2022 14:00:01] WARNING: [pool nextcloud] server reached pm.max_children setting (16), consider raising it
	declare -A searchs  # La clé est le motif
	searchs[pm.max_children]="pm.max_children atteint"
	#  WARNING: [pool www] seems busy (you may need to increase pm.start_servers, or pm.min/max_spare_servers), spawning 8 children, there are 0 idle, and 13 total children
	searchs[seems busy]="pool seems busy"
	for search in "${!searchs[@]}" ; do
		# Attrape le pattern, sans présager du pool
		grep "$search" "$logfile" > "$t"
		local n=0
		declare -A stats
		# Compte pour chaque pool
		for pool in "${pools[@]}" ; do
			stats[$pool]="$(grep -c "\[pool $pool\]" "$t")"
		done
		for pool in "${pools[@]}" ; do
			printf '%s{parameter="%s",php_version="%s",php_fpm_pool="%s",label="%s"} %s\n' "$prom_key" "$search" "$phpvers" "$pool" "${searchs[$search]}" "${stats[$pool]}"
		done
	done
	rm -f "$t"
	g_unlock_me
}
function watcher_docker() {
	if ! command -v docker >/dev/null 2>&1 ; then
		return
	fi
	g_lock_me || return 1
	local to_check=( "$@" )
	local prom_key='girofle_docker'
	local t="$(mktemp /tmp/.the-watcher-prometheus-docker.XXXXXX)"
	printf '# HELP %s_cpu_percent Cpu usage of the container, in percent, given by docker stats\n' "$prom_key"
	printf '# TYPE %s_cpu_percent Gauge\n' "$prom_key"
	printf '# HELP %s_cgroup_cpu_usage Cpu usage of the container, given by cgroup\n' "$prom_key"
	printf '# TYPE %s_cgroup_cpu_usage Gauge\n' "$prom_key"
	printf '# HELP %s_cgroup_mem_usage Memory usage of the container, given by cgroup \n' "$prom_key"
	printf '# TYPE %s_cgroup_mem_usage Gauge\n' "$prom_key"
	printf '# HELP %s_mem_percent Memory usage of the container, in percent, given by docker stats\n' "$prom_key"
	printf '# TYPE %s_mem_percent Gauge\n' "$prom_key"
	printf '# HELP %s_mem_usage_current_byte Memory used by the container, in byte, given by docker stats\n' "$prom_key"
	printf '# TYPE %s_mem_usage_current_byte Gauge\n' "$prom_key"
	printf '# HELP %s_mem_usage_max_byte Max memory the container is allowed to use, given by docker stats\n' "$prom_key"
	printf '# TYPE %s_mem_usage_max_byte Gauge\n' "$prom_key"
	printf '# HELP %s_container_running If that container is running\n' "$prom_key"
	printf '# TYPE %s_container_running Gauge\n' "$prom_key"

	docker stats --no-stream --format json > "$t"
	printf '# HELP %s_container_count Number of containers running\n' "$prom_key"
	printf '# TYPE %s_container_count Gauge\n' "$prom_key"
	printf '%s_container_count %i\n' "$prom_key" "$(jq -s '. | length' "$t")"
	local names ids name image mempc cpupc memu status startedat
	# Dans le genre moche et pas pratique:
	# {"BlockIO":"54.6MB / 9.22MB","CPUPerc":"1.00%","Container":"cc30bc285d9a","ID":"cc30bc285d9a","MemPerc":"0.09%","MemUsage":"27.74MiB / 31.24GiB","Name":"mailcowdockerized-netfilter-mailcow-1","NetIO":"0B / 0B","PIDs":"6"}
	# Ceux-là, ça se peut qu'ils ne tournent pas
	if [ ${#to_check[@]} -gt 0 ] ; then
		names=( "${to_check[@]}" )
		for name in "${names[@]}" ; do
			ids+=( "$(docker ps --filter  "name=$name" --format '{{ .ID }}')" )
		done
	else	# Si on me demande rien, je prends tous ceux que je trouve
		mapfile -t names < <(jq -r '.Name' "$t")
		mapfile -t ids < <( jq -r '.ID' "$t")
	fi
	for id in "${ids[@]}" ; do
		IFS=" " read -r status image name startedat < <(docker inspect "$id" --format '{{ .State.Status }} {{ .Config.Image }} {{ .Name }} {{ .State.StartedAt }}')
		local s="$(find /sys/fs/cgroup/ -name "docker-$id*" | tail -1)"
		cpusage="$(awk '/^usage_usec/{print $2}' "$s/cpu.stat")"   # En us
		cg_cu="$(awk -v cu=$cpusage '{printf "%f",cu/($1*1000000)}' /proc/uptime)"
		cg_mu="$(awk -v mu=$(<"$s/memory.current") '/^MemTotal:/{printf "%f",mu/($2*1024) }' /proc/meminfo)"

		name="${name/\//}"
		if [ "$status" = "running" ] ; then
			printf '%s_is_running{image="%s",containerid="%s",name="%s"} %i\n' "$prom_key" "$image" "$id" "$name" "1"
			# 2.059MiB / 3.812GiB
			mapfile -d " " -t memu < <(jq -r "select(.ID==\"$id\") | .MemUsage" "$t")
			mempc="$(jq -r "select(.ID==\"$id\") | .MemPerc" "$t" | tr -d '%' )"
			cpupc="$(jq -r "select(.ID==\"$id\") | .CPUPerc" "$t" | tr -d '%' )"
			printf '%s_cpu_percent{image="%s",containerid="%s",name="%s"} %.02f\n' "$prom_key" "$image" "$id" "$name" "$cpupc"
			printf '%s_cgroup_cpu_usage{image="%s",containerid="%s",name="%s"} %.05f\n' "$prom_key" "$image" "$id" "$name" "$cg_cu"
			printf '%s_mem_percent{image="%s",containerid="%s",name="%s"} %.05f\n' "$prom_key" "$image" "$id" "$name" "$mempc"
			printf '%s_cgroup_mem_usage{image="%s",containerid="%s",name="%s"} %.05f\n' "$prom_key" "$image" "$id" "$name" "$cg_mu"
			printf '%s_mem_usage_current_byte{image="%s",containerid="%s",name="%s"} %f\n' "$prom_key" "$image" "$id" "$name" "$(_bytify "${memu[0]}")"
			printf '%s_mem_usage_max_byte{image="%s",containerid="%s",name="%s"} %f\n' "$prom_key" "$image" "$id" "$name" "$(_bytify "${memu[2]}")"
			status_ts="$(date -d "$startedat" "+%s")"
			printf '%s_uptime_s{image="%s",containerid="%s",name="%s"} %i\n' "$prom_key" "$image" "$id" "$name" "$((EPOCHSECONDS - status_ts))"
		else
			printf '%s_is_running{image="%s",containerid="%s",name="%s"} %i\n' "$prom_key" "$image" "$id" "$name" "0"
		fi
	done
	rm -f "$t"
	g_unlock_me
}
function watcher_http() {	# url[lcode-ucode]#label Considere comme succes si le code de retour est entre lcode et ucode (inclus)
	g_lock_me || return 1
	local prom_key='girofle_http'
	printf '# HELP %s_return_code Return code \n' "$prom_key"
	printf '# TYPE %s_return_code Gauge\n' "$prom_key"
	printf '# HELP %s_is_ok 1 if considered as success, 0 if false \n' "$prom_key"
	printf '# TYPE %s_is_ok Gauge\n' "$prom_key"
	declare -A ret
	for pair in "$@" ; do
		IFS='#' read -r url label <<<"$pair"
		# comment ca je triche ?
		url="${url/]/}"
		IFS='[' read -r url codes <<<"$url"
		label="${label:-$url}"
		IFS='-' read -r lcode ucode <<<"$codes"
		code="$(curl -w '%{http_code}' -ks $url -o /dev/null)"
		printf '%s_return_code{url="%s",label="%s",lcoderange="%i",ucoderange="%i"} %s\n' "$prom_key" "$url" "$label" "$lcode" "$ucode" "$code"
		# Vérité "à la perl" : 0 faux, 1 vrai (inverse du shell)
		if [ "$code" -ge "$lcode" ] && [ "$code" -le "$ucode" ] ; then
			# Vrai
			is_ok=1
		else
			is_ok=0
		fi
		printf '%s_is_ok{url="%s",label="%s",lcoderange="%i",ucoderange="%i"} %i\n' "$prom_key" "$url" "$label" "$lcode" "$ucode" "$is_ok"
	done
	g_unlock_me
}
function watcher_cert() {
	g_lock_me || return 1
	local prom_key='girofle_cert'
	printf '# HELP %s_not_after Certificate expire at this timestamp\n' "$prom_key"
	printf '# TYPE %s_not_after Gauge\n' "$prom_key"
	for fqdn in "$@" ; do
		local t="$(mktemp /tmp/.the-watcher-prometheus.cert.XXXXXX)" ; trap "rm -f $t" 0
		_ftimeout 5 _fetch_cert "$fqdn" > "$t"
		if [ -s "$t" ] ; then
			local d="$( date -d "$(openssl x509 -in "$t" -enddate -noout | cut -d "=" -f 2)" +%s)"
			local s="$(openssl x509 -in "$t" -subject -noout | cut -d '=' -f 2- | sed -e 's/^ *//')"
			local c="$(sed -e 's/.*CN *= *\(.*\)/\1/'<<<"$s")"
			printf "%s_not_after{file=\"%s\",subject=\"%s\",cn=\"%s\"} %i\n" "$prom_key" "$fqdn" "$s" "$c" "$d"
		else
			printf "%s_not_after{file=\"%s\",subject=\"%s\",cn=\"%s\"} %i\n" "$prom_key" "$fqdn" "" "" "0"
		fi
		rm -f /tmp/$fqdn.stderr
		rm -f "$t"
	done
	g_unlock_me
}
function watcher_mailcow_mailqueue() {
	g_lock_me || return 1
	local prom_key='girofle_mailcow_mailqueue'
	local id cname="mailcowdockerized-postfix-mailcow-1" t=$(mktemp /tmp/.the-watcher-prometheus.mcq.XXXXXX) qs ; trap "rm -f $t" 0
	printf '# HELP %s_length Length of mailcow mailqueue\n' "$prom_key"
	printf '# TYPE %s_length\n' "$prom_key"
	id="$(docker ps --filter "name=$cname" --format '{{ .ID }}')"
	if [ -z "$id" ] ; then
		echo "container $cname not found" >&2
		g_unlock_me
		return 1
	fi
	docker exec "$id" postqueue -j > "$t"
	# Queues standards de postfix
	qs=( maildrop hold incoming active deferred )
	for q in "${qs[@]}" ; do
		l="$(jq -s "[ .[] | select(.queue_name==\"$q\") ] | length" "$t")"
		printf '%s_length{name="%s"} %i\n' "$prom_key" "$q" "$l"
	done
	g_unlock_me
}
function watcher_mailcow_sogo() {
	g_lock_me || return 1
	local prom_key="girofle_mailcow_sogo" hang_for=1 start_time=$EPOCHSECONDS
	local service_name="sogo-mailcow" max_hang_for=29
	local t="$(mktemp /tmp/.the-watcher-prometheus.mcsogo.XXXXXX)"
	printf '# HELP %s_max_hanging_requests Since last run, max number of processes that are, during the same minute, hanging the same request\n' "$prom_key"
	printf '# TYPE %s_max_hanging_requests\n' "$prom_key"
	printf '# HELP %s_max_hanging_requests_since When was the last run of %s_max_hanging_requests\n' "$prom_key" "$prom_key"
	printf '# TYPE %s_max_hanging_requests_since\n' "$prom_key"
	printf '# HELP %s_max_hanging_requests_when When the %s_max_hanging_requests occurs\n' "$prom_key" "$prom_key"
	printf '# TYPE %s_max_hanging_requests_when\n' "$prom_key"
	printf '# HELP %s_pid_is_hanging_request_for That pid is hanging request for that number of min\n' "$prom_key"
	printf '# TYPE %s_pid_is_hanging_request_for Gauge\n' "$prom_key"
	printf '# HELP %s_sogo_requests_sum_size Sum of requests size, sorted by method, in kibibytes\n' "$prom_key"
	printf '# TYPE %s_sogo_requests_sum_size Gauge\n' "$prom_key"
	printf '# HELP %s_duration Time taken by that run, in seconds\n' "$prom_key"
	printf '# TYPE %s_duration Gauge\n' "$prom_key"
	local lastrun="$(g_get_lastrun)"
	if [ -z "$1" ] ; then
		cd /opt/mailcow-dockerized && docker compose logs "$service_name" --since "$(date -d @"$lastrun" "+%Y-%m-%dT%H:%M:%S")" > "$t"
		g_set_lastrun
	else
		cat "$1" > "$t"
		# Pas de mise à jour de lastrun: je suis probablement en train de débugger
	fi
	if ! [ -s "$t" ] ; then
		g_unlock_me
		return 1
	fi
	# Chope les logs des x dernieres min,
	# Par exemple, en faisant du ménage et de l'ordre, ça fait à peu près ça :
	# Feb  7 14:37:49 has been hanging in the same request for 1 minutes
	# Feb  7 14:37:49 has been hanging in the same request for 1 minutes
	# 
	# Feb  7 14:36:40 has been hanging in the same request for 2 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 2 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 2 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 2 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 2 minutes
	# Feb  7 14:36:44 has been hanging in the same request for 2 minutes
	# Feb  7 14:37:49 has been hanging in the same request for 2 minutes
	# 
	# Feb  7 14:36:16 has been hanging in the same request for 3 minutes
	# Feb  7 14:36:16 has been hanging in the same request for 3 minutes
	# Feb  7 14:37:49 has been hanging in the same request for 3 minutes
	# 
	# Feb  7 14:36:16 has been hanging in the same request for 4 minutes
	# Feb  7 14:36:16 has been hanging in the same request for 4 minutes
	# Feb  7 14:36:16 has been hanging in the same request for 4 minutes
	# Feb  7 14:36:16 has been hanging in the same request for 4 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 4 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 4 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 4 minutes
	# Feb  7 14:36:40 has been hanging in the same request for 4 minutes
	for hang_for in $(seq 1 $max_hang_for) ; do
		# Awk:
		# 	- Supprime les secondes
		# 	- Compte combien il y a d'entrées pour ce timestamp (de la form Mon d hh:mm).  Ca donne le nombre de ligne pour chaque minute pour cette valeur de hang_for
		# 	- Trouve le timestamp qui a la plus grande entrée
		# 		Ca pose un pb de raisonnement : dans l'exemple ci-dessus, pour "3 minutes" ça va n'en compter que 2 (ceux de 14:36)
		# 		Ce qui ne pose pas de pb si le script est lancé toutes les minutes, mais si c'est plus lentement ça ne comptera pas le "3 minutes" de 14:37
		# 		Si je dispatch par minute, je vais avoir ce genre de chose :
		# 			girofle_mailcow_sogo_max_hanging_requests{container_service="sogo-mailcow",for_min="2"} 6
		# 			girofle_mailcow_sogo_max_hanging_requests_since{container_service="sogo-mailcow",for_min="2"} 1739453862
		# 			girofle_mailcow_sogo_max_hanging_requests_when{container_service="sogo-mailcow",for_min="2"} 1738935360
		# 			girofle_mailcow_sogo_max_hanging_requests{container_service="sogo-mailcow",for_min="2"} 1
		# 			girofle_mailcow_sogo_max_hanging_requests_since{container_service="sogo-mailcow",for_min="2"} 1739453862
		# 			girofle_mailcow_sogo_max_hanging_requests_when{container_service="sogo-mailcow",for_min="2"} 1738935420
		# 		Ce qui ne va pas plaire à prometheus (une métrique est dupliquée : for_min="2")

		IFS=";" read -r ts n < <(grep -F "has been hanging in the same request for $hang_for minutes" "$t" | awk '{gsub(":..$","",$5); entries[$3 " " $4 " " $5]+=1}END{max=0;for (i in entries) {if (entries[i] > max){entry=i;max=entries[i]}}; printf "%s;%i",entry,entries[entry]}')
		#while IFS=";" read -r ts n ; do
			# Là j'ai le timestamp (style Feb 7 14:54) et le nb d'entrées pour ce timestamp
			printf '%s_max_hanging_requests{container_service="%s",for_min="%i"} %i\n' "$prom_key" "$service_name" "$hang_for" "$n"
			printf '%s_max_hanging_requests_since{container_service="%s",for_min="%i"} %i\n' "$prom_key" "$service_name" "$hang_for" "$lastrun"
			printf '%s_max_hanging_requests_when{container_service="%s",for_min="%i"} %i\n' "$prom_key" "$service_name" "$hang_for" "$(date -d "${ts:-Jan 1 1970 GMT}" +%s)"
		#done < <(grep -F "has been hanging in the same request for $hang_for minutes" "$t" | awk '{gsub(":..$","",$5); entries[$3 " " $4 " " $5]+=1}END{for (i in entries) {printf "%s;%i\n",i,entries[i]}}')
	done
	# Awk:
	# 	- hash pid <-> hang_for. Ainsi hang_for[pid] contient le temps depuis lequel le pid hang, et y a fatalement la valeur la plus grande (si le temps ne s'inverse pas)
	# 	Mais si, entre deux run, un pid fini par faire sa tâche, puis hang de nouveau, le-temps-s'inverse
	# 	#oneliner
	awk -v prom_key=$prom_key -v container_service=$service_name '/has been hanging in the same request for/{hang_for[$12]=$21}END{for (pid in hang_for) {printf "%s_pid_is_hanging_request_for{container_service=\"%s\",pid=\"%i\"} %i\n",prom_key,container_service,pid,hang_for[pid]}}' "$t"
	# Ca, c'est important de le savoir
	printf "%s_error_no_child %i\n" "$prom_key" "$(grep -c 'No child available to handle incoming request!' "$t")"
	# Awk: le match devrair matcher ']: <ipv4 ou ipv6>' et faire que je ne chope que les lignes qui contiennent une requête
	# Je stocke la taille en k parce que le int de awk est limité à 2^31-1 (2147483647)
	awk -v prom_key=$prom_key -v container_service=$service_name -F'"' 'BEGIN{methods="GET POST PUT REPORT PROPFIND OPTIONS HEAD DELETE PROPPATCH";split(methods,am," "); for (i in am) sum[am[i]]}/\]: [0-9a-f]+[\.:]/{split($3,a," ");split($2,r," ");m=r[1];s=a[6];if (match(a[6],/K/)){ s=substr(a[6],0,RSTART-1)};if (match(a[6],/M/)){ s=1024*substr(a[6],0,RSTART-1)};sum[m]+=s}END{for (m in sum) {printf "%s_requests_sum_size{method=\"%s\",container_service=\"%s\"} %i\n",prom_key,m,container_service,sum[m]}}' "$t"
	printf '%s_duration %i\n' "$prom_key" "$((EPOCHSECONDS - start_time))"
	g_unlock_me
	rm -f "$t"
}
function watcher_unbound() {
	g_lock_me || return 1
	local prom_key="girofle_unbound"
	if ! [ -x /usr/sbin/unbound-control ]; then
		g_unlock_me
		return 1
	fi
	# Oui, c'est très mailcow-centric, et ca dépend de /opt/mailcow-dockerized/unbound-remote.conf, désolé
	# total.num.queries=2774
	/usr/sbin/unbound-control -c /opt/mailcow-dockerized/unbound-remote.conf stats_noreset | awk -F"=" -v prom_key=$prom_key '{printf "%s_stats{counter=\"%s\"} %f\n",prom_key,$1,$2}'
	g_unlock_me
}
function watcher_dnsmasq() {
	g_lock_me || return 1
	# Shamelessly inspired by https://github.com/google/dnsmasq_exporter/blob/v0.3.0/collector/collector.go
	local prom_key="girofle_dnsmasq"
	local KEYS
	declare -A KEYS
	KEYS[cachesize]="Configured size of the DNS cache"
	KEYS[insertions]="DNS cache insertions"
	KEYS[evictions]="DNS cache evictions: numbers of entries which replaced an unexpired cache entry"
	KEYS[misses]="DNS cache misses: queries which had to be forwarded"
	KEYS[hits]="DNS queries answered locally (cache hits)"
	KEYS[auth]="DNS queries for authoritative zones"
	# https://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=blob;f=src/option.c;h=f4ff7c01894bbdb1e79b71b524500ade8fdf739d;hb=HEAD#l6012
	# https://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=blob;f=src/cache.c;h=0eacec94a4b9b00ae66b4bdf4e584528954c691a;hb=HEAD#l1749
	KEYS[servers]="Sum counts from different records for same server"
	#KEYS[version]="??"
	NS="127.0.0.1"
	if ! command -v dig >/dev/null 2>&1 ; then
		exit 1
	fi
	if ! dig +tries=1 +timeout=1 +short www.free.fr @"$NS" &>/dev/null ; then
		exit 2
	fi
	for k in "${!KEYS[@]}" ; do
		if [ "$k" = "servers" ] ; then
			continue
		fi
		printf '# TYPE %s_%s Gauge\n' "$prom_key" "$k"
		printf '# HELP %s_%s %s\n' "$prom_key" "$k" "${KEYS[$k]}"
		r="$(dig +short chaos txt "${k}.bind" "@$NS")"
		printf '%s_%s{nameserver="%s"} %i\n' "$prom_key" "${k}" "$NS" "${r//\"/}"
	done
	for k in servers ; do
		printf '# TYPE %s_%s Gauge\n' "$prom_key" "$k"
		printf '# HELP %s_%s %s\n' "$prom_key" "$k" "${KEYS[$k]}"
		# https://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=blob;f=src/cache.c;h=0eacec94a4b9b00ae66b4bdf4e584528954c691a;hb=HEAD#l1769
		# "192.168.65.7#53 2 0" "8.8.8.8#53 4 0" => 192.168.65.7#53 2 0 8.8.8.8#53 4 0
		IFS=" " read -r -a res < <(dig +short chaos txt "${k}.bind" @"$NS" | tr -d '"' )
		# tout ça pour éviter un sed -e 's/" "/\n/'
		for ((i=0;i<${#res[@]};i+=3)) ;do
			printf '%s_%s_queries{nameserver="%s",server="%s",port="%i"} %i\n' "$prom_key" "${k}" "$NS" "${res[$i]%#*}" "${res[$i]#*#}" "${res[$((i+1))]}"
			printf '%s_%s_failed_queries{nameserver="%s",server="%s",port="%i"} %i\n' "$prom_key" "${k}" "$NS" "${res[$i]%#*}" "${res[$i]#*#}" "${res[$((i+2))]}"
		done
	done
	g_unlock_me
}
function watcher_wireguard() {
	local prom_key="girofle_wireguard"
	local t="$(mktemp /tmp/.the-watcher-prometheus.wireguard.XXXXXX)"
	if ! wg show all dump > "$t" ; then
		rm -f "$t"
		return 2
	fi
	g_lock_me || return 1
	printf "# HELP %s_latest_handshake When last handshake was sent\n" "$prom_key"
	printf "# HELP %s_tranfer_rx Bytes received\n" "$prom_key"
	printf "# HELP %s_tranfer_tx Bytes sent\n" "$prom_key"
	printf "# TYPE %s_latest_handshake Gauge\n" "$prom_key"
	printf "# TYPE %s_tranfer_rx Gauge\n" "$prom_key"
	printf "# TYPE %s_tranfer_tx Gauge\n" "$prom_key"
	# C'est cool, awk
	awk -v promkey="girofle_wireguard" 'NR>2 {		# Vire la première ligne
		ip=$5;
		# Oui, ne fonctionne que pour ipv4
		# Vire le netmask, coupe en octets
		gsub(/...$/,"",ip);split(ip,bytes,".");
		pi=""; # Ip, à l envers
		host="(unknown)";
		for (i=length(bytes);i>1;i--)
			pi=pi bytes[i] ".";
		pi=pi bytes[1];
		# Là j ai l adresse a l srevne
		cmd="dig -t PTR +short +answer " pi ".in-addr.arpa | grep -v lan";
		# dig renvoie par ex:
		# cactus.lan.
		# cactus.
		cmd | getline host;
		# Suppression du . final
		gsub(/\.$/,"",host);
		printf "%s_latest_handshake{peer=\"%s\",endpoint=\"%s\",host=\"%s\"} %i\n",promkey,$2,$4,host,$6;
		printf "%s_transfer_rx{peer=\"%s\",endpoint=\"%s\",host=\"%s\"} %i\n",promkey,$2,$4,host,$7;
		printf "%s_transfer_tx{peer=\"%s\",endpoint=\"%s\",host=\"%s\"} %i\n",promkey,$2,$4,host,$8;
		}' "$t"
	rm -f "$t"
	g_unlock_me
}
function _sponge() {
	local t="$(mktemp /tmp/.the-watcher-prometheus.sponge.XXXXXX)"
	if [ -z "$SPONGED_OUT_FILE" ] ; then
		cat
	else
		cat > "$t"
		if [ -s "$t" ] ; then
			mv -f "$t" "$SPONGED_OUT_FILE" && chmod a+r "$SPONGED_OUT_FILE"
		fi
	fi
	rm -f "$t"
}
function main() {
	( _ec=0
	while [ -n "$1" ] ; do
		case $1 in
			--cron)
				if [[ $2 =~ ^[0-9]+$ ]] ; then
					g_sleep_under_cron "${2}" ; shift			# Sleep random (5min, par défaut) si je tourne sous cron
				else
					g_sleep_under_cron 300
				fi
				;;
			memory=*)			watcher_memory $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ');;
			cpu=*)				watcher_proc_cpu $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ');; # idem
			oo=*)				watcher_oo $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ') ;; # Ex : oo=chif.fr
			backup)				watcher_backup ;;
			mail)				watcher_mail ;;
			fpmstatus)			watcher_phpfpm_status ;;
			phplogs)			watcher_php_logs;;
			mailqueue)			watcher_mailqueue;;
			mailcow_mailqueue)	watcher_mailcow_mailqueue;;
			mailcow_sogo)		watcher_mailcow_sogo;;
			mailcow_sogo=*)		watcher_mailcow_sogo $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ');;		# Permet de parser un fichier de logs
			proc=*)				watcher_proc $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ') ;; # Ex : proc=php-fpm#php,nginx
			http=*)				watcher_http $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ');;		# url[lcode-ucode]#label
			cert=*)				watcher_cert $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ');;		# fqdn,fqdn
			dnsmasq)			watcher_dnsmasq ;;
			unbound)			watcher_unbound ;;
			nc)					watcher_nc ;;
			nc-activity)		watcher_nc_activity ;;
			docker=*)			watcher_docker $(cut -d '=' -f 2 <<< "$1" | tr ',' ' ');;
			docker)				watcher_docker;;
			wireguard)			watcher_wireguard;;
		esac
		# Un "ou" logique pour garder une trace des codes de retour
		_ec="$(( _ec | $?))"
		shift
	done
	return $_ec ) | _sponge
}
main "$@"
