#!/bin/bash
# List discourse instances based on the #discourse tag

enabled_instances=/etc/nginx/sites-enabled
DOMAINS=()


#ls $enabled_instances
for f in $enabled_instances/*.conf; do
	#echo $f
	if [[ $(cat $f | sed '2,$d') =~ ^\#dis ]] ; then
		DOM=$(cat $f | grep "server_name " | sed 's/.*server_name\ //' | sed 's/\;//' | sed 's/\ /\n/')
		DOMAINS+=($DOM)
		#echo $DOM
	fi;
done

UNIQDOMAINS=(); while IFS= read -r -d '' x; do UNIQDOMAINS+=("$x"); done < <(printf "%s\0" "${DOMAINS[@]}" | sort -uz)

for d in  ${UNIQDOMAINS[@]}; do
	echo $d
done
