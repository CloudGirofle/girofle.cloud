#!/bin/bash
# Test if all notify push services are running
logfile="/tmp/notifypush_check"
allNP=$(cd "/etc/systemd/system/";ls notifypush_*)
rc=0

date > $logfile
for d in $allNP; do
    systemctl is-active --quiet $d ; retcod=$?
    if [[ $retcod -ne 0 ]]; then
	rc=1
	echo "service $d is not running" >> $logfile
    fi
done
exit $rc
