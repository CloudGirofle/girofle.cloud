#!/usr/bin/python3
#from prometheus_client import start_http_server, Gauge
import requests
import time
import json
from dotenv import dotenv_values

domain = "https://chat.alternatiba.eu/" # Serveur, avec trailing slash
prometheus_file = "/var/lib/prometheus/node-exporter/mattermost.prom"
SECRET_ENV = "/root/.girofle.env"

# Liste des endpoints
endpoints = [
    #"/api/v4/analytics/old?team_id=775s1mmgxtdr98fsfsdcifz31a",        #Changez le team id
    "/api/v4/analytics/old"
]

# Chargement du fichier d'environnement
config = dotenv_values(SECRET_ENV)
mattermost_token = config["MATTERMOST_TOKEN"]

# Création de la gauge qui renvois les metrics via le serveur http
#gauge = Gauge('mattermost_metrics', 'Description of gauge', labelnames=('metric_name', 'endpoint'))

def get_api_data(endpoint):
    headers = {
        f"Authorization": f"Bearer {mattermost_token}",
        "Content-Type": "application/json"
    }
    url = f"{domain}{endpoint}" # on génère l'URL du endpoint
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to retrieve data: {response.status_code}")
        return None

def process_data(endpoint, data):
    allmetrics = []

    # Format metrics
    for item in data:
        metric_name = item.get('name')
        value = item.get('value', 0)
        #gauge.labels(metric_name, endpoint).set(value)

        prom_help =  f"# HELP mattermost_{metric_name} A Mattermost metric."
        prom_type =  f"# TYPE mattermost_{metric_name} Gauge"
        prom_value = f"mattermost_{metric_name} {value}"
        allmetrics += [prom_help, prom_type, prom_value]

    # Save metrics
    with open(prometheus_file, 'w') as f:
        for l in allmetrics:
            f.write(l+"\n")

        
def main_server():
    # Démarrer le serveur HTTP pour exposer les métriques à Prometheus
    start_http_server(8000)
    while True:
        for endpoint in endpoints:
            data = get_api_data(endpoint)
            if data:
                process_data(endpoint, data)
        time.sleep(15)  # Attendre 15 secondes avant de faire les prochains appels API

def main():
    for endpoint in endpoints:
        data = get_api_data(endpoint)
        if data:
            process_data(endpoint, data)
        
if __name__ == '__main__':
    main()
