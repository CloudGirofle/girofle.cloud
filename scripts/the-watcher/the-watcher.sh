#!/usr/bin/env bash
db="$HOME/the-watcher.db"
MAIL_MESSAGE="$(mktemp /tmp/.the-watcher.mail.XXXXXX)"
E_OK=0
E_INVALID_PARAM=1
export PS4='> @$BASH_LINENO '
if ! sqlite3 -version >/dev/null 2>&1 ; then
	echo 'Besoin de sqlite3'
	exit 1
fi
# Traduction service <-> appli
DIR_EXE_ABS="$(dirname "$(realpath "$0")")"
declare -A APPS
APPS[oo]="onlyoffice7 onlyoffice8 onlyoffice-old"
APPS[docker]="docker"
APPS[ipv6]="ipv6"
APPS[notifypush]="notifypush"
APPS[vpn]="vpn"
APPS[mm]="chat.alternatiba.eu"
APPS[dc]="" # calculé dynamiquement au runtime

function main() {
	local svcs svc
	declare -a apps
	while getopts 'sha:' opts ; do
		case "$opts" in
			a)  IFS=',' read -r -a svcs <<< "${OPTARG}" ;;
			h)  usage; exit $E_OK ;;
			s)  SHOW='y';;
			*)  echo '??' ; exit $E_INVALID_PARAM ;;
		esac
	done
	testDb
	for svc in ${svcs[*]} ; do
		if [ -n "$SHOW" ] ; then
			for app in ${APPS[$svc]} ; do
				printf "%s: %d\n" "$app" "$(getCode $app)"
			done
		else
			svcname=$(cut -d '=' -f 1 <<< "$svc")
			if [[ "$svc" =~ .*=.* ]] ; then
				svcarg=$(cut -d '=' -f 2 <<< "$svc")
			else
				svcarg=''
			fi
			if type -p watcher_$svcname ; then
				eval watcher_$svcname $svcarg
			fi
		fi
	done
	sendMail girofle@tech.girofle.cloud
}
function usage() {
	local apps
	local COLOR_RST="\e[0m"			# Reset
	local COLOR_OPT="\e[0;36m"		# Cyan
	local COLOR_PRM="\e[0;33m"		# Jaune pâle
	local COLOR_WHT="\e[1;37m"		# Blanc vif
	apps=( $( grep -E '^function +watcher_' "$0" | sed -re 's/function +watcher_//' -re 's/([a-zA-Z0-9]*).*/\1/' ) )
	echo
	echo -e "$COLOR_WHT$(basename "$0") $COLOR_OPT-a$COLOR_PRM <service,service,...>$COLOR_RST		# Lance les check pour les applis 'app'"
	echo -e "$COLOR_WHT$(basename "$0") $COLOR_OPT-a$COLOR_PRM <service,service,...> $COLOR_OPT-s$COLOR_RST	# Affiche les status des applis 'app'"
	echo
	echo -e "${COLOR_PRM}service${COLOR_RST}: ${apps[*]}"
	echo
}
function testDb() {
	if [ ! -s "$db" ] ; then
		sqlite3 -echo "$db" "create table if not exists 'watched' (app TEXT PRIMARY KEY, state TEXT);"
		sqlite3 -echo "$db" "create table if not exists 'history' (id INTEGER PRIMARY KEY,t TIMESTAMP DEFAULT CURRENT_TIMESTAMP, app TEXT, old TEXT, new TEXT);"
	fi
}
function message() {
	echo "$1" >> "$MAIL_MESSAGE"
}
function sendMail() {
	if [ -s "$MAIL_MESSAGE" ] ; then
		for d in "$@" ; do
			mail "$d" -s "[Girofle] Watcher report" <"$MAIL_MESSAGE"
		done
	fi
}
function codeHasChanged() {
	local app="$1" code="$2"
	oldCode="$(sqlite3 "$db" "select state from 'watched' where app='$app';")"
	[ "x$oldCode" != "x$code" ]
}
function storeCode() {
	local app="$1" code="$2"
	local oldcode="$(getCode "$app")"
	if [ -z "$(sqlite3 "$db" "select app from 'watched' where app='$app';" )" ] ; then
		sqlite3 "$db" "insert into 'watched' (app,state) values ('$app','$code');"
	else
		sqlite3 "$db" "update 'watched' set state='$2' where app='$app';"
	fi
	if codeHasChanged "$app" "$oldcode" ; then
		sqlite3 "$db" "insert into history (app,old,new) values ('$app','$oldcode','$code');"
	fi
}
function getCode() {
	local app="$1"
	sqlite3 "$db" "select state from 'watched' where app='$app';"
}
function watcher_ping() {
	local host="$1" app="$(cut -d '_' -f 2 <<< ${FUNCNAME[0]})"
	ping -c 1 -W 2 -n $host >/dev/null ; _ec=$?
	if codeHasChanged "$app" "$_ec" ; then
		if [ "$_ec" -eq "2" ] ; then
			message "$host not responding (code changed from $(getCode "$app") to $_ec)"
		elif [ "$_ec" -eq "0" ] ; then
			message "Can ping $host (code changed from $(getCode "$app") to $_ec)"
		else
			message "ping $host: code changed from $(getCode "$app") to $_ec"
		fi
	fi
	storeCode "$app" "$_ec"
}
function watcher_ipv6() {
	local app="${APPS[ipv6]}"
	ping6 -c 1 -W 2 2001:4860:4860::8888 >/dev/null ; _ec=$?
	if codeHasChanged "$app" "$_ec" ; then
		message "IPv6 changed from $(getCode "$app") to $_ec"
	fi
	storeCode "$app" "$_ec"
}

function watcher_notifypush() {
	local app="${APPS[notifypush]}"
	${DIR_EXE_ABS}/nextcloud_notify_push.sh ; _ec=$?
	if codeHasChanged "$app" "$_ec" ; then
	    message "NotifyPush changed from $(getCode "$app") to $_ec"
	    message "$(cat /tmp/notifypush_check)"
	fi
	storeCode "$app" "$_ec"
}

function watcher_vpn() {
	local app="${APPS[vpn]}" tmp="$(mktemp /tmp/.the-watcher.vpn.XXXXXX)"
	${DIR_EXE_ABS}/vpn.py &>$tmp  ; _ec=$?
	if codeHasChanged "$app" "$_ec" ; then
	    message "VPN changed from $(getCode "$app") to $_ec"
	    message "$(<"$tmp")"
	fi
	rm -f "$tmp"
	storeCode "$app" "$_ec"
}

function watcher_oo() {
	for app in ${APPS[oo]} ;do
		code=$(curl -w "%{http_code}" -Iks "https://$app.girofle.cloud/info/info.json"  -o /dev/null)
		if codeHasChanged "$app" "$code" ; then
			message "$app http return code changed from $(getCode "$app") to $code"
		fi
		storeCode "$app" "$code"
	done
}

function watcher_mm() {
	for app in ${APPS[mm]} ;do
		code=$(curl -w "%{http_code}" -Iks "https://${app}"  -o /dev/null)
		if codeHasChanged "$app" "$code" ; then
			message "$app http return code changed from $(getCode "$app") to $code"
		fi
		storeCode "$app" "$code"
	done
}

function watcher_dc() {
  APPS[dc]="$(${DIR_EXE_ABS}/list_discourse.sh | tr '\n' ' ')"
	for app in ${APPS[dc]} ;do
		code=$(curl -w "%{http_code}" -Iks "https://${app}"  -o /dev/null)
		if codeHasChanged "$app" "$code" ; then
			message "$app http return code changed from $(getCode "$app") to $code"
		fi
		storeCode "$app" "$code"
	done
}

function watcher_docker() {
	local app="docker" since="5m" t="$(mktemp /tmp/.the-watcher.docker.XXXXXX)"
	local containers=("$(sudo docker ps --format "{{.Names}}")")
	for container in ${containers[@]} ; do
		sudo docker logs --since "$since" "$container" >> "$t"
	done

	code="$(grep -c Error "$t")"
	if codeHasChanged "$app" "$code" ; then
		message "The is $code error(s) in docker's log since last $since"
		message "$(<"$t")"
	fi
	rm -f "$t"
	storeCode "$app" "$code"
}
main "$@"
rm -f "$MAIL_MESSAGE"
