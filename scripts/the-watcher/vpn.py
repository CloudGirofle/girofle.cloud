#!/usr/bin/python3
# By CG, Jul 2023, GPLv3+

import dns.resolver
import subprocess
import re
from subprocess import run
from ipaddress import IPv4Address, IPv4Network

# apt install python3-netaddr

# ==== Variables
NETWORK = [
    "mutu.lan",
    "mutu2.lan",
    "mutu3.lan",
    "forum.lan",
    "melisse.lan",
    "cornichon.lan",
    "castoroil.lan",
    "castoroil2.lan",
    "bacopa.lan",
    "cactus.lan",
]  # list of domains

DNS_SERVER_ADDRESS = "127.0.0.53"
WG_INTERFACE_NAME = "wg1"
WG_SERVICE_NAME = "wg-quick@" + WG_INTERFACE_NAME

# ==== Init

my_resolver = dns.resolver.Resolver()

# ==== Functions


def print_(s):
    if verbose:
        print(s)


def test_dns(dns, domains):
    # test if a list of domains can be resolved by the DNS
    # return the resolution
    my_resolver.nameservers = [dns]
    r = []
    for d in domains:
        answer = my_resolver.resolve(d)
        assert len(answer) == 1, "DNS sent more than one IP: {}".format(answer)
        print_("{} resolves to: {}".format(d, answer[0].to_text()))
        r.append((d, answer[0].to_text()))
    return r


def _regexp_ip(s):
    # pattern = re.compile( r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)([/][0-3][0-2]?|[/][1-2][0-9]|[/][0-9])?")
    pattern = re.compile("(?:\d{1,3}\.){3}\d{1,3}(?:/\d\d?)?")
    return pattern.search(s)[0]


def test_vpn(ips):
    # Test if service is active
    p = run(["systemctl", "is-active", "--quiet", WG_SERVICE_NAME])
    assert p.returncode == 0, "Systemd daemon {} is not running properly.".format(WG_SERVICE_NAME)

    # Test if interface exists
    p = run(["ip", "addr", "show", "dev", WG_INTERFACE_NAME], stdout=subprocess.DEVNULL)
    assert p.returncode == 0, f"interface {WG_INTERFACE_NAME} does not seem to exist"

    # Get subnet
    c = "ip -o -f inet addr show dev " + WG_INTERFACE_NAME
    p = run(c.split(" "), capture_output=True, text=True)  # Python >= 3.7 only
    subnets = [i for i in p.stdout.split("\n") if "scope global" in i]
    subnets = [_regexp_ip(i) for i in subnets]
    our_subnet = subnets[0]
    # Check that all resolved IPs are in subnet

    for ip in ips:
        assert IPv4Address(ip) in IPv4Network(our_subnet, strict=False), "{} not in {} subnet {}".format(
            ip, WG_INTERFACE_NAME, our_subnet
        )

    # Get result of wg show
    p = run(["wg", "show"], capture_output=True, text=True)  # Python >= 3.7 only
    server_ip = our_subnet.split("/")[0]
    peers = [i for i in p.stdout.split("\n\n") if i.startswith("peer")]
    peers_ips = [[ii for ii in i.split("\n") if ii.strip().startswith("allowed ips:")][0] for i in peers]
    peers_ips = [_regexp_ip(i) for i in peers_ips]
    vpn_network_ips = [server_ip] + peers_ips

    # Check that all ips are peers
    for ip in ips:
        assert any([IPv4Address(ip) in IPv4Network(i, strict=False) for i in vpn_network_ips]), "{}:{}".format(
            ip, peers_ips
        )

    # Ping
    err = []
    for ip in ips:
        p = run(["ping", "-c", "5", "-4", ip], stdout=subprocess.DEVNULL)
        if p.returncode != 0:
            err.append(ip)
    assert len(err) == 0, "Cannot ping the ips after 5 tries: {}".format(err)


if __name__ == "__main__":
    verbose = False
    DOMAIN_IP = test_dns(DNS_SERVER_ADDRESS, NETWORK)
    test_vpn([i[1] for i in DOMAIN_IP if i[0].endswith(".lan")])
