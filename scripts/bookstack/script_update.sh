#!/bin/bash

#Ce script est conçu pour un passage de la version 23.01.1 vers la version 23.12.2.
#Avant d'effectuer la mise à niveau, il est important d'exécuter le script qui check la compatibilité pour déterminer si la machine est apte à accueillir la mise à jour de bookstack v23.12.2.
# Mutu semble bien répondre aux exigences minimum.

#Dans le cas ou y'a un hardening git, ou une initialisation du repo, il faut ajouter un le path de l'instance comme une safe directory en décommentant la ligne suivante
#git config --global --add safe.directory /var/www/html/bookstack

# Permet à composer de s'exécuter en tant que superuser.
export COMPOSER_ALLOW_SUPERUSER=1

# Récupère les derniers tags du dépôt distant
git fetch --tags

# Récupère le dernier tag de la version disponible
latest_version=$(git describe --tags "$(git rev-list --tags --max-count=1)")

# Affiche la dernière version disponible
echo "La dernière version disponible de BookStack est : $latest_version"

# Demande à l'utilisateur s'il souhaite continuer avec la dernière version disponible
read -p "Voulez-vous continuer avec l'installation de cette version ? (y/n) " -n 1 -r
echo    # (optionnel) ajoute une ligne vide pour l'esthétique
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Installation annulée."
    exit 1
fi

echo "Continuation de l'installation..."

# Mise à jour du répo local à partir du répo distant 'remote' sous le nom origin à la dernière version
# Attention Attention, il faut bien mettre dans .gitignores les dossiers qu'on ne veux pas supprimer (Les données upload par les utilisateurs ?), autrement, les dossiers qui ne sont pas inclus seront supprimer,
# Par defaut il y'a déjà une liste, lors des testes, il ne m'a pas supprimer mes données que j'ai upload pour le test.

git pull origin "$latest_version"

# Installation des dépendances nécessaires au projet
composer install -n --no-dev

# Effectue une migration de la base de données
php artisan migrate --no-interaction --force

# Nettoyage du système
php artisan cache:clear
php artisan config:clear
php artisan view:clear

echo "Installation terminée."
