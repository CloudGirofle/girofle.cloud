#!/bin/bash

# Vérifier la version PHP si elle est > ou < à la version requise 8.0.2.
php_version=$(php -v | head -n 1 | cut -d " " -f 2)
if [[ "$(printf '%s\n' "$php_version" "8.0.2" | sort -V | head -n1)" = "8.0.2" ]]; then
    echo "PHP Version = $php_version . ### VOTRE VERSION EST COMPATIBLE ###"
else
    echo "PHP Version = $php_version . ### VOTRE VERSION EST INCOMPATIBLE ### ! Installez une version >= 8.0.2"
fi

# Vérifier la version MariaDB si elle est > ou < à la version requise 10.2
mariadb_version=$(mysql -V | awk '{ print $5 }' | cut -d ',' -f1)
if [[ "$(printf '%s\n' "$mariadb_version" "10.2" | sort -V | head -n1)" = "10.2" ]]; then
    echo "MariaDB version =  $mariadb_version . ### VOTRE VERSION EST COMPATIBLE ###"
else
    echo "MariaDB version $mariadb_version . ### VOTRE VERSION EST INCOMPATIBLE ### ! Installez une version >= 10.2"
fi

# Vérifier Composer
composer_version=$(composer --version -n | cut -d " " -f 3)

if [[ "$(printf '%s\n' "$composer_version" "2.0" | sort -V | head -n1)" = "2.0" ]]; then
    echo "Composer version $composer_version ### VOTRE VERSION EST COMPATIBLE ###"
else
    echo "Composer version $composer_version  ### VOTRE VERSION EST INCOMPATIBLE ### ! Installez une version >= 2.0"
fi

# Vérifier Git
if command -v git >/dev/null 2>&1; then
    echo "GIT INSTALLATION OK"
else
    echo "GIT INSTALLATION PAS OK. Installez git et réessayez."
fi

# Vérifier Nginx ou Apache
if systemctl is-active --quiet nginx; then
    echo "NGINX est actif"
elif systemctl is-active --quiet apache2; then
    echo "Apache2 est actif"
else
    echo "Aucun serveur web compatible PHP n'est actif"
fi
