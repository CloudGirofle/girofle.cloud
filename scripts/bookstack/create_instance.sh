#!/usr/bin/bash
# This script must be ran with admin rights
# Usage: sudo ./create_instance.sh yolo.girofle.cloud

# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR
########## ARGS  ##########

DIR_EXE=$(dirname $0)
DIR_EXE_ABS=$(realpath $0)
THE_EXE=$(basename $0 .sh)

if [[ $1 == "" ]]
then
  echo "Please enter the host name of the new instance (ex: \`${THE_EXE}.sh mutu.girofle.cloud\`)"
  exit 1
fi

########## CONSTANTS ##########

BASE_DIR="/var/www/"
DOMAIN="${1}"
INSTALL_DIR="${BASE_DIR}${DOMAIN}/"
tmp_n=${DOMAIN//\./_} # use domain and replace . by _
DATABASE_USER=${tmp_n//\-/_}  # use domain and replace - by _
DATABASE_NAME="wiki_"${DATABASE_USER}
NGINX_TEMPLATE_PATH=${DIR_EXE}/../conf/bookstack-nginx.conf.tmpl
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${DOMAIN}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${DOMAIN}.conf

########## TLS ##########
# setup certbot
echo -e "\e[32mSetting up TLS certificate ...\e[0m"
certbot certonly --noninteractive --nginx --email contact@girofle.cloud --agree-tos --expand -d ${DOMAIN}

########## NGINX ##########

if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
then 
  echo -e "\e[32mCreating nginx configuration file at ${NGINX_CONF_FILE_PATH} and link to it ${NGINX_CONF_LINK_PATH} ...\e[0m"
    cp ${NGINX_TEMPLATE_PATH} ${NGINX_CONF_FILE_PATH}
    sed -i 's/INSTANCE_HOST_NAME/'${DOMAIN}'/g' ${NGINX_CONF_FILE_PATH}
    ln -s ${NGINX_CONF_FILE_PATH} ${NGINX_CONF_LINK_PATH}
  echo "Nginx is reloading ..."
  systemctl reload nginx
else
  echo -e "\e[32mNginx configuration file already exists. Skipping nginx configuration.\e[0m"
fi


########## INPUTS ##########

# Get database password
if [[ ! -f ${DATABASE_PASSWORD} ]]
then
  read -sp "Database password not found in envs, please prodive a password: " DATABASE_PASSWORD
  echo ""
fi

# Get Bookstack admin password
if [[ ! -f ${ADMIN_PASSWORD} ]]
then
  read -sp "Bookstack admin password not found in envs, please prodive a password:" ADMIN_PASSWORD
  echo ""
fi

# Get user info
IFS= read -p "Other admin login: " USER_NAME
read -p "Other admin email address: " USER_EMAIL
read -sp "Other admin password: " USER_PASSWORD

########## DATABASE ##########
# Create database
echo -e "\e[32mCreating database $DATABASE_NAME ...\e[0m"
mysql -uroot --execute="CREATE DATABASE ${DATABASE_NAME};"
mysql -uroot --execute="CREATE USER ${DATABASE_USER}@'localhost' IDENTIFIED BY '${DATABASE_PASSWORD}'; GRANT ALL ON ${DATABASE_NAME}.* TO ${DATABASE_USER}@'localhost'";
mysql -uroot --execute="FLUSH PRIVILEGES";

########## BOOKSTACK ##########
if [[ ! -d ${INSTALL_DIR} ]] # Base download
then
   cd $BASE_DIR
   git clone https://github.com/BookStackApp/BookStack.git --branch release --single-branch ${DOMAIN}
   chown www-data:www-data -R ${DOMAIN}
else
  echo -e "\e[32mBookstack files already exist in ${INSTALL_DIR}. Skipping nextcloud file configurations.\e[0m"
fi


cd ${INSTALL_DIR}   
sudo -u www-data composer install --no-dev

# DBG
#export DOMAIN=caca.girofle.cloud
#export DATABASE_NAME="wiki_caca_girofle_cloud"
#export DATABASE_USER="caca_girofle_cloud"
#export DATABASE_PASSWORD=cacaboudin

sudo -u www-data cp .env.example .env
sed -i "s/APP_URL=.*/APP_URL=https:\/\/${DOMAIN}/g" .env
sed -i "s/DB_HOST=.*/DB_HOST=localhost/g" .env
sed -i "s/DB_DATABASE=.*/DB_DATABASE=${DATABASE_NAME}/g" .env
sed -i "s/DB_USERNAME=.*/DB_USERNAME=${DATABASE_USER}/g" .env
sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=${DATABASE_PASSWORD}/g" .env
sed -i "s/MAIL_DRIVER=.*/MAIL_DRIVER=sendmail/g" .env
sed -i "s/MAIL_FROM=.*/MAIL_FROM=bookstack@${DOMAIN}/g" .env
echo "\n# Sets application language to French" >> .env
echo "APP_LANG=fr" >> .env

# Besoin de configurer MAIL_FROM et MAIL_FROM_NAME et MAIL8DRIVER=sendmail
sudo -u www-data php artisan key:generate # Generate a unique application key.
sudo -u www-data php artisan migrate # Update the database.

# DBG
#export USER_EMAIL=
#export USER_LOGIN=
#export USER_PASSWORD=
sudo -u www-data php artisan bookstack:create-admin --email="${USER_EMAIL}" --name="${USER_LOGIN}" --password="${USER_PASSWORD}"

echo "IMPORTANT!!! log in using admin@admin.com / password and change the password"
