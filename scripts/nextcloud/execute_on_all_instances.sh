#!/usr/bin/bash
# GPLv3+
# List available instances, & build a nice summary table when the stats keyword is used

for path in /var/www/*/; do
    if [ -d "${path}" ] && [ -f "${path}/occ" ]; then # Test if this is a Nextcloud instance
        echo "dO SOMETHING"
	cd $path
    fi
done
