#!/usr/bin/python3
# https://docs.nextcloud.com/server/latest/developer_manual/client_apis/WebDAV/trashbin.html
# inspiration : https://github.com/norcon/nextcloud-trash-restore-original-location/


import subprocess, datetime, logging, os
import xmltodict
from concurrent.futures import ThreadPoolExecutor

logging.basicConfig(filename='restore.log', level=logging.DEBUG)

NC_URL = "https://nuage.bl-evolution.com"
NC_USER = "carole.donato"
NC_PASS = "o6r9HJB75SjZMSO5"
CURL_FOLDER = f"./{NC_USER}/"
list_of_files = "{NC_USER}.xml"

get_xml = f"""
curl "{NC_URL}/remote.php/dav/trashbin/{NC_USER}/trash/" \
--user "{NC_USER}:{NC_PASS}" \
  -H 'Content-Type: application/xml; charset=UTF-8' \
  -H 'Depth: 1' \
  -X PROPFIND \
  --data-binary '<d:propfind xmlns:d="DAV:" xmlns:oc="http://nextcloud.org/ns"> \
    <d:prop> \
        <oc:trashbin-filename /> \
        <oc:trashbin-original-location /> \
        <oc:trashbin-deletion-time /> \
        <d:owner-id /> \
        <d:getcontentlength /> \
        <d:getlastmodified/> \
        <d:resourcetype/> \
    </d:prop> \
</d:propfind>' > {list_of_files}
"""

logging.debug(get_xml)

with open(list_of_files, 'r') as f:
    raw = xmltodict.parse(f.read())

to_restore = []
no_restore = []

logging.info("Found {} files that can be restored".format(len(raw['d:multistatus']['d:response'])))

for f in raw['d:multistatus']['d:response']:
    if type(f['d:propstat']) is list:
        if len(f['d:propstat']) == 2:
            f['d:propstat'] = {**f['d:propstat'][0], **f['d:propstat'][1]} # merge dicts
        else:
            print("NOT IMPLEMENTED")
    c = f['d:propstat']['d:prop']
    if 'nc:trashbin-original-location' in c and c['nc:trashbin-original-location'] is not None:
        to_restore.append(f)
    else:
        no_restore.append(f)

logging.info("{} files to restore and {} ignored".format(len(to_restore), len(no_restore)))

date_begin = "2024-03-19 00:00:00"
date_end = "now"


d_b = datetime.datetime.strptime(date_begin, '%Y-%m-%d %H:%M:%S')
d_e = datetime.datetime.now()

for r in no_restore:
    logging.info("Not restoring: {}".format(r['d:href']))

n_restore = len(to_restore)
errors = []

def download_files(ir):
    i,r = ir
    rr = r['d:propstat']['d:prop']
    href = r['d:href']
    da = datetime.datetime.fromtimestamp(int(rr['nc:trashbin-deletion-time']))#.strftime('%c')
    path_and_name = rr['nc:trashbin-original-location']
    path = os.path.dirname(path_and_name)

    ## Filter by date
    if not (d_b < da < d_e): # we skip if not in the date interval
        return

    download_directory = os.path.join(CURL_FOLDER, path)
    download_directory_and_name = os.path.join(CURL_FOLDER, path_and_name)
    if os.path.isfile(download_directory_and_name):
        logging.info("{}/{} Skipping existing file: {} -> {}".format(i+1, n_restore, href, path_and_name))
        os.utime(download_directory_and_name, (da.timestamp(), da.timestamp()))
        return

    curl_command = [
            "curl",
            "--retry", "999",  # Anzahl der Wiederholungsversuche
            "--retry-delay", "5",  # Verzögerung zwischen den Versuchen in Sekunden
            NC_URL + href,
            "--output", download_directory_and_name,
            "--user", f"{NC_USER}:{NC_PASS}",
        ]

    try:
        if not os.path.isdir(download_directory):
            os.makedirs(download_directory)
        subprocess.run(curl_command, check=True)
        os.utime(download_directory_and_name, (da.timestamp(), da.timestamp()))
        logging.info("{}/{} SUCCESS: {} -> {}".format(i+1, n_restore, href, path_and_name))
    except:
        errors.append((href, path_and_name))
        logging.error("{}/{} ERROR: {} -> {}".format(i+1, n_restore, href, path_and_name))

#for (i,r) in enumerate(to_restore):
#    download_files(i,r)

max_parallel_threads = 30
print(list(enumerate(to_restore))[0])
if False:
    with ThreadPoolExecutor(max_workers=max_parallel_threads) as executor:
        r = executor.map(download_files, list(enumerate(to_restore)))
        for rr in r:
            print(rr)
else:
    # we build a list of duplicated files
    all_dups = {}
    for e in to_restore:
        k = e['d:propstat']['d:prop']['nc:trashbin-original-location']
        if k is not None:
            if k not in all_dups:
                all_dups[k] = []
            all_dups[k].append(e)
    all_dups = {k:v for (k,v) in all_dups.items() if len(v)>1}

    # we keep the most recent ones
    recent_dups = []
    for v in all_dups.values():
        el = v[0]
        print([i['d:propstat']['d:prop']['nc:trashbin-deletion-time'] for i in v])
        for e in v:
            if int(e['d:propstat']['d:prop']['nc:trashbin-deletion-time'])>int(el['d:propstat']['d:prop']['nc:trashbin-deletion-time']):
                el=e
                print("{}>{}".format(e['d:propstat']['d:prop']['nc:trashbin-deletion-time'],el['d:propstat']['d:prop']['nc:trashbin-deletion-time']))
        recent_dups.append(el)
    print(len(recent_dups))

if True:
    with ThreadPoolExecutor(max_workers=max_parallel_threads) as executor:
        r = executor.map(download_files, list(enumerate(recent_dups)))
        for rr in r:
            print(rr)

