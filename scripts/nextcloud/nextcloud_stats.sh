#!/usr/bin/bash
# GPLv3+
# List available instances, & build a nice summary table when the stats keyword is used

nacc=0;
ngb=0;
for path in /var/www/*/; do
    if [ -d "${path}" ] && [ -f "${path}/occ" ]; then # Test if this is a Nextcloud instance
        echo
        echo "-- $path is a Nextcloud instance";
        cd "${path}"
        echo "Nextcloud version `sudo -u www-data php occ -V`"

        if [[ $1 == "info" ]]
        then
            sudo -u www-data php occ update:check
            sudo -u www-data php occ onlyoffice:documentserver
            acc=`sudo -u www-data php occ user:list | wc -l`;
            nacc=$(( $nacc + $acc ));
            echo "Number of accounts: $acc";
            echo "Disk space: `du -h --max-depth=0 .`"
            gb=`du --max-depth=0 . | cut -d$'\t' -f1`
            ngb=$(( $ngb + $(( $gb * 1024 )) ));
        fi
        if [[ $1 == "upgrade" ]]
        then
            sudo -u www-data php ${path}/updater/updater.phar
        fi
        if [[ $1 == "optimize-db" ]]
        then
            sudo -u www-data php occ db:add-missing-indices
            sudo -u www-data php occ db:add-missing-primary-keys
            sudo -u www-data php occ db:add-missing-columns
            sudo -u www-data php occ db:convert-filecache-bigint
        fi
    else
        echo "-- $path is not a Nextcloud instance";
    fi
done

#### Final stats
if [[ $1 == "info" ]]
then
    echo
    echo "====";
    echo "Total number of accounts: $nacc"
    echo "Total disk space: `numfmt --to=iec-i $ngb`"
fi
