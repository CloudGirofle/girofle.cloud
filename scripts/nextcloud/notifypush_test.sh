#!/bin/bash
# Script to test whether notify_push is properly running on all instances
# To be run as root in a crontab

DIR=/var/www/
for i in $(ls /var/www); do
    d=${DIR}${i}
    if [[ -f ${d}/config/config.php ]];then
        echo "-- $i"
        cd $d
        sudo -u www-data php occ notify_push:self-test
        echo
    fi
done
