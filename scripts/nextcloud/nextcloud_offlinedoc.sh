#!/bin/bash
# Update an "offline" documentation, integrated to the Nextcloud Interface

# abort on error
set -e

function error_on_exit() {
  echo "Something went wrong on line $1." >> "$LOG_FILE"
  if [[ -d ${DOC_DIR}.bak ]]; then
	  echo "Restoring from backup." >> "$LOG_FILE"
    if [[ -d ${DOC_DIR} ]]; then
      rm -rf "${DOC_DIR}"
    fi
	  mv "${DOC_DIR}.bak" "${DOC_DIR}"
  fi
  exit 1
}
trap 'error_on_exit $LINENO' ERR

## Constants
DIR_EXE=$(dirname "$0")
DOC_DIR=/var/www/nextcloud_default_documentation
CSS_INJ_PATH=${DIR_EXE}/conf/bookstack/bookstack_injection.css
LOG_FILE="offlinedoc.log"

echo "offlinedoc started $(date +%d-%m-%Y\ %H:%M:%S)" >> "$LOG_FILE"

## Backup previous doc
if [[ -d ${DOC_DIR}.bak ]]
then
    echo "Backup folder ${DOC_DIR}.bak exists, aborting." >> "$LOG_FILE"
    exit 1
fi

if [[ -d ${DOC_DIR} ]]
then
    mv ${DOC_DIR} ${DOC_DIR}.bak
fi

## Download site
mkdir ${DOC_DIR}
wget --output-file=$LOG_FILE -nH -r -np -k --html-extension --tries=1 --waitretry=1 \
     -U "Mozilla/5.0 (Windows NT 6.1; rv:66.0) Gecko/20100101 Firefox/66.0" \
     -x --keep-session-cookies -D "docs.girofle.cloud" -B "https://docs.girofle.cloud/" \
     --reject-regex 'export/html' --reject-regex 'export/pdf' --reject-regex '/revisions/' \
     --reject-regex '/revisions.html' --reject 'search*,tags*' \
     https://docs.girofle.cloud/ -P ${DOC_DIR}/raw || true

## Inject CSS
CSS_PATH=$(ls ${DOC_DIR}/raw/dist/styles.css*)
cat "${CSS_INJ_PATH}" >> "${CSS_PATH}"

## Inject redirections
cp -r ${DOC_DIR}/raw/ ${DOC_DIR}/user
cp -r ${DOC_DIR}/raw/ ${DOC_DIR}/admin

cp ${DOC_DIR}/user/index.html ${DOC_DIR}/user/index.html.old
cp ${DOC_DIR}/admin/index.html ${DOC_DIR}/admin/index.html.old
cp "${DIR_EXE}/conf/bookstack/bookstack-index-user.html" "${DOC_DIR}/user/index.html"
cp "${DIR_EXE}/conf/bookstack/bookstack-index-admin.html" "${DOC_DIR}/admin/index.html"

## Remove backup
rm -rf ${DOC_DIR}.bak

## Deploy
for folder in /var/www/*; do
  if [[ -d ${folder}/core/doc ]]; then
      echo "Deploy on folder: ${folder}" >> $LOG_FILE
      if [[ -d ${folder}/core/doc.bak ]]; then
	  rm -rf "${folder}/core/doc.bak"
      fi
      sudo -u www-data mv "${folder}/core/doc" "${folder}/core/doc.bak"
      sudo -u www-data mkdir "${folder}/core/doc"
      sudo -u www-data ln -s ${DOC_DIR}/user/ "${folder}/core/doc/user"
      sudo -u www-data ln -s ${DOC_DIR}/admin/ "${folder}/core/doc/admin"
  fi
done

echo "offlinedoc terminated $(date +%d-%m-%Y\ %H:%M:%S)" >> "$LOG_FILE"
