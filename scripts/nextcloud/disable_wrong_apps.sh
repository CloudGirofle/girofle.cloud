for folder in /var/www/*; do
    if [[ -d ${folder}/core/doc ]]; then
        echo "Checking ${folder}"
        cd "${folder}"
        sudo -u www-data php occ app:disable richdocuments | grep -v "No such app enabled"
        sudo -u www-data php occ app:disable richdocumentscode | grep -v "No such app enabled"
        sudo -u www-data php occ app:disable documentserver_community | grep -v "No such app enabled"
    fi
done
