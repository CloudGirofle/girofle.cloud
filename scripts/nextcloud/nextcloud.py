"""
Script pour gérer les instances Nextcloud

ex: sudo python3 nextcloud.py
"""

import argparse
import datetime
import difflib
import json
import os
import logging
import subprocess
import glob
import socket
import shutil
from typing import Tuple, Union

try:
    from yaspin import yaspin

    YASPIN = True
except ModuleNotFoundError:
    # Error handling
    print("Package yaspin not installed. For nicer output, please install yaspin")
    YASPIN = False

logging.basicConfig(level=os.environ.get("LOG_LEVEL", "INFO"))

BASE_DIR = "/var/www/"
TARGET_VERSION = "29.0.12.2"
FIRSTRUNWIZARD_REPO = "https://framagit.org/CloudGirofle/firstrunwizard-girofle.git"
FIRSTRUNWIZARD_BRANCH = "v28"
WWW_USER = "www-data"
WWW_GROUP = "www-data"
NGINX_ROOT_DIR = "/etc/nginx/sites-enabled/"
NGINX_CONF_TEMPLATE = os.path.dirname(__file__) + "/conf/nginx.conf.tmpl"
FQDN = socket.getfqdn()


def spinner(func):
    def decorator(*args, **kwargs):
        text = args[0]
        if YASPIN:
            with yaspin(text=text) as sp:
                sp.color = "magenta"
                return func(*args, **kwargs)
        else:
            return func(*args, **kwargs)

    return decorator


class Instance:
    def __init__(self, name, path):
        self.name = name
        self.path = path
        self.version = "-"
        self.user_count = "-"
        self.onlyoffice = "-"
        self.disk_usage = "-"
        self.disk_usage_bytes = "-"

    def __repr__(self) -> str:
        return self.name

    @spinner
    def fetch_data(self) -> None:
        self.version = self.occ(["status", "--output=json"])["version"]
        self.user_count = len(self.occ(["user:list", "--output=json"]).keys())
        try:
            self.onlyoffice = self.occ(["onlyoffice:documentserver"]).split(" ")[-1].strip()
        except Exception as e:
            logging.debug(e)
        du = self.get_disk_usage()
        self.disk_usage = du[0]  # used to be commented
        self.disk_usage_bytes = du[1]

    def occ(self, args) -> str or dict:
        occ_cmd = ["php", self.path + "/occ"]

        logging.debug("[%s] Running occ command with args: " + " ".join(args), self.name)
        result = subprocess.check_output(occ_cmd + args, universal_newlines=True, user=WWW_USER, group=WWW_GROUP)
        logging.debug("[%s] " + result.replace("\n", ""), self.name)

        if "--output=json" in args:
            result = json.loads(result)

        return result

    def get_disk_usage(self) -> Tuple[str, int]:
        usage = 0
        for path, dirs, files in os.walk(self.path):
            for file in files:
                filename = os.path.join(path, file)
                usage += os.path.getsize(filename)
        disk_usage = "{}GB".format(usage // (2**30))

        return disk_usage, usage

    def upgrade(self) -> bool:
        # remove admin customized doc if it exists
        admin_doc = f"{self.path}/core/doc/admin"
        if os.path.islink(admin_doc):
            logging.info(f"Unlinking admin doc: {admin_doc}")
            os.unlink(admin_doc)

        # remove user customized doc if it exists
        user_doc = f"{self.path}/core/doc/user"
        if os.path.islink(user_doc):
            logging.info(f"Unlinking user doc: {user_doc}")
            os.unlink(user_doc)

        # run occ upgrade (to update the apps
        self.occ(["upgrade"])

        # run the Updater
        logging.info("Running Nextcloud Updater")
        updater_phar = ["php", self.path + "/updater/updater.phar", "--no-interaction", "--no-backup"]
        result = subprocess.run(updater_phar, universal_newlines=True, user=WWW_USER, group=WWW_GROUP)
        if result.returncode != 0:
            logging.error("Updater failed. Aborting.")
            return False
        else:
            self.version = self.occ(["status", "--output=json"])["version"]

        # replace documentation by ours
        if os.path.exists(admin_doc):
            shutil.move(admin_doc, admin_doc + ".bak")
        if os.path.exists(user_doc):
            shutil.move(user_doc, user_doc + ".bak")
        os.symlink("/var/www/nextcloud_default_documentation/admin/", admin_doc)
        os.symlink("/var/www/nextcloud_default_documentation/user/", user_doc)

        return True


class InstanceManager:
    def __init__(self, instance_filter: str, non_interactive: str = False, stop_at: str = "", exclude: bool = False):
        # init
        self.instance_filter = instance_filter
        self.non_interactive = non_interactive
        self.instances = []
        self.stop_at = stop_at
        self.exclude = exclude

        # get instances
        self.get_instances()

    def _confirm(self, message: str) -> bool:
        """Asks user confirmation if required."""
        if self.non_interactive:
            return True

        while True:
            response = input(message + " (Y/n) ?\n")
            if response.lower() in ["y", "yes", ""]:
                return True
            elif response.lower() in ["n", "no"]:
                return False
            else:
                print("Please answer with 'y' for yes or 'n' for no.")

    def get_instances(self) -> int:
        folders = [entry.path for entry in os.scandir(BASE_DIR) if entry.is_dir()]
        for folder in folders:
            if os.path.exists(folder + "/occ"):
                name = folder.split("/")[-1]

                # skip instances that are in filter
                if self.exclude is True:
                    if self.instance_filter and name in self.instance_filter:
                        continue
                # skip instances that are not in filter
                else:
                    if self.instance_filter and name not in self.instance_filter:
                        continue

                self.instances.append(Instance(name=name, path=folder))

        self.instances = sorted(self.instances, key=lambda x: x.name)

        if len(self.instances) == 0:
            print(f"No nextcloud instance found in {BASE_DIR}")

        return len(self.instances)

    def list_instances(self) -> None:
        """List the Nextcloud instances"""
        columns = ["name", "version", "users", "disk", "onlyoffice"]
        table_format = "{:<50} {:<9} {:>5} {:>8} {:<30}"

        # print table header
        print(table_format.format(*columns))
        print(table_format.format(*["-----"] * len(columns)))

        # statistics
        stats = {"user_count": 0, "disk_usage": 0, "n_instance": 0}

        # print table content
        for instance in sorted(self.instances, key=lambda x: x.name):
            try:
                instance.fetch_data()
                print(
                    table_format.format(
                        instance.name,
                        instance.version,
                        instance.user_count,
                        instance.disk_usage,
                        instance.onlyoffice,
                    )
                )
                stats["n_instance"] += 1
                stats["disk_usage"] += instance.disk_usage_bytes
                stats["user_count"] += instance.user_count
            except Exception as e:
                logging.warning(f"Could not get data from instance {instance.name}.")
                logging.warning(e)

        # print aggregated statistics
        print()
        print("TOTAL:")
        print("\t Instances: {}".format(stats["n_instance"]))
        print("\t Disk usage: {}".format(stats["disk_usage"] / (2**30)))
        print("\t Accounts: {}".format(stats["user_count"]))

    def upgrade_instances(self) -> None:
        """Upgrade the Nextcloud instances to the targeted version"""

        # Parse date of shutdown
        stop_date = None
        if self.stop_at != "":
            stop_date = datetime.datetime.strptime(self.stop_at, "%Y-%m-%d %H:%M:%S")

        for instance in self.instances:
            if stop_date and datetime.datetime.now() > stop_date:
                logging.info("Not starting next upgrade, its time to stop!")
                break

            instance.fetch_data()
            logging.info("[%s] Version: %s", instance, instance.version)

            while instance.version != TARGET_VERSION:
                # check if version is not higher that target version
                if int(instance.version.split(".")[0]) > int(TARGET_VERSION.split(".")[0]):
                    break
                elif int(instance.version.split(".")[0]) == int(TARGET_VERSION.split(".")[0]):
                    if int(instance.version.split(".")[1]) > int(TARGET_VERSION.split(".")[1]):
                        break
                elif int(instance.version.split(".")[0]) == int(TARGET_VERSION.split(".")[0]) and int(
                    instance.version.split(".")[1]
                ) == int(TARGET_VERSION.split(".")[1]):
                    if int(instance.version.split(".")[2]) >= int(TARGET_VERSION.split(".")[2]):
                        break
                if self._confirm(f"Would you like to upgrade instance {instance}"):
                    if instance.upgrade():
                        logging.info(
                            "[%s] Instance upgraded to version %s",
                            instance,
                            instance.version,
                        )
                    else:
                        break
                else:
                    break

    def clean_instances(self) -> None:
        """Clean the Nextcloud instances
        Only deletes old backup folders for now.
        """
        for instance in self.instances:
            # remove the old backup folders
            for folder_path in glob.glob(f"{instance.path}/data/updater-*/backups/nextcloud-*/"):
                if self._confirm(f"Would you like to delete folder {folder_path}?\n"):
                    logging.info(f"Deleting folder {folder_path}")
                    shutil.rmtree(folder_path)
                else:
                    logging.info(f"Not deleting folder {folder_path}")

    def firstrunwizard(self) -> None:
        """Update the First-Run-Wizard app with our custom app"""
        for instance in self.instances:
            # check if firstrunwizard app exists and that this is our custom app
            # we check if the .git/ folder exists to know that it's our custom app
            app_folder = f"{instance.path}/apps/firstrunwizard"
            if not os.path.exists(f"{app_folder}/.git/"):
                # Disabling firstrunwizard app
                logging.info("[%s] Disabling Firstrunwizard app.", instance)
                instance.occ(["app:disable", "firstrunwizard"])

                # remove existing app
                logging.info(f"Deleting folder {app_folder}")
                shutil.rmtree(app_folder)

                # clone Girofle firstrunwizard repo
                result = subprocess.run(
                    [
                        "git",
                        "clone",
                        "-b",
                        f"{FIRSTRUNWIZARD_BRANCH}",
                        "--single-branch",
                        "--depth",
                        "1",
                        f"{FIRSTRUNWIZARD_REPO}",
                        f"{instance.path}/apps/firstrunwizard/",
                    ],
                    universal_newlines=True,
                    user=WWW_USER,
                    group=WWW_GROUP,
                )
                if result.returncode != 0:
                    logging.error(
                        "[%s] Could not clone custom firstrunwizard. Aborting.",
                        instance,
                    )
                    continue
                else:
                    logging.info("[%s] Custom Firstrunwizard app cloned.", instance)

                # enable new app
                instance.occ(["app:enable", "firstrunwizard"])
            else:
                result = subprocess.run(
                    ["git", "pull", "origin", f"{FIRSTRUNWIZARD_BRANCH}"],
                    cwd=f"{instance.path}/apps/firstrunwizard/",
                    universal_newlines=True,
                    user=WWW_USER,
                    group=WWW_GROUP,
                )
                if result.returncode != 0:
                    logging.error("[%s] Could not pull custom firstrunwizard. Aborting.", instance)
                    continue
                else:
                    logging.info("[%s] Custom Firstrunwizard app updated.", instance)

    def onlyoffice(self) -> None:
        """Update the Onlyoffice app"""
        for instance in self.instances:
            try:
                logging.info("[%s] Updating Onlyoffice config.", instance)
                instance.occ(
                    [
                        "config:import",
                        f"{os.path.dirname(__file__)}/conf/onlyoffice8.json",
                    ]
                )
                instance.occ(["onlyoffice:documentserver", "--check"])
            except subprocess.CalledProcessError as e:
                logging.error("[%s] Could not update or check onlyoffice configuration", e)

    def config(self) -> None:
        """Check Nextcloud config and update it if necessary"""
        for instance in self.instances:
            self.current_instance = instance
            logging.info("[%s] Checking Nextcloud config.", instance)

            expected_system_configs = [
                ("default_language", "fr"),
                ("default_phone_region", "fr"),
                ("upgrade.disable-web", True),
                ("memcache.local", "\\OC\\Memcache\\APCu"),
                ("memcache.locking", "\\OC\\Memcache\\Redis"),
                ("filelocking.enabled", True),
                ("overwrite.cli.url", f"https://{instance.name}"),
                ("preview_max_memory", "256"),
                ("updatechecker", True),
                ("debug", False),
                ("trashbin_retention_obligation", "auto, 90"),
                ("versions_retention_obligation", "auto, 180"),
                ("updater.release.channel", "stable"),
                (
                    "redis",
                    {
                        "host": "localhost",
                        "port": 6379,
                        "dbindex": 0,
                        "timeout": 1.5,
                        "read_timeout": 1.5,
                    },
                ),
                ("maintenance_window_start", 1),
                ("appstoreenabled", True),
                ("appstoreurl", f"https://{FQDN}/nextcloud"),
                ("knowledgebase.embedded", True),
            ]

            # get configurations
            system_configs = instance.occ(["config:list", "--output=json", "--private"])["system"]
            self._array_to_dict(system_configs)

            # check configurations and offer to change them
            for conf in expected_system_configs:
                self._update_config(system_configs, conf[0], conf[1])

    @staticmethod
    def _array_to_dict(config: dict):
        """Convert Nextcloud array string into dict
        input: "redis": "array ('host' => 'localhost', 'port' => 6379, 'dbindex' => 0,'timeout' => 1.5, 'read_timeout' => 1.5)"
        output: "redis": {'host': 'localhost', 'port': 6379, 'dbindex': 0,'timeout': 1.5, 'read_timeout': 1.5}"
        """
        for key, val in config.items():
            if isinstance(val, str) and val.startswith("array"):
                new_val = "{" + (val.replace("array (", "").replace(")", "").replace("=>", ":").replace("'", '"')) + "}"
                config[key] = json.loads(new_val)

    def _update_config(
        self,
        system_configs: dict,
        key: str,
        val: Union[str, bool, list],
    ) -> None:
        """Set the Nextcloud configuration using occ, if necessary"""
        logging.debug("[%s] Checking config: %s", self.current_instance, key)
        if key not in system_configs or val != system_configs[key]:
            logging.warning(
                "[%s] Config %s is %s and should be %s.",
                self.current_instance,
                key,
                system_configs[key] if key in system_configs else "empty",
                val,
            )
            if self._confirm("Would you like to change the configuration value"):
                if isinstance(val, dict):
                    for subkey, subval in val.items():
                        self._set_config(" ".join([key, subkey]), subval)
                else:
                    self._set_config(key, val)

            logging.warning("[%s] Config %s set.", self.current_instance, key)

    def _set_config(self, key, val) -> list:
        command = ["config:system:set", f"--value={val}"] + key.split(" ")
        if isinstance(val, bool):
            command.append("--type=boolean")
        elif isinstance(val, float):
            command.append("--type=float")
        elif isinstance(val, int):
            command.append("--type=integer")

        self.current_instance.occ(command)

        return command

    def nginx(self) -> None:
        """Check Nginx config and update it if necessary"""
        for instance in self.instances:
            logging.info("[%s] Checking Nginx config.", instance)
            nginx_conf_file = NGINX_ROOT_DIR + instance.name + ".conf"

            # read files
            with open(NGINX_CONF_TEMPLATE, "r") as template, open(nginx_conf_file, "r") as nginx_file:
                # read files contents
                template_content = template.readlines()
                nginx_content = nginx_file.readlines()

            # replace template with instance values
            template_content = [line.replace("INSTANCE_HOST_NAME", instance.name) for line in template_content]
            template_content = [line.replace("#NOTIFY_PUSH_BLOCK ", "") for line in template_content]

            # check diff
            if template_content != nginx_content:
                logging.info("[%s] Nginx config file differs from template.", instance)
                if self._confirm("Would you like to see the diff"):
                    diff = difflib.unified_diff(template_content, nginx_content)
                    for line in diff:
                        print(line, end="")

                # rewrite Nginx config
                if self._confirm("Would you like to rewrite the config file"):
                    with open(nginx_conf_file, "w") as nginx_file:
                        nginx_file.writelines(template_content)

        if self._confirm("Would you like to restart the Nginx service"):
            result = subprocess.run(["systemctl", "reload", "nginx"])
            if result.returncode != 0:
                logging.error("Could not reload nginx service. Aborting.")
            else:
                logging.info("Nginx service reloaded")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("command", help="command", default="list", nargs="?", const=1)
    parser.add_argument("-y", "--non-interactive", action="store_true")
    parser.add_argument("--instances", nargs="?", default="")
    parser.add_argument(
        "--exclude", action="store_true", help="Reverse the instance filter to exclude the listed instances"
    )
    parser.add_argument("--stopat", nargs="?", default="")
    args = parser.parse_args()

    if args.command == "list":
        manager = InstanceManager(instance_filter=args.instances, exclude=args.exclude)
        manager.list_instances()
    elif args.command == "upgrade":
        manager = InstanceManager(
            instance_filter=args.instances,
            non_interactive=args.non_interactive,
            stop_at=args.stopat,
            exclude=args.exclude,
        )
        manager.upgrade_instances()
    elif args.command == "firstrunwizard":
        manager = InstanceManager(instance_filter=args.instances, exclude=args.exclude)
        manager.firstrunwizard()
    elif args.command == "onlyoffice":
        manager = InstanceManager(instance_filter=args.instances, exclude=args.exclude)
        manager.onlyoffice()
    elif args.command == "config":
        manager = InstanceManager(
            instance_filter=args.instances, non_interactive=args.non_interactive, exclude=args.exclude
        )
        manager.config()
    elif args.command == "nginx":
        manager = InstanceManager(
            instance_filter=args.instances, non_interactive=args.non_interactive, exclude=args.exclude
        )
        manager.nginx()
    elif args.command == "clean":
        manager = InstanceManager(
            instance_filter=args.instances, non_interactive=args.non_interactive, exclude=args.exclude
        )
        manager.clean_instances()
    else:
        print(f"Command not found {args.command}.")
