#!/usr/bin/bash
## migrate_instance.sh
# A script to migrate an instance to the mutu server
# By MW, GPLv3+, Oct. 2020

DIR_EXE=$(dirname $0)
THE_EXE=$(basename $0 .sh)

########## ARGUMENTS ##########

if [[ $1 == "" ]] || [[ $1 == "help" ]] || [[ $2 == "" ]] || [[ $3 == "" ]]
then
  echo -e "\e[32mMigration love to you.\e[0m"
  echo "usage: ${THE_EXE} domain data-folder sql-dump"
  echo "example: ${THE_EXE}.sh miaou.girofle.cloud Backup2020-12/ backup2020-12.sql"
  exit 1
else
  DOMAIN="${1}"
  SOURCE_DIR="${2}"
  SOURCE_SQL="${3}"
fi

# check if SQL file exists
if [[ ! -d "$SOURCE_DIR" ]]
then
  echo "Source data directory ${SOURCE_DIR} does not exists."
  exit 1
fi

# check if SQL file exists
if [[ ! -f "$SOURCE_SQL" ]]
then
  echo "SQL file ${SOURCE_SQL} does not exists."
  exit 1
fi

########## CONSTANTS ##########

BASE_DIR="/var/www/"
INSTALL_DIR="${BASE_DIR}${DOMAIN}/"
DATABASE_USER=${DOMAIN//[\.\_]/_}  # use domain and replace '.' or '-' by '_'
DATABASE_NAME="nc_"${DATABASE_USER}

NGINX_TEMPLATE_PATH=${DIR_EXE}/conf/nginx.conf.tmpl
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${DOMAIN}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${DOMAIN}.conf

########## TLS ##########

# setup certbot
echo -e "\e[32mSetting up TLS certificate ...\e[0m"
certbot certonly --noninteractive --nginx --email contact@girofle.cloud --agree-tos --expand -d ${DOMAIN}

########## NGINX ##########

if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
then
  echo -e "\e[32mCreating nginx configuration file at ${NGINX_CONF_FILE_PATH} and link to it ${NGINX_CONF_LINK_PATH} ...\e[0m"
    cp ${NGINX_TEMPLATE_PATH} ${NGINX_CONF_FILE_PATH}
    sed -i 's/INSTANCE_HOST_NAME/'${DOMAIN}'/g' ${NGINX_CONF_FILE_PATH}
    ln -s ${NGINX_CONF_FILE_PATH} ${NGINX_CONF_LINK_PATH}
  echo "Nginx is reloading ..."
  systemctl reload nginx
else
  echo -e "\e[32mNginx configuration file already exists. Skipping nginx configuration.\e[0m"
fi


########## FILES ##########

if [ "${SOURCE_DIR}" != "${INSTALL_DIR}" ]
then
  echo -e "\e[32mCopying files to ${INSTALL_DIR}\e[0m"
  cp -r ${SOURCE_DIR} ${INSTALL_DIR}
else
  echo -e "\e[32mNextcloud data files are in the right folder. Skipping file copying.\e[0m"
fi
chown -R www-data:www-data ${INSTALL_DIR}

########## DATABASE #############

# Get database password
if [[ ! -f ${DATABASE_PASSWORD} ]]
then
  read -sp "Database password not found in envs, please prodive a password: " DATABASE_PASSWORD
  echo ""
fi

# Create database
echo -e "\e[32mCreating database $DATABASE_NAME ...\e[0m"
mysql -uroot --execute="CREATE DATABASE ${DATABASE_NAME};"
mysql -uroot --execute="CREATE USER ${DATABASE_USER}@'localhost' IDENTIFIED BY '${DATABASE_PASSWORD}'; GRANT ALL ON ${DATABASE_NAME}.* TO ${DATABASE_USER}@'localhost'";
mysql -uroot --execute="FLUSH PRIVILEGES";

mysql -u root ${DATABASE_NAME} < ${SOURCE_SQL}

echo "ALL DONE, please edit ${INSTALL_DIR}/config/config.php to adjust to the new instance"
echo -e "\e[32mYou are the Migration MASTAAAR.\e[0m"
