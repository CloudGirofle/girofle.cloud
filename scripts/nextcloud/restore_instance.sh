#!/usr/bin/bash
# This script must be ran with admin rights
# Usage: sudo ./restore_instance.sh yolo.girofle.cloud
# By Cloud Girofle, GPLv3+, Aug. 2023
# Script pour restaurer une instance à partir du script archive_instance.sh


# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR


########## ARGS  ##########

if [[ $1 == "" ]] # make sur we have an argument provided
then
  echo "Please enter the location of the .tar.bz2 file (ex: \`${THE_EXE}.sh yolo.girofle.cloud.backup.tar.bz2\`)"
  exit 1
fi

if [[ $1 != *.tar.bz2 ]] # * is used for pattern matching
then
    echo "Please provide the path to a *.tar.bz2 file created with archive_instance.sh"
    exit 1
fi

if [[ ! -f $1 ]]
then
    echo "File ${1} does not exit."
    exit 1
fi

########## CONSTANTS ##########
ARCHIVE_PATH=${1}
BASE_DIR="/var/www/"
TMP_DIR="/tmp/${ARCHIVE_PATH}"
NC_CONFIG="config/config.php"
NC_FILES="www/"
NC_DATA="data/"
NC_DATA_EXTRA="data/"
NC_SQL=nextcloud.sql

########## TEST IF PROGRAMS INSTALLED (from apt)
ok=true
nginx -v 2> /dev/null || ok=false;echo "[NOT FOUND] Nginx"
mysql -V > /dev/null || ok=false;echo "[NOT FOUND] Mysql"
php -v > /dev/null || ok=false;echo "[NOT FOUND] PHP"
bzip2 -V > /dev/null || ok=false;echo "[NOT FOUND] bzip2"

if [[ $ok != true ]]
then
    echo "Please install the required dependencies before we can restore"
    exit 1
fi

########## DECOMPRESS ##########
echo -e "\e[32mExtracting... (please be patient)\e[0m"

if [[ -d ${TMP_DIR} ]]
then
    echo "The temporary folder ${TMP_DIR} exists, cannot extract archive, aborting"
    exit 1
else
    mkdir -p ${TMP_DIR}
fi

tar --checkpoint=.1000 -xjf ${ARCHIVE_PATH} -C ${TMP_DIR}

echo "Content of the archive:"
tree -d -L 2 ${TMP_DIR}
echo ""

######### Extract variables & check integrity #######
echo -e "\e[32mChecking data integrity\e[0m"

# nc files
echo -e "\e[32m-- ${TMP_DIR}/${NC_FILES}\e[0m"
if [[ ! -d ${TMP_DIR}/${NC_FILES} ]]
then
    echo "ERROR: directory ${NC_FILES} not found in extracted archive"
    exit 1
fi

# domain
DOMAIN="$(ls -d ${TMP_DIR}/${NC_FILES}/*/ | head -n 1)"
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${DOMAIN}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${DOMAIN}.conf

echo "-- Restoring domain ${DOMAIN}"

# nginx files
echo -e "\e[32m-- ${TMP_DIR}/${DOMAIN}.conf\e[0m"
if [[ ! -f ${TMP_DIR}/${DOMAIN}.conf ]]
   echo "ERROR: nginx config file not found"
   exit 1
fi


# config file
echo -e "\e[32m-- ${TMP_DIR}/${DOMAIN}/${NC_CONFIG}\e[0m"
if [[ ! -f ${TMP_DIR}/${DOMAIN}/${NC_CONFIG} ]]
then
    echo "ERROR: file ${DOMAIN}/${NC_CONFIG} not found in extracted archive"
    exit 1
fi

# data files
echo -e "\e[32m-- ${TMP_DIR}/${NC_FILES}${DOMAIN}/${NC_DATA}\e[0m"
if [[ ! -d ${TMP_DIR}/${NC_FILES}${DOMAIN}/${NC_DATA} ]] # default NC dir
then
    echo "WARNING: directory ${NC_FILES}${DOMAIN}/${NC_DATA} not found in extracted archive"
elif [[ ! -d ${TMP_DIR}/${NC_DATA_EXT} ]]
then
    echo "ERROR: nor directory ${NC_DATA_EXT}, aborting"
    exit 1
fi

######### Check that we can restore #######
echo -e "\e[32mChecking data integrity\e[0m"

# files dir
if [[ -d ${BASE_DIR}/${DOMAIN} ]]
then
    echo "Folder ${BASE_DIR}/${DOMAIN} exists, aborting"
    exit 1
fi

# database info
config="${TMP_DIR}/${DOMAIN}/${NC_CONFIG}"
DATABASE_NAME=$(echo "$(cat $config);print(\$CONFIG['dbname']);" | php)
DATABASE_USER=$(echo "$(cat $config);print(\$CONFIG['dbuser']);" | php)
DATABASE_PASSWORD=$(echo "$(cat $config);print(\$CONFIG['dbpassword']);" | php)


# mysql
# source https://stackoverflow.com/a/28112553
# source https://stackoverflow.com/a/7364807
if [ -d /var/lib/mysql/${DATABASE_NAME} ] ; then 
    echo "Database ${DATABASE_NAME} already exists, aborting"
    exit 1
fi
RES="$(mysql -uroot -sse "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = '${DATABASE_USER}')")"

if [ "$RES" = 1 ]; then
    echo "Database user ${DATABASE_USER} exists, aborting"
    exit 1
fi


######### Restore #######
${DIR_EXE_ABS}/migrate_instance.sh ${DOMAIN} ${TMP_DIR}/${NC_FILES}/${DOMAIN} ${TMP_DIR}/${NC_SQL}

# echo -e "\e[32mSetting up TLS certificate ...\e[0m"
# certbot certonly --noninteractive --nginx --email contact@girofle.cloud --agree-tos --expand -d ${DOMAIN}

# if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
# then 
#   echo -e "\e[32mCreating nginx configuration file at ${NGINX_CONF_FILE_PATH} and link to it ${NGINX_CONF_LINK_PATH} ...\e[0m"
#     cp ${TMP_DIR}/${DOMAIN}.conf ${NGINX_CONF_FILE_PATH}
#     #sed -i 's/INSTANCE_HOST_NAME/'${DOMAIN}'/g' ${NGINX_CONF_FILE_PATH}
#     ln -s ${NGINX_CONF_FILE_PATH} ${NGINX_CONF_LINK_PATH}
#   echo "Nginx is reloading ..."
#   systemctl reload nginx
# else
#   echo -e "\e[32mNginx configuration file already exists. Skipping nginx configuration.\e[0m"
# fi

# # Create database
# echo -e "\e[32mCreating database $DATABASE_NAME ...\e[0m"
# mysql -uroot --execute="CREATE DATABASE ${DATABASE_NAME};"
# mysql -uroot --execute="CREATE USER ${DATABASE_USER}@'localhost' IDENTIFIED BY '${DATABASE_PASSWORD}'; GRANT ALL ON ${DATABASE_NAME}.* TO ${DATABASE_USER}@'localhost'";
# mysql -uroot --execute="FLUSH PRIVILEGES";
#mysqldump ${DATABASE_NAME} < ${NC_SQL}

# # Restore NC files
# cp -r ${TMP_DIR}/${NC_FILES}/${DOMAIN} ${BASE_DIR}

# # Restore data file
# echo "data file at custom location not implemented, please cp by yourself"

