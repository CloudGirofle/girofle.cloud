#!/usr/bin/python3
# By Cloud Girofle, Jul 2023, GPLv3+

#List Nextcloud instances
#Must be ran as the occ user
#ex: sudo -u www-data python3 nextcloud.py

import os, re, pathlib, datetime
from subprocess import run
from glob import glob

BASE_DIR = "/var/www/"
OO_FORMATS = "csv,doc,docm,docx,docxf,oform,dotx,epub,html,odp,ods,odt,otp,ots,ott,pdf,potm,potx,ppsm,ppsx,ppt,pptm,pptx,rtf,txt,xls,xlsm,xlsx,xltm,xltx".split(",")
OO_FORMATS_EDITABLE = "docxf,docx,oform,xls,xlsx,odt,ods".split(",") # incomplete

def get_instances():
    instances = []
    folders = [entry.path for entry in os.scandir(BASE_DIR) if entry.is_dir()]
    for folder in folders:
        if os.path.exists(folder + "/occ"):
            name = folder.split("/")[-1]
            instances.append({'name': name, 'path': folder})
    return instances

def get_data_location(i):
    def _stripspace(s):
        return s.replace(' ','').replace('\t','')
    with open(os.path.join(i['path'], 'config/config.php'), 'r') as f: # assume config lies only in config.php
        cfg = f.readlines()
    data = [i for i in cfg if "'datadirectory'=>" in _stripspace(i)][0]
    data = data.split('=>')[1].split("'")[1]
    assert os.path.isdir(data)
    return data

def list_modified_files(data, before=None, after=None, extensions=[]):
    def _list_toregexp(l):
        """Build a regexp to match the extensions"""
        l = [".*"+i.replace('.', '\.')+"$" for i in l]
        p = "|".join(l)
        return p
    # List all files matching extension
    allfiles = glob(data+'/**', recursive=True)
    if len(extensions) != 0:
        r = re.compile(_list_toregexp(extensions))
        allfiles = list(filter(r.match, allfiles))

    # Get modification date
    modfiles=allfiles
    moddates=[None]*len(modfiles)
    if (before is not None) or (after is not None):
        if before is None:
           before=datetime.datetime.now()
        if after is None:
           after=datetime.datetime.fromtimestamp(0)
        assert after<before
        modfiles = []
        for f in allfiles:
            pf = pathlib.Path(f)
            mod_date = pf.stat().st_mtime
            mod_date = datetime.datetime.fromtimestamp(mod_date) #, tz=datetime.timezone.utc)
            if after <= mod_date <= before:
               modfiles.append(f)
               moddates.append(mod_date)
               print(f, mod_date)

if __name__ == "__main__":
    list_instances = get_instances()
    # Times are UTC
    before=datetime.datetime.strptime("2023-07-20 07:00:00", "%Y-%m-%d %H:%M:%S") # YYYY-MM-dd HH:mm:SS
    after= datetime.datetime.strptime("2023-07-19 20:00:00", "%Y-%m-%d %H:%M:%S")
    l = len(list_instances)
    for (i,instance) in enumerate(list_instances):
        print(f"# {i+1}/{l}: {instance['name']}")
        datapath = get_data_location(instance)
        list_modified_files(datapath, extensions=OO_FORMATS_EDITABLE, before=before, after=after)
