#!/bin/bash
# Usage: sudo ./nextcloud_setup_wizard.sh <domain> <tag>
# Example sudo ./nextcloud_setup_wizard.sh nuage.girofle.cloud v25.1

# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR


DOMAIN=$1
NEXTCLOUD_TAG=$2

# check input arguments
[ -z "$1" ] && echo "Please provide a domain as first argument."
[ -z "$2" ] && echo "Please provide a git tag as second argument (ex v25.1)"

cd /var/www/${DOMAIN}
echo -e "\e[32mSetup First Run Wizard for ${DOMAIN}."

sudo -u www-data php occ app:disable firstrunwizard
sudo mv apps/firstrunwizard/ apps/firstrunwizard.bak # should check if a backup already exists
cd apps;sudo -u www-data git clone -b ${NEXTCLOUD_TAG} --single-branch --depth 1 https://framagit.org/CloudGirofle/firstrunwizard-girofle.git

#cd apps;sudo -u www-data git clone https://framagit.org/CloudGirofle/firstrunwizard-girofle.git
sudo mv firstrunwizard-girofle/ firstrunwizard
cd ..
sudo -u www-data php occ app:enable firstrunwizard
