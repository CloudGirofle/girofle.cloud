#!/usr/bin/env bash
# This script downloads the Nextcloud app and category list from the official Nextcloud App store, and removes some applications that we don't like.
# -- This script is meant to be ran periodically.
# -- The output file are usually served by a web server.
# By Cloud Girofle, GPLv3+, Fev. 2025

# abort on error
set -e
function error_on_exit() {
    echo -e "\e[31mSomethong went wrong, exiting\e[0m"
    exit 1
}
trap error_on_exit ERR

THE_EXE=$(basename "$0" .sh)
if [[ $1 == "" ]]
then
    echo "Please enter the path of the local app store (ex: ${THE_EXE}.sh /var/www/mutu3.girofle.cloud/nextcloud)."
    exit 1
fi
LOCAL_APP_STORE_PATH=$1

echo -e "\e[32mDownloading files into $LOCAL_APP_STORE_PATH\e[0m"
curl -s https://apps.nextcloud.com/api/v1/categories.json > "$LOCAL_APP_STORE_PATH/categories.json"
curl -s https://apps.nextcloud.com/api/v1/apps.json | jq 'walk(if type == "object" and (.id == "richdocuments" or .id == "richdocumentscode" or .id == "richdocumentscode_arm64" or .id =="documentserver_community") then del(.) else . end)' > "$LOCAL_APP_STORE_PATH/apps.json" 2>&1
echo -e "\e[32mAll Good.\e[0m"
