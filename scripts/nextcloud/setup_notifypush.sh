#!/bin/bash
# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR

# Variables
THE_EXE=$(basename "$0" .sh)

if [[ $1 == "" ]]
then
  echo "Please enter the host name of the new instance (ex: \`${THE_EXE}.sh mutu.girofle.cloud\`)"
  exit 1
fi

## Run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Get Hostname
HOSTNAME="$(hostname)"
if [[ $HOSTNAME == "" ]]
then echo "Please setup hostname"
  exit
fi

DOMAIN="${1}"

BASE_DIR="/var/www/"
NC_DIR="${BASE_DIR}${DOMAIN}/"

NGINX_CONF_FILE_PATH="/etc/nginx/sites-available/${DOMAIN}.conf"

SYSTEMD_SERVICENAME="notifypush@${DOMAIN}"

SOCKET_SUBDIR="notify_push"
SOCKET_DIR="/run/${SOCKET_SUBDIR}/"

# ==== Check socket ====
if [[ ! -d ${SOCKET_DIR} ]] # existence
then
    echo "Creating Socket folder ${SOCKET_DIR}"
    mkdir ${SOCKET_DIR}
else
    echo "CHECK: Socket folder exists at ${SOCKET_DIR}"
fi

if [[ $(stat -c '%U' ${SOCKET_DIR}) == "www-data" ]] # permissions
then
    echo "CHECK: Socket dir has the right owner"
else
    echo "Socket ${SOCKET_DIR} is not owned by www-data, changing this"
    chown -R www-data ${SOCKET_DIR}
fi

# ===== Setup & activate Daemon
systemctl start "${SYSTEMD_SERVICENAME}"
systemctl enable "${SYSTEMD_SERVICENAME}"

# ==== Setup nginx ====
# test if the location ^~ /push/ directive is present
returncode=0;
returncode2=0;
grep -v '^ *#' "${NGINX_CONF_FILE_PATH}" | grep -q "location ^~ /push/" || returncode=1;
grep -q "#NOTIFY_PUSH_BLOCK" "${NGINX_CONF_FILE_PATH}" || returncode2=1;
if [ ${returncode} -eq 0 ]
then
    echo "Route /push/ found"
    PROXY_PASS_COMMAND=$(grep -v '^ *#' "${NGINX_CONF_FILE_PATH}" | grep -A 1 "location ^~ /push/" | tail -n1)
    echo "$PROXY_PASS_COMMAND"
    PROXY_PASS_TARGET="http://unix:${SOCKET_DIR}${DOMAIN}.sock:/"
    if [[ $PROXY_PASS_COMMAND == *${PROXY_PASS_TARGET}* ]]
    then
	echo "Proper proxy line found: $PROXY_PASS_COMMAND"
    else
	echo "NO PROPER PROXY PASS COMMAND FOUND, expected: $PROXY_PASS_TARGET"
	echo "ABORTING"
	exit 1
    fi
else
    if [ ${returncode2} -eq 0 ]
    then
	    echo "#NOTIFY_PUSH_BLOCK found, activating"
	    sed -i "s/\#NOTIFY_PUSH_BLOCK//g" "${NGINX_CONF_FILE_PATH}"
    else
	    echo "#NOTIFY_PUSH_BLOCK not found, aborting"
	    exit 1
    fi
fi
service nginx reload

# ==== Nextcloud ====

cd "${NC_DIR}"
returncode=0

# Select IP addresses for trusted proxies
if [[ $HOSTNAME == "mutu3" ]]
then
  IPV4="37.187.9.30"
  IPV6="2001:41d0:a:91e::1"
else
  IPV4="94.23.29.115"
  IPV6="2001:41d0:2:1e73::1"
fi

sudo -u www-data php occ config:system:set trusted_proxies 0 --value="$IPV4"
sudo -u www-data php occ config:system:set trusted_proxies 1 --value="$IPV6"
sudo -u www-data php occ app:enable notify_push
sudo -u www-data php occ notify_push:setup "https://${DOMAIN}/push"
sudo -u www-data php occ notify_push:self-test || returncode=1

# ====

if [ ${returncode} -eq 0 ]
then
    echo "CHECK: Nextcloud is properly configured to work with notify_push"
else
    echo "Nextcloud is NOT properly configured to work with notify_push"
fi
