#!/usr/bin/bash
# This script must be ran with admin rights
# Usage: sudo ./create_instance.sh yolo.girofle.cloud

# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR
########## ARGS  ##########

DIR_EXE=$(dirname $0)
DIR_EXE_ABS=$(realpath $DIR_EXE)
THE_EXE=$(basename $0 .sh)

if [[ $1 == "" ]]
then
  echo "Please enter the host name of the new instance (ex: \`${THE_EXE}.sh mutu.girofle.cloud\`)"
  exit 1
fi

########## CONSTANTS ##########

BASE_DIR="/var/www/"
FQDN=$(hostname -f)
DOMAIN="${1}"
INSTALL_DIR="${BASE_DIR}${DOMAIN}/"
tmp_n=${DOMAIN//\./_} # use domain and replace . by _
DATABASE_USER=${tmp_n//\-/_}  # use domain and replace - by _
DATABASE_NAME="nc_"${DATABASE_USER}
NEXTCLOUD_VERSION="29.0.12"
NEXTCLOUD_ARCHIVE_NAME="nextcloud-${NEXTCLOUD_VERSION}.tar.bz2"
NEXTCLOUD_ARCHIVE_LOCAL_PATH="${BASE_DIR}${NEXTCLOUD_ARCHIVE_NAME}"
NEXTCLOUD_WIZARDBRANCH="v28"
NGINX_TEMPLATE_PATH=${DIR_EXE}/conf/nginx.conf.tmpl
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${DOMAIN}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${DOMAIN}.conf
ONLYOFFICE_CONFIG_PATH=${DIR_EXE_ABS}/conf/onlyoffice8.json
CUSTOM_DOC_DIR=${BASE_DIR}nextcloud_default_documentation/
#PHPOCCPARAMS="--define apc.enable_cli=1"
########## TLS ##########

# setup certbot
echo -e "\e[32mSetting up TLS certificate ...\e[0m"
certbot certonly --noninteractive --nginx --email contact@girofle.cloud --agree-tos --expand -d ${DOMAIN}

########## NGINX ##########

if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
then 
  echo -e "\e[32mCreating nginx configuration file at ${NGINX_CONF_FILE_PATH} and link to it ${NGINX_CONF_LINK_PATH} ...\e[0m"
    cp ${NGINX_TEMPLATE_PATH} ${NGINX_CONF_FILE_PATH}
    sed -i 's/INSTANCE_HOST_NAME/'${DOMAIN}'/g' ${NGINX_CONF_FILE_PATH}
    ln -s ${NGINX_CONF_FILE_PATH} ${NGINX_CONF_LINK_PATH}
  echo "Nginx is reloading ..."
  systemctl reload nginx
else
  echo -e "\e[32mNginx configuration file already exists. Skipping nginx configuration.\e[0m"
fi

########## INPUTS ##########

# Get ONLYOFFICE JWT 
if [[ ! -f ${ONLYOFFICE_JWT} ]]
then
  read -sp "Onlyoffice secret token (ONLYOFICE_JWT) not found in envs, please prodive a JWT: " ONLYOFFICE_JWT
  echo ""
fi

# Get database password
if [[ ! -f ${DATABASE_PASSWORD} ]]
then
  read -sp "Database password not found in envs, please prodive a password: " DATABASE_PASSWORD
  echo ""
fi

# Get Nextcloud admin password
if [[ ! -f ${ADMIN_PASSWORD} ]]
then
  read -sp "Nextcloud admin password not found in envs, please prodive a password:" ADMIN_PASSWORD
  echo ""
fi

# Get user info
read -p "Other admin login: " USER_LOGIN
IFS= read -p "Other admin full name: " USER_NAME
read -p "Other admin email address: " USER_EMAIL

########## DATABASE ##########

# Create database
echo -e "\e[32mCreating database $DATABASE_NAME ...\e[0m"
mysql -uroot --execute="CREATE DATABASE ${DATABASE_NAME};"
mysql -uroot --execute="CREATE USER ${DATABASE_USER}@'localhost' IDENTIFIED BY '${DATABASE_PASSWORD}'; GRANT ALL ON ${DATABASE_NAME}.* TO ${DATABASE_USER}@'localhost'";
mysql -uroot --execute="FLUSH PRIVILEGES";

########## NEXTCLOUD ##########

# Download Nextcloud file
if [[ ! -f ${NEXTCLOUD_ARCHIVE_LOCAL_PATH} ]]
then
  echo -e "\e[32mDownloading Nextcloud ${NEXTCLOUD_VERSION} ...\e[0m"
  wget -q -O ${NEXTCLOUD_ARCHIVE_LOCAL_PATH} https://download.nextcloud.com/server/releases/${NEXTCLOUD_ARCHIVE_NAME}
else
  echo -e "\e[32mNextcloud archive already downloaded. Reusing ${NEXTCLOUD_ARCHIVE_LOCAL_PATH} ...\e[0m"
fi

# Configure Nextcloud files
if [[ ! -f ${INSTALL_DIR}/index.php ]]
then
  echo -e "\e[32mCreating new Nextcloud ${NEXTCLOUD_VERSION} instance at ${INSTALL_DIR} ...\e[0m"
  mkdir -p ${INSTALL_DIR}
  tar -xf ${NEXTCLOUD_ARCHIVE_LOCAL_PATH} -C ${INSTALL_DIR} --strip-components=1
  chown -R www-data:www-data ${INSTALL_DIR}
else
  echo -e "\e[32mNextcloud files already exist in ${INSTALL_DIR}. Skipping nextcloud file configurations.\e[0m"
fi

# Run Nextcloud installation
echo -e "\e[32mStarting Nextcloud installation ...\e[0m"
cd "${INSTALL_DIR}"
sudo -u www-data php occ maintenance:install \
  --database "mysql" --database-name $DATABASE_NAME \
  --database-user ${DATABASE_USER} --database-pass "$DATABASE_PASSWORD" \
  --admin-user "admin" --admin-pass "$ADMIN_PASSWORD"
sudo -u www-data php occ config:system:set trusted_domains 1 --value="${DOMAIN}"
sudo -u www-data php occ config:system:set --value='\OC\Memcache\APCu' memcache.local
sudo -u www-data php occ config:system:set --value='fr' default_language
sudo -u www-data php occ config:system:set --value="https://${DOMAIN}" overwrite.cli.url
sudo -u www-data php occ config:system:set --value="${DOMAIN}" mail_domain
sudo -u www-data php occ config:system:set --value='fr' default_phone_region # NC21+?
sudo -u www-data php occ config:system:set --value='fr' default_locale
sudo -u www-data php occ config:system:set --value='true' upgrade.disable-web
sudo -u www-data php occ config:system:set --value='256' preview_max_memory
sudo -u www-data php occ config:system:set maintenance_window_start --type=integer --value=1
sudo -u www-data php occ config:system:set trashbin_retention_obligation --value='auto, 90'
sudo -u www-data php occ config:system:set versions_retention_obligation --value='auto, 180'
sudo -u www-data php occ config:system:set updater.release.channel --value='stable'
sudo -u www-data php occ config:system:set appstoreenabled --value='true'
sudo -u www-data php occ config:system:set appstoreurl --value="https://${FQDN}/nextcloud"
sudo -u www-data php occ config:system:set knowledgebase.embedded --value="true"

echo -e "\e[32mNew nextcloud instance ${DOMAIN} has been created successfully.\e[0m"

## Configure Redis
echo -e "\e[32mConfiguring Redis and file locking."
sudo -u www-data php occ config:system:set memcache.locking --value='\OC\Memcache\Redis'
sudo -u www-data php occ config:system:set filelocking.enabled --value='true' --type=boolean
sudo -u www-data php occ config:system:set redis host --value='localhost'
sudo -u www-data php occ config:system:set redis port --value=6379
sudo -u www-data php occ config:system:set redis dbindex --value=0
sudo -u www-data php occ config:system:set redis timeout --value=1.5
sudo -u www-data php occ config:system:set redis read_timeout --value=1.5

## Configure notify_push
cd ${DIR_EXE_ABS}
./setup_notifypush.sh ${DOMAIN} || ./setup_notifypush.sh ${DOMAIN} # dirty hack, needs to be run twice...

#### Onlyoffice connector
cd ${INSTALL_DIR}
echo -e "\e[32mInstalling & configuring OnlyOffice."
sudo -u www-data php occ app:install onlyoffice
sudo -u www-data php occ config:import "$ONLYOFFICE_CONFIG_PATH"
sudo -u www-data php occ config:system:set onlyoffice jwt_secret --value="${ONLYOFFICE_JWT}"
sudo -u www-data php occ onlyoffice:documentserver

#### Install other apps
echo -e "\e[32mInstalling & configuring other apps.\e[0m"
sudo -u www-data php occ app:install calendar

# commenté car pb de CSP pour l'instant
#cd apps; sudo -u www-data git clone https://gitlab.com/nextcloud-other/nextcloud-annotate pdfannotate; cd .. # maybe we should cache it
#sudo -u www-data php occ app:enable pdfannotate

#### Accessibility & appearance
echo -e "\e[32mDisable Dashboard & default files (temporary).\e[0m"
sudo -u www-data php occ app:disable dashboard
sudo -u www-data php occ config:system:set --value='/var/www/nextcloud_default_files/default/' skeletondirectory

echo -e "\e[32mSetup First Run Wizard.\e[0m"
cd ${DIR_EXE_ABS}
./nextcloud_setup_wizard.sh ${DOMAIN} ${NEXTCLOUD_WIZARDBRANCH}

# Here we should also configure our custom documentation

if [[ -d ${CUSTOM_DOC_DIR} ]]
then
    cd ${INSTALL_DIR}
    echo -e "\e[32mSetup custom documentation\e[0m"
    sudo -u www-data mv ./core/doc ./core/doc.origin.bak
    sudo -u www-data mkdir ./core/doc
    sudo -u www-data ln -s ${CUSTOM_DOC_DIR}/user/ ${INSTALL_DIR}/core/doc/user
    sudo -u www-data ln -s ${CUSTOM_DOC_DIR}/admin/ ${INSTALL_DIR}/core/doc/admin
else
    echo -e "\e[20mNo custom documentation found in ${CUSTOM_DOC_DIR}, not setting up.\e[0m"
fi

#### Create admin user
echo -e "\e[32mCreating other account with admin rights.\e[0m"
curl -X POST -H "OCS-APIRequest:true" "https://admin:${ADMIN_PASSWORD}@${DOMAIN}/ocs/v1.php/cloud/users" \
     -d userid="${USER_LOGIN}" \
     -d displayName="${USER_NAME}" \
     -d email="${USER_EMAIL}" \
     -d groups[]="admin"

### Bravo
echo "\e[32mBravo ! Your new Nextcloud is now available at https://${DOMAIN} \e[0m"

#### TODO
echo -e "\e[32mWhat you need to do now. IMPORTANT.\e[0m"
echo "- Configure Onlyoffice to disable PDF edition https://${DOMAIN}/settings/admin/onlyoffice"
echo "- Create a monitoring key and add it to momo"
