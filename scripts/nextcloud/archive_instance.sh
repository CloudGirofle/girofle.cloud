#!/usr/bin/bash
## archive_instance.sh
# A script to archive an instance on mutu
# By MW, GPLv3+, Oct. 2020
# usage: ./archive_instance.sh <name of instance> {backup|archive}

## Run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# abort on error
set -e
function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR


DIR_EXE=$(dirname $0)
THE_EXE=$(basename $0 .sh)

if [[ $1 == "" ]]
then
    echo "Please enter the host name of the an existing instance (ex: \`${THE_EXE}.sh mutu.girofle.cloud backup\`).  Instances (possibly) available:"
    ls -d /var/www/*/
    exit 1
fi

if [[ $2 == "" ]]
then
    echo "Please enter whether we should {backup,archive} the instance (ex: \`${THE_EXE}.sh mutu.girofle.cloud backup\`)."
  exit 1
fi

########## CONSTANTS ##########
BASE_DIR="/var/www/"
DOMAIN="${1}"
BACKUPMODE="${2}"
INSTALL_DIR="${BASE_DIR}${DOMAIN}/"
DATABASE_USER=${DOMAIN//\./_}  # use domain and replace . by _
DATABASE_NAME="nc_"${DATABASE_USER}
DATA_DIRECTORY="$(sudo -u www-data php ${INSTALL_DIR}/occ config:system:get datadirectory)"

NGINX_TEMPLATE_PATH=${DIR_EXE}/conf/nginx.conf.tmpl
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${DOMAIN}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${DOMAIN}.conf

BACKUP_DIR="/home/girofle/backups/"
BACKUP_FILE="${DOMAIN}-`date +"%Y%m%d"`.tar.bz2"

########## LIST TO REMOVE ##########
if [[ ${BACKUPMODE} == "backup" ]]
then
   echo -e "\e[32mThe script is running in ${BACKUPMODE} mode, no data will be lost\e[0m"
fi

if [[ ${BACKUPMODE} == "archive" ]]
then
   echo -e "\e[32mThe script is running in ${BACKUPMODE} mode, the instance will be totally purged after the archive\e[0m"
   echo -e "\e[32mNote: to simply make the instance unavailable you can \`unlink ${NGINX_CONF_LINK_PATH}\`\e[0m"
fi

echo -e "\e[32mThe following will be processed ...\e[0m"
echo "-- nginx configuration file: ${NGINX_CONF_LINK_PATH}"
echo "-- nginx configuration file: ${NGINX_CONF_FILE_PATH}"
echo "-- nextcloud directory: ${INSTALL_DIR}"
echo "-- nextcloud data directory: ${DATA_DIRECTORY}"
echo "-- mariaDB table: ${DATABASE_NAME}"
echo "-- mariaDB user: ${DATABASE_USER}"
echo ""
echo -e "\e[32mThe backup will be saved in ${BACKUP_DIR}${BACKUP_FILE}\e[0m"
read -p  $'\e[32mAre you sure you want to proceed? (Y/N): \e[0m' CONT
if [ "$CONT" != "Y" ]; then
    echo "Exiting";
    exit 0
fi

########## SANITY CHECKS ############
if [[ -f ${BACKUP_DIR}${BACKUP_FILE} ]]
then
    echo "Backup file exists: ${BACKUP_DIR}${BACKUP_FILE}, ABORTING."
    exit 1;
fi

if [[ -d ${BACKUP_DIR}${BACKUP_FILE}.tmp ]]
then
    echo "Backup temporary folder exists: ${BACKUP_DIR}${BACKUP_FILE}.tmp, ABORTING."
    exit 1;
else
    mkdir -p ${BACKUP_DIR}${BACKUP_FILE}.tmp
fi

if [[ ! -d ${INSTALL_DIR} ]]
then
    echo "[ERROR] nextcloud directory not found: ${INSTALL_DIR}"
    exit 1
fi

if [[ ! -d ${DATA_DIRECTORY} ]]
then
    echo "[ERROR] nextcloud data directory not found: ${DATA_DIRECTORY}"
    exit 1
fi

########## BACKUP/DELETE DATA & CO ##########
########## MYSQL STUFF
echo -e "\e[32mBackuping database\e[0m"
mysqldump -u root ${DATABASE_NAME} > ${BACKUP_DIR}${BACKUP_FILE}.tmp/nextcloud.sql

########## NGINX ##########
if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
then
    echo -e "\e[32mNginx configuration does not exist. Skipping nginx removal.\e[0m"
else
    echo -e "\e[32mNginx configuration...\e[0m"
    cp ${NGINX_CONF_LINK_PATH} ${BACKUP_DIR}${BACKUP_FILE}.tmp/
fi

########## FILES ##########
echo -e "\e[32mBackup files from ${INSTALL_DIR}...\e[0m"
mkdir ${BACKUP_DIR}${BACKUP_FILE}.tmp/www/
cp -r ${INSTALL_DIR} ${BACKUP_DIR}${BACKUP_FILE}.tmp/www/

if find "${INSTALL_DIR}" -samefile "${DATA_DIRECTORY}" -printf 'Y\n' -quit | grep -qF Y; then
    echo -e "\e[32m${DATA_DIRECTORY} is inside ${INSTALL_DIR}\e[0m"
else
    mkdir ${BACKUP_DIR}${BACKUP_FILE}.tmp/data/
    cp -r ${DATA_DIRECTORY} ${BACKUP_DIR}${BACKUP_FILE}.tmp/data/
fi

########### COPY migrate/backup SCRIPT #######

##### Compress (& show progress)
FROMSIZE=`du -sk --apparent-size ${BACKUP_DIR}${BACKUP_FILE}.tmp | cut -f 1`;
CHECKPOINT=`echo ${FROMSIZE}/50 | bc`;
echo "Estimated: [==================================================]";
echo -n "Progess:   [";
tar -c --record-size=1K --checkpoint="${CHECKPOINT}" --checkpoint-action="ttyout=>" -f - -C ${BACKUP_DIR}${BACKUP_FILE}.tmp "${BACKUP_DIR}${BACKUP_FILE}.tmp" | bzip2 > "${BACKUP_DIR}${BACKUP_FILE}";
echo "]"

echo "ALL backup DONE, the backup file was saved to: ${BACKUP_DIR}${BACKUP_FILE}"

if [[ ${BACKUPMODE} == "backup" ]]
then
    exit 0;
fi

#### Asking for confirmation to delete everything
read -p  $'\e[32mThe backup completed with success. Are you sure you want to proceed with deleting the existing instance? (Y/N): \e[0m' CONT
if [ "$CONT" != "Y" ]; then
    echo "Exiting";
    exit 0
fi

echo -e "\e[32mDeleting database\e[0m"
mysql -uroot --execute="DROP DATABASE ${DATABASE_NAME};"
mysql -uroot --execute="DROP USER ${DATABASE_USER}@localhost;"

echo -e "\e[32mDeleting TLS certificate ...\e[0m"
certbot delete --cert-name ${DOMAIN}

echo -e "\e[32mNginx configuration...\e[0m"
unlink ${NGINX_CONF_LINK_PATH}
rm ${NGINX_CONF_FILE_PATH}
echo "Nginx is reloading ..."
systemctl reload nginx

echo -e "\e[32mRemove files\e[0m"
rm -rf ${DATA_DIRECTORY}
rm -rf ${INSTALL_DIR}

echo -e "\e[32mALL DONE! The instance ${DOMAIN} has been deleted \e[0m"
