#!/usr/bin/env bash
LOCK_FILE="/var/run/sendEventReminder.lock"
LOCK_FILE_TTL=300
function lock_me() {
	if [ -s "$LOCK_FILE" ]; then
		local age=$(( EPOCHSECONDS - $(stat -c %Y "$LOCK_FILE") ))
		if [ $age -gt $LOCK_FILE_TTL ] ; then
			echo "Lock file too old (>${LOCK_FILE_TTL}s). Forcing"
			rm -f "$LOCK_FILE"
		else
			return 1
		fi
	fi
	echo $$ > "$LOCK_FILE"
}
function unlock_me() {
	rm -f "$LOCK_FILE"
}
# ====================
lock_me || exit 1
while read occ ; do
	printf "=== %s\n" $(dirname "$occ")
	if [ -z "$(sudo -u www-data php -f "$occ" config:app:get dav sendEventReminders)" ] ; then	# sendEventReminder est à yes
		if [ -z "$(sudo -u www-data php -f "$occ" config:app:get dav sendEventRemindersMode)" ] ; then
			echo "Change sending mode to occ"
			sudo -u www-data php -f "$occ" config:app:set dav sendEventRemindersMode --value occ
		else
			echo "Mode already set to occ"
		fi
		echo "Sending event"
		sudo -u www-data php -f "$occ" dav:send-event-reminders
	else
		echo "sendEventReminder not configured"
	fi
done < <(find /var/www -maxdepth 2 -name occ)
printf "Took %is\n" "$SECONDS"
unlock_me
