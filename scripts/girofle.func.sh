#!/usr/bin/env bash
function _build_lock_base_name() {
	local fn
	if [ -z "$1" ] ; then
		fn="girofle-$(basename "$0")-${FUNCNAME[3]}"
	else
		fn="$1"
	fi
	echo "$fn"
}
function _build_lock_file_name() {
	local fn lockfile lockdir="/var/lock"
	fn="$(_build_lock_base_name "$1")"
	if ! [ -w "$lockdir" ] ; then
		lockdir="/tmp"
	fi
	lockfile="$lockdir/$fn.lock"
	echo "$lockfile"
}
function _build_lastrun_file_name() {
	local fn lrfile lrdir="/var/lib"
	fn="$(_build_lock_base_name "$1")"
	if ! [ -w "$lrdir" ] ; then
		lrdir="/tmp"
	fi
	lrfile="$lrdir/$fn.lastrun"
	echo "$lrfile"
}
# Renvoie true si le pid est lancé sous cron, false sinon
function _under_cron() {
	local pid="${1:-$$}" cmd
	while [ "$pid" -eq 0 ] ; do
		cmd="$(ps -o cmd= "$pid")"
		if [[ "$cmd" =~ ^/usr/sbin/cron\  ]] || \
			[[ "$cmd" =~ ^/usr/local/sbin/fcron\  ]] || \
			[[ "$cmd" =~ ^/usr/bin/fcron\  ]] ; then
			return 0
		fi
		pid="$(ps -o ppid= "$pid")"
	done
	return 1
}
# Quand est-ce que "je" ai été lancé pour la dernière fois
function g_get_lastrun() {
	local lrfile="$(_build_lastrun_file_name "$1" )" G_LASTRUN_DEFAULT=${G_LASTRUN_DEFAULT:-3600}
	if ! [ -s "$lrfile" ] ; then
		echo $((EPOCHSECONDS-G_LASTRUN_DEFAULT)) > "$lrfile"
	fi
	cat "$lrfile"
}
function g_set_lastrun() {
	local lrfile="$(_build_lastrun_file_name "$1" )"
	echo "$EPOCHSECONDS" > "$lrfile"
}
# Lock un script au niveau de la fonction
# g_lock_me			# Crée un lock nommé girofle-<nom de fichier>-<fonction appelante>, dans /var/run si possible, sinon /tmp
# g_lock_me <nom>	# Crée un lock nommé <nom>
function g_lock_me() {
	local lockfile="$(_build_lock_file_name "$1" )" G_LOCK_FILE_TTL=${G_LOCK_FILE_TTL:-300}
	if [ -s "$lockfile" ]; then
		fn_age="$(( EPOCHSECONDS - $(stat -c %Y "${lockfile}" ) ))"
		if [ "$fn_age" -gt "$G_LOCK_FILE_TTL" ] ; then
			g_log '%s too old (age > %is), removing\n' "${lockfile}" "$G_LOCK_FILE_TTL"
			rm -f "${lockfile}"
		else
			g_log 'An instance of %s is running for %is (G_LOCK_FILE_TTL=%is) (pid=%i)' "$(_build_lock_file_name "$1")" "$fn_age" "$G_LOCK_FILE_TTL" "$(<"$lockfile")"
			return 1
		fi
	fi
	echo "$$" > "$lockfile"
}
function g_unlock_me() {
	local lockfile="$(_build_lock_file_name "$1" )"
	rm -f "$lockfile"
}
# Log qqch sur stderr
# g_log message
# g_log printf-style-chaine [ val ] ...
function g_log() {
	local m s
	if [ "$#" -gt 1 ] ; then
		local s="$1" ; shift
		printf -v m "$s" "$@"
	else
		m="$1"
	fi
	if [[ ${FUNCNAME[1]} =~ ^g_.* ]] ; then		# Suis-je appelé depuis une fct de cette biblio ?
		printf '[%s@%i] %s\n' "${FUNCNAME[2]}" "${BASH_LINENO[1]}" "$m" >&2		# la ref dans le script qui appel la fonction qui m'appel
	else
		printf '[%s@%i] %s\n' "${FUNCNAME[1]}" "${BASH_LINENO[0]}" "$m" >&2		# la ref est dans le script qui m'appel
	fi
}
# Récupère une valeur dans un fichier clé=valeur
# g_get_conf_value	ficher clé [ var ]		# Lit la clé "clé" dans le fichier "fichier" (et met la valeur dans "var", sinon sur stdout)
function g_get_conf_value() {	# file key [ var ]
	local conf_file="$1" key="$2" var="$3" v
	if [ ! -r "$conf_file" ] ; then
		g_log "Can't read $conf_file"
		return 1
	fi
	# adapté de https://stackoverflow.com/questions/19154996/awk-split-only-by-first-occurrence
	v="$(awk -F"=" "/^$key=/{ st = index(\$0,FS);print substr(\$0,st+1)}" "$conf_file")"
	if [ -z "$v" ] ; then
		g_log "Key '$key' wasn't found in '$conf_file'"
		return 2
	fi
	if [ -n "$var" ] ; then
		eval export "$var"="$v"
	else
		echo "$v"
	fi
}
# Dort un peu si le script est lancé sous cron
# g_sleep_under_cron [ max [ min ] ]	# Dort un nombre de seconde au pif entre min et max
function g_sleep_under_cron() {
	local max="${1:-60}" min="${2:-0}" s
	if _under_cron ; then
		s="$((RANDOM % (max-min) + min ))"
		sleep "$s"
	fi
}
