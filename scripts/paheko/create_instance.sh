#!/usr/bin/bash
# This script must be ran with admin rights
# Usage: sudo ./create_instance.sh yolo.girofle.cloud

# abort on error
set -e

function error_on_exit() {
    echo "Somethong went wrong, exiting"
    exit 1
}
trap error_on_exit ERR
########## ARGS  ##########

DIR_EXE=$(dirname $0)

DOMAIN=$1
BASE_DIR=/var/www/
DOWNLOAD=paheko-1.3.10.tar.gz
NGINX_TEMPLATE_PATH=${DIR_EXE}/conf/nginx.conf.tmpl
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${DOMAIN}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${DOMAIN}.conf


# create cert
echo -e "\e[32mSetting up TLS certificate ...\e[0m"
certbot certonly --noninteractive --nginx --email contact@girofle.cloud --agree-tos --expand -d ${DOMAIN}

# Create nginx
if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
then
  echo -e "\e[32mCreating nginx configuration file at ${NGINX_CONF_FILE_PATH} and link to it ${NGINX_CONF_LINK_PATH} ...\e[0m"
    cp ${NGINX_TEMPLATE_PATH} ${NGINX_CONF_FILE_PATH}
    sed -i 's/INSTANCE_HOST_NAME/'${DOMAIN}'/g' ${NGINX_CONF_FILE_PATH}
    ln -s ${NGINX_CONF_FILE_PATH} ${NGINX_CONF_LINK_PATH}
  echo "Nginx is reloading ..."
  systemctl reload nginx
else
  echo -e "\e[32mNginx configuration file already exists. Skipping nginx configuration.\e[0m"
fi

# Create directory
cd ${BASE_DIR}
mkdir ${BASE_DIR}/${DOMAIN}/

# Download source files if required
if [[ ! -f ${DOWNLOAD} ]]
then
  echo -e "\e[32mDownloading Paheko archive ${DOWNLOAD} ...\e[0m"
  wget -q -O ${DOWNLOAD} "https://fossil.kd2.org/paheko/uv/${DOWNLOAD}"
else
  echo -e "\e[32mPaheko archive already downloaded. Reusing existing archive.\e[0m"
fi

# extract archive
echo -e "\e[32mExtracting Paheko archive ${DOWNLOAD} into ${DOMAIN}...\e[0m"
tar xzf paheko*.tar.gz --strip 1 --directory ${DOMAIN}
chown -R www-data:www-data ${DOMAIN}

# Conclusion
echo -e "\e[32mHello, voilà !\e[0m"
echo -e "\e[32mYou can now go to https://$DOMAIN/install.php and finish the installation.\e[0m"

# NB
echo "Don't forget to add the following linces to the file ${DOMAIN}/config.local.php"
echo "const MAIL_SENDER = 'paheko@girofle.cloud';"
echo "const MAIL_RETURN_PATH = 'the-return-email-here'"
